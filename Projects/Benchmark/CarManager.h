#pragma once

#include <vector>
#include <random>

#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Engine/Engine.h>
#include <DTEngine/Core/SceneObjectComponent.h>

namespace Benchmark
{
	class CarManager : public DTCore::SceneObjectComponent
	{
	public:
		explicit CarManager(DTCore::SceneObject &sceneObject, size_t spawnTime);
		~CarManager();

		void addCar();
		void addCar(unsigned char laneId, unsigned char materialId);

		void update(DTEngine::Millisecs deltaTime) override;

	private:
		void initCache();

		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> createMaterial(size_t type);

	private:
		struct Cache;

	private:
		CarManager::Cache *mCache;

		DTEngine::Millisecs mDeltaTime;
		DTEngine::Millisecs mSpawnTime;

		std::minstd_rand mRandGenerator;
		std::uniform_int_distribution<int> mLineDistr;
		std::uniform_int_distribution<int> mSpawnTimeDistr;
	};

}