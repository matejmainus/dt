#pragma once

#include <vector>
#include <random>

#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Engine/Engine.h>
#include <DTEngine/Core/SceneObjectComponent.h>

namespace Benchmark
{
	class CloudManager : public DTCore::SceneObjectComponent
	{
	public:
		explicit CloudManager(DTCore::SceneObject &sceneObject, glm::vec3 areaSize);
		~CloudManager();

		void addCloud();

		glm::vec3 getCloudDestination(glm::vec3 pos);

	private:
		void initCache();

		glm::vec3 generateCloudPosition();

	private:
		struct Cache;

	private:
		CloudManager::Cache *mCache;

		glm::vec3 mAreaSize;

		std::minstd_rand mRandGenerator;
		std::uniform_real_distribution<float> mRandDistrib;

		char cloudIndex;
	};

}