#include "DTEngine/Core/SceneObject.h"
#include <glm/gtx/norm.hpp>

#include "Cloud.h"
#include "CloudManager.h"

namespace Benchmark
{
	Cloud::Cloud(DTCore::SceneObject & sceneObj, CloudManager & cloudManager) :
		SceneObjectComponent(sceneObj),
		mCloudManager(cloudManager),
		mTime(0)
	{
		mTransform = sceneObj.component<DTSceneObjectComponents::Transform>();
		mStart = mTransform->position();

		mDest = mCloudManager.getCloudDestination(mStart);
	}

	Cloud::~Cloud()
	{
	}

	void Cloud::update(DTEngine::Millisecs deltaTime)
	{
		mTime += deltaTime;

		float factor = mTime.count() * 0.0005f;
		glm::vec3 pos = glm::mix(mStart, mDest, factor);

		mTransform->translateTo(pos);

		if (factor >= 1.0)
		{
			mStart = pos;
			mDest = mCloudManager.getCloudDestination(mStart);
			mTime = DTEngine::Millisecs(0);
		}
	}
}