#pragma once

#include <vector>
#include <random>

#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Engine/glm.h>
#include <DTEngine/Engine/Engine.h>

#include <DTEngine/Core/SceneObjectComponent.h>
#include <DTEngine/Core/Materials/DefaultMaterial.h>
#include <DTEngine/Core/Materials/SimpleMaterial.h>

#include <DTEngine/Core/Texture.h>

namespace Benchmark
{
	class EnvManager : public DTCore::SceneObjectComponent
	{
	public:
		explicit EnvManager(DTCore::SceneObject &sceneObject,
			DTEngine::SharedPtr<DTCore::Texture> mask,
			glm::vec3 offsets, glm::vec3 scales);

		virtual ~EnvManager();

		void addBundle(size_t count);

		size_t growUpTree(DTCore::SceneObject *treeObj, size_t growLevel);

		void update(DTEngine::Millisecs deltaTime) override;
		
	private:
		void initCache();

		DTCore::SceneObject* addTree();
		DTCore::SceneObject* addBush();
		DTCore::SceneObject* addRock();

		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> createTreeMaterial(size_t type);
		DTEngine::SharedPtr<DTMaterials::SimpleMaterial> createBushMaterial();
		DTEngine::SharedPtr<DTMaterials::SimpleMaterial> createRockMaterial();

		glm::vec3 findPosition();

	private:
		struct Cache;

	private:
		glm::vec3 mOffsets;
		glm::vec3 mScales;

		EnvManager::Cache *mCache;

		DTEngine::SharedPtr<DTCore::Texture> mMask;

		std::minstd_rand mRandGenerator;
		DTEngine::Millisecs mDeltaTime;
	};

}
