#pragma once

#include <DTEngine/Core/Scene.h>
#include <DTEngine/Engine/Namespaces.h>

namespace Benchmark
{
	class Scene
	{
	public:
		explicit Scene()
		{}

		virtual ~Scene()
		{}

	protected:
		DTEngine::Core::Scene *mScene;
	};
}