#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Core/RenderStatsManager.h>
#include <iostream>

#include "StatsWriter.h"

namespace Benchmark
{

	StatsWriter::StatsWriter(const std::string &statsFile, const std::string &usageFile)
	{
		mStatsFile.open(statsFile);
		mUsageFile.open(usageFile);
	}

	StatsWriter::~StatsWriter()
	{
		mStatsFile.flush();
		mStatsFile.close();

		mUsageFile.flush();
		mUsageFile.close();
	}

	void StatsWriter::writeHeader()
	{
		mStatsFile << "frameTime,updateTime,drawTime,gpuFrameTime,"
			"drawCallCount,copyCallCount,queueSubmitCount,memoryMapCount,"
			"pipelineBindCount,descriptorUpdateCount,descriptorCopyCount,descriptorBindCount" << std::endl;
		
		mUsageFile << "gpuMemoryAllocSize,descriptorSetCount,texturesCount,buffersCount" << std::endl;
	}

	void StatsWriter::writeFrameStats()
	{
		DTCore::RenderStatsManager &statsMgr = DTCore::RenderStatsManager::getInstance();

		DTCore::RenderStatsManager::FrameStats *frameStats = statsMgr.lastFrame();

		//std::cout << frameStats->frameTime.count() << " " << frameStats->gpuFrameTime.count() << " " << frameStats->drawCallCount << std::endl;

		mStatsFile << frameStats->frameTime.count() << "," << frameStats->updateTime.count() << "," << frameStats->drawTime.count() << "," << frameStats->gpuFrameTime.count() << ","
		    << frameStats->drawCallCount << "," << frameStats->copyCallCount << "," << frameStats->queueSubmitCount << ","  << frameStats->memoryMapCount << ","
			<< frameStats->pipelineBindCount << "," << frameStats->descriptorUpdateCount << "," << frameStats->descriptorCopyCount << "," << frameStats->descriptorBindCount  << std::endl;
	}

	void StatsWriter::writeUsageStats()
	{
		DTCore::RenderStatsManager &statsMgr = DTCore::RenderStatsManager::getInstance();

		DTCore::RenderStatsManager::UsageStats *usageStats = statsMgr.usageStats();
		
		mUsageFile << usageStats->gpuMemoryAllocSize << "," << usageStats->descriptorSetCount << "," << usageStats->gpuBufferCount << "," << usageStats->gpuImageCount << std::endl;
	}

}