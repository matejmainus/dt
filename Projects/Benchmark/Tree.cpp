#include <DTEngine/Core/SceneObject.h>

#include "Tree.h"

namespace Benchmark
{
	Tree::Tree(DTCore::SceneObject &sceneObj, EnvManager* envMgr, DTEngine::Millisecs growUpTime) :
		SceneObjectComponent(sceneObj),
		mGrowUpTime(growUpTime),
		mGrowTime(0),
		mEnvMgr(envMgr)
	{
		
	}

	Tree::~Tree()
	{
	}

	void Tree::update(DTEngine::Millisecs deltaTime)
	{
		mGrowTime += deltaTime;

		if (mGrowTime > mGrowUpTime)
		{
			mGrowLevel = mEnvMgr->growUpTree(&mSceneObject, mGrowLevel);

			mGrowTime = DTEngine::Millisecs(0);
		}
	}
}
