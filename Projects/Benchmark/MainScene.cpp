#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Core/SceneManager.h>
#include <DTEngine/Core/ResourceManager.h>
#include <DTEngine/Core/SceneObject.h>
#include <DTEngine/Core/Mesh.h>
#include <DTEngine/Core/Materials/DefaultMaterial.h>
#include <DTEngine/Core/SceneObjectComponents/Transform.h>
#include <DTEngine/Core/SceneObjectComponents/MeshRenderer.h>
#include <DTEngine/Core/SceneObjectComponents/Camera.h>

#include "MainScene.h"
#include "CameraTransform.h"
#include "EnvManager.h"
#include "CarManager.h"
#include "CloudManager.h"
#include "App.h"

namespace Benchmark
{

	MainScene::MainScene()
	{
		mScene = DTCore::SceneManager::getInstance().createScene("MainBenchmarkScene");
		mScene->setAsMain();
		mScene->setBackgroundColor(DTCore::Color(0.38f, 0.67f, 0.989f));

		DTCore::SceneObject *rootObj = &mScene->rootObject();

		initCamera(rootObj);
		DTCore::SceneObject* land = initLand(rootObj);
		initShop(land);
		initRoad(land);

		initEnvironment(land);
		initCars(land);
		initClouds(land);
	}

	MainScene::~MainScene()
	{
		DTCore::SceneManager::getInstance().removeScene(mScene);
	}

	DTCore::SceneObject* MainScene::initCamera(DTCore::SceneObject *parent)
	{
		DTCore::SceneObject *camObj = parent->createChild("camera");

		DTSceneObjectComponents::Camera *camera = camObj->createComponent<DTSceneObjectComponents::Camera>(0.7f, 1.77f, 0.1f, 1000.0f);
		camera->setRenderTarget(App::getInstance().window());
		camera->setAsMain();
		
		DTSceneObjectComponents::Transform *cameraTransform = camObj->component<DTSceneObjectComponents::Transform>();
		CameraTransform *camTransformScript = camObj->createComponent<CameraTransform>(glm::vec2(11.7, -0.5), glm::vec3(0,0,0), 180.0f, 0.003f, 4.0f);
		
		return camObj;
	}

	DTCore::SceneObject* MainScene::initLand(DTCore::SceneObject *parent)
	{
		DTEngine::SharedPtr<DTCore::Mesh> mesh = DTCore::ResourceManager::getInstance().loadMesh("./Data/Mesh/land.fbx");
	
		DTEngine::SharedPtr<DTCore::Texture> diffuseTexture = DTCore::ResourceManager::getInstance().loadTexture("./Data/Textures/land-diffuse.png");

		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> material = std::make_shared<DTMaterials::DefaultMaterial>();
		material->setDiffuseTexture(diffuseTexture);

		DTCore::SceneObject *obj = parent->createChild("land");
		DTSceneObjectComponents::Transform *transform = obj->component<DTSceneObjectComponents::Transform>();
		//transform->move(0, 20, -100);

		DTSceneObjectComponents::MeshRenderer *meshRenderer = obj->createComponent<DTSceneObjectComponents::MeshRenderer>();

		meshRenderer->setMesh(mesh);
		meshRenderer->setMaterial(material);
		
		return obj;
	}

	DTCore::SceneObject* MainScene::initShop(DTCore::SceneObject *parent)
	{
		DTEngine::SharedPtr<DTCore::Mesh> mesh = DTCore::ResourceManager::getInstance().loadMesh("./Data/Mesh/shop.fbx");

		DTEngine::SharedPtr<DTCore::Texture> diffuseTexture = DTCore::ResourceManager::getInstance().loadTexture("./Data/Textures/shop-diffuse.png");

		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> material = std::make_shared<DTMaterials::DefaultMaterial>();
		material->setDiffuseTexture(diffuseTexture);

		DTCore::SceneObject *obj = parent->createChild("shop");
		DTSceneObjectComponents::Transform *transform = obj->component<DTSceneObjectComponents::Transform>();
		transform->translateTo(36.0f, 0.4f, 1.6f);
		transform->rotateByAngles(0, 206, 0);
		
		DTSceneObjectComponents::MeshRenderer *meshRenderer = obj->createComponent<DTSceneObjectComponents::MeshRenderer>();

		meshRenderer->setMesh(mesh);
		meshRenderer->setMaterial(material);

		return obj;
	}

	DTCore::SceneObject* MainScene::initEnvironment(DTCore::SceneObject *parent)
	{
		DTEngine::SharedPtr<DTCore::Texture> mask = DTCore::ResourceManager::getInstance().loadTexture("./Data/Textures/env-height.png");

		DTCore::SceneObject *obj = parent->createChild("EnvManager");
		EnvManager *foliageManager = obj->createComponent<EnvManager>(mask, glm::vec3(-50, 13, 52), glm::vec3(100, -63, -140));
		foliageManager->addBundle(App::getInstance().objectCount());

		return obj;
	}

	DTCore::SceneObject* MainScene::initCars(DTCore::SceneObject *parent)
	{
		DTCore::SceneObject *obj = parent->createChild("carMgr");
		CarManager *carManager = obj->createComponent<CarManager>(400);
		
		DTSceneObjectComponents::Transform *transform = obj->component<DTSceneObjectComponents::Transform>();
		transform->translateTo(0, 1.5, 10);

		return obj;
	}

	DTCore::SceneObject * MainScene::initClouds(DTCore::SceneObject * parent)
	{
		DTCore::SceneObject *obj = parent->createChild("cloudMgr");
		CloudManager *cloudManager = obj->createComponent<CloudManager>(glm::vec3(50, 10, 50));
		
		for (size_t i = 0; i < 12; ++i)
			cloudManager->addCloud();

		DTSceneObjectComponents::Transform *transform = obj->component<DTSceneObjectComponents::Transform>();
		transform->translateTo(0, -40.0f, 0);

		return obj;
	}

	DTCore::SceneObject* MainScene::initRoad(DTCore::SceneObject *parent)
	{
		DTEngine::SharedPtr<DTCore::Mesh> mesh = DTCore::ResourceManager::getInstance().loadMesh("./Data/Mesh/road.fbx");

		DTEngine::SharedPtr<DTCore::Texture> diffuseTexture = DTCore::ResourceManager::getInstance().loadTexture("./Data/Textures/road-diffuse.png");

		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> material = std::make_shared<DTMaterials::DefaultMaterial>();
		material->setDiffuseTexture(diffuseTexture);
		material->setDiffuseColor(DTCore::Color(.2f, .2f, .2f));

		DTCore::SceneObject *obj = parent->createChild("road");
		DTSceneObjectComponents::Transform *transform = obj->component<DTSceneObjectComponents::Transform>();
		transform->translateTo(0, 2.5, 10);

		DTSceneObjectComponents::MeshRenderer *meshRenderer = obj->createComponent<DTSceneObjectComponents::MeshRenderer>();

		meshRenderer->setMesh(mesh);
		meshRenderer->setMaterial(material);

		return obj;
	}

}