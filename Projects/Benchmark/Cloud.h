#pragma once
#pragma once

#include <vector>
#include <random>

#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Engine/Engine.h>
#include <DTEngine/Core/SceneObjectComponent.h>
#include <glm/gtx/norm.hpp>

#include <DTEngine/Core/SceneObjectComponents/Transform.h>

namespace Benchmark
{
	class CloudManager;

	class Cloud : public DTCore::SceneObjectComponent
	{
	public:
		explicit Cloud(DTCore::SceneObject &sceneObj, CloudManager &cloudManager);
		~Cloud();

		void update(DTEngine::Millisecs deltaTime) override;

	private:
		glm::vec3 mStart;
		glm::vec3 mDest;

		DTEngine::Millisecs mTime;

		CloudManager& mCloudManager;

		DTSceneObjectComponents::Transform *mTransform;
	};

}