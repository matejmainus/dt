#pragma once

#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Core/SceneObjectComponent.h>
#include <DTEngine/Core/EventHandler.h>

namespace Benchmark
{

	class CameraTransform : public DTCore::SceneObjectComponent, public DTCore::EventHandler
	{
	public:
		explicit CameraTransform(DTCore::SceneObject &obj,
			glm::vec2 startPoint, 
			glm::vec3 lookPoint,
			float radius, float orbitSpeed, float zoomSpeed);
		virtual ~CameraTransform();

		virtual bool handleEvent(const SDL_Event *evt) override;

		void update(DTEngine::Millisecs deltaTime) override;

	private:
		float mMouseX;
		float mMouseY;

		float mRadius;

		float mOrbitSpeed;
		float mZoomSpeed;

		glm::vec3 mLookPoint;

		DTSceneObjectComponents::Transform *mTransform;
	};

}
