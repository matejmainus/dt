#include <random>

#include <DTEngine/Core/ResourceManager.h>
#include <DTEngine/Core/SceneObject.h>
#include <DTEngine/Core/Mesh.h>
#include <DTEngine/Core/Materials/DefaultMaterial.h>
#include <DTEngine/Core/Materials/SimpleMaterial.h>
#include <DTEngine/Core/SceneObjectComponents/Transform.h>
#include <DTEngine/Core/SceneObjectComponents/MeshRenderer.h>

#include "App.h"
#include "EnvManager.h"
#include "Tree.h"

#define TREE_LEVELS 3

namespace Benchmark
{
	struct EnvManager::Cache
	{
		DTEngine::SharedPtr<DTCore::Mesh> treeMesh[TREE_LEVELS];
		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> treeMat[TREE_LEVELS];

		DTEngine::SharedPtr<DTCore::Mesh> bushMesh;
		DTEngine::SharedPtr<DTMaterials::SimpleMaterial> bushMat;

		DTEngine::SharedPtr<DTCore::Mesh> rockMesh;
		DTEngine::SharedPtr<DTMaterials::SimpleMaterial> rockMat;
	};

	EnvManager::EnvManager(DTCore::SceneObject &sceneObject,
		DTEngine::SharedPtr<DTCore::Texture> mask,
		glm::vec3 offsets, glm::vec3 scales) :
		SceneObjectComponent(sceneObject),
		mMask(mask),
		mOffsets(offsets),
		mScales(scales)
	{
		unsigned seed = (unsigned) std::chrono::system_clock::now().time_since_epoch().count();
		mRandGenerator.seed(seed);

		mCache = new EnvManager::Cache();

		initCache();
	}

	EnvManager::~EnvManager()
	{
		delete mCache;
	}

	void EnvManager::addBundle(size_t count)
	{
		unsigned int type;

		for (size_t i = 0; i < count; ++i)
		{
			type = mRandGenerator() % 10;

			if(type < 8)
				addTree();
			else if(type == 8)
				addBush();
			else if(type == 9)
				addRock();
		}
	}

	DTCore::SceneObject* EnvManager::addTree()
	{
		glm::vec3 pos = findPosition();

		pos *= mScales;
		pos += mOffsets;

		glm::vec3 rot = glm::vec3(0, mRandGenerator() % 360, 0);

		DTCore::SceneObject *treeObj = mSceneObject.createChild("tree");

		DTSceneObjectComponents::Transform *transform = treeObj->component<DTSceneObjectComponents::Transform>();
		transform->translateTo(pos);
		transform->rotateByAngles(rot);

		DTSceneObjectComponents::MeshRenderer *meshRenderer = treeObj->createComponent<DTSceneObjectComponents::MeshRenderer>();
		meshRenderer->setMesh(mCache->treeMesh[0]);
		meshRenderer->setMaterial(mCache->treeMat[0]);

		DTEngine::Millisecs growTime = DTEngine::Millisecs((mRandGenerator() % 20000) + 3000);

		Tree *tree = treeObj->createComponent<Tree>(this, growTime);

		return treeObj;
	}

	DTCore::SceneObject * EnvManager::addBush()
	{
		glm::vec3 pos = findPosition();

		pos *= mScales;
		pos += mOffsets;

		DTCore::SceneObject *bushObj = mSceneObject.createChild("bush");

		DTSceneObjectComponents::Transform *transform = bushObj->component<DTSceneObjectComponents::Transform>();
		transform->translateTo(pos);

		DTSceneObjectComponents::MeshRenderer *meshRenderer = bushObj->createComponent<DTSceneObjectComponents::MeshRenderer>();
		meshRenderer->setMesh(mCache->bushMesh);

		if (App::getInstance().useMaterialReuse())
			meshRenderer->setMaterial(mCache->bushMat);
		else
			meshRenderer->setMaterial(createBushMaterial());

		return bushObj;
	}

	DTCore::SceneObject * EnvManager::addRock()
	{
		glm::vec3 pos = findPosition();

		pos *= mScales;
		pos += mOffsets;

		DTCore::SceneObject *rockObj = mSceneObject.createChild("rock");

		DTSceneObjectComponents::Transform *transform = rockObj->component<DTSceneObjectComponents::Transform>();
		transform->translateTo(pos);

		DTSceneObjectComponents::MeshRenderer *meshRenderer = rockObj->createComponent<DTSceneObjectComponents::MeshRenderer>();
		meshRenderer->setMesh(mCache->rockMesh);

		if (App::getInstance().useMaterialReuse())
			meshRenderer->setMaterial(mCache->rockMat);
		else
			meshRenderer->setMaterial(createRockMaterial());

		return rockObj;
	}

	size_t EnvManager::growUpTree(DTCore::SceneObject * treeObj, size_t growLevel)
	{
		DTSceneObjectComponents::MeshRenderer *meshRenderer = treeObj->component<DTSceneObjectComponents::MeshRenderer>();

		growLevel++;

		if (growLevel == TREE_LEVELS)
		{
			treeObj->destroy();
			addTree();

			return 0;
		}
		else
		{
			meshRenderer->setMesh(mCache->treeMesh[growLevel]);

			if (App::getInstance().useMaterialReuse())
				meshRenderer->setMaterial(mCache->treeMat[growLevel]);
			else
				meshRenderer->setMaterial(createTreeMaterial(growLevel));
			
			return growLevel;
		}
	}

	void EnvManager::update(DTEngine::Millisecs deltaTime)
	{
	}

	glm::vec3 EnvManager::findPosition()
	{
		float x = 0;
		float y = 0;
		float z = 0;
		RGBQUAD texel;

		while (y < 0.0001)
		{
			x = (mRandGenerator() - 1) / (float) mRandGenerator.max();
			z = (mRandGenerator() - 1)/ (float) mRandGenerator.max();

			mMask->texel(x, z, &texel);

			y = texel.rgbRed / 255.0f;
		}

		return glm::vec3(x, y, z);
	}

	void EnvManager::initCache()
	{
		DTCore::ResourceManager &resMgr = DTCore::ResourceManager::getInstance();

		DTEngine::SharedPtr<DTCore::Texture> diffuseTexture;

		for (size_t i = 0; i < TREE_LEVELS; ++i)
		{
			mCache->treeMesh[i] = resMgr.loadMesh("./Data/Mesh/tree" + std::to_string(i) + ".fbx");
			mCache->treeMat[i] = createTreeMaterial(i);
		}

		mCache->bushMesh = resMgr.loadMesh("./Data/Mesh/bush0.fbx");
		mCache->bushMat = createBushMaterial();

		mCache->rockMesh = resMgr.loadMesh("./Data/Mesh/rock0.fbx");
		mCache->rockMat = createRockMaterial();
	}

	DTEngine::SharedPtr<DTMaterials::DefaultMaterial> EnvManager::createTreeMaterial(size_t type)
	{
		DTCore::ResourceManager &resMgr = DTCore::ResourceManager::getInstance();

		DTEngine::SharedPtr<DTCore::Texture> diffuseTexture = resMgr.loadTexture("./Data/Textures/tree" + std::to_string(type) + "-diffuse.png");
		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> material = std::make_shared<DTMaterials::DefaultMaterial>();
		material->setDiffuseTexture(diffuseTexture);

		return material;
	}

	DTEngine::SharedPtr<DTMaterials::SimpleMaterial> EnvManager::createBushMaterial()
	{
		DTEngine::SharedPtr<DTMaterials::SimpleMaterial> material = std::make_shared<DTMaterials::SimpleMaterial>();
		material->setDiffuseColor(DTCore::Color(0.261f, 1.0f, 0.046f, 1.0f));

		return material;
	}

	DTEngine::SharedPtr<DTMaterials::SimpleMaterial> EnvManager::createRockMaterial()
	{
		DTEngine::SharedPtr<DTMaterials::SimpleMaterial> material = std::make_shared<DTMaterials::SimpleMaterial>();
		material->setDiffuseColor(DTCore::Color(0.196f, 0.156f, 0.082f, 1.0f));

		return material;
	}
}