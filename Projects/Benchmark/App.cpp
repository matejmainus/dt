#include <DTEngine/Renderer/Compositor.h>
#include <DTEngine/Renderer/RenderSystem.h>

#include "App.h"
#include "StatsWriter.h"
#include "MainScene.h"
#include "PrototypeScene.h"

namespace Benchmark
{
	const std::string APP_NAME = "Benchmark";

	App::App() :
		mAppScene(nullptr),
		mObjCount(2000),
		mSceneType(SceneType::default)
	{
	}

	App::~App()
	{
	}

	void App::setFlags(Flags flags)
	{
		mFlags = flags;
	}

	void App::setObjectCount(size_t objCount)
	{
		mObjCount = objCount;
	}

	void App::setScene(SceneType type)
	{
		mSceneType = type;
	}

	void App::init()
	{
		mStatsWriter = new StatsWriter("stats.csv", "usage.csv");
		
		mWindow = DTRenderer::RenderSystem::getInstance().createWindow("", 1366, 768);

		if(mSceneType == SceneType::prototype)
			mAppScene = new PrototypeScene();
		else
			mAppScene = new MainScene();
	}

	void App::shutdown()
	{
		if (mAppScene)
			delete mAppScene;

		if (mStatsWriter)
			delete mStatsWriter;

		mWindow.reset();
	}

	void App::start()
	{
		mStatsWriter->writeHeader();
	}

	void App::stop()
	{
	}

	const DTEngine::SharedPtr<DTRenderer::Window>& App::window() const
	{
		return mWindow;
	}

	const std::string & App::name() const
	{
		return APP_NAME;
	}

	void App::update(const DTEngine::Millisecs &deltaTime)
	{
		mWindow->setTitle("Benchmark " + std::to_string(deltaTime.count()) + "ms");

		mStatsWriter->writeFrameStats();
		mStatsWriter->writeUsageStats();
	}
}