#include <iostream>
#include <memory>
#include <string>

#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Core/Kernel.h>
#include <DTEngine/Core/RenderSettings.h>

#include "App.h"

void parseArgs(int argc, char* argv[])
{
	Benchmark::App &app = Benchmark::App::getInstance();
	DTCore::RenderSettings &rs = DTCore::RenderSettings::getInstance();

	if (argc > 2) {
		size_t flags = std::stoi(argv[1]);

		rs.setFlags(DTCore::RenderSettings::Flags(flags));
	}

	if (argc > 3) {
		size_t flags = std::stoi(argv[2]);

		app.setFlags(Benchmark::App::Flags(flags));
	}

	if (argc > 4) {
		size_t objects = std::stoi(argv[3]);

		app.setObjectCount(objects);
	}

	if (argc > 5) {
		size_t threads = std::stoi(argv[4]);

		rs.setRenderThreads(threads);
	}

	if (argc > 6) {
		size_t gpuPos = std::stoi(argv[5]);

		rs.setSelectedGpu(gpuPos);
	}
}


int main(int argc, char* argv[])
{
	Benchmark::App app;

	DTCore::RenderSettings::create();
	DTCore::RenderSettings &rs = DTCore::RenderSettings::getInstance();

	parseArgs(argc, argv);

	/*	
	rs.setFlags(DTCore::RenderSettings::FlagBits::stagingBuffers |
		DTCore::RenderSettings::FlagBits::memoryPools |
		DTCore::RenderSettings::FlagBits::pushConstants |
		DTCore::RenderSettings::FlagBits::treeSort
		);
	
	//app.setFlags(Benchmark::App::Flags(0));
	//app.setFlags(Benchmark::App::FlagBits::materialReuse);
	app.setObjectCount(8000);
	*/
	//rs.setRenderThreads(1);

	DTCore::Kernel kernel(app);

	kernel.run();

	return 0;
}