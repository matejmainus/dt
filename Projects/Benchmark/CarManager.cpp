#include <DTEngine/Core/ResourceManager.h>
#include <DTEngine/Core/SceneObject.h>
#include <DTEngine/Engine/glm.h>
#include <DTEngine/Core/Mesh.h>
#include <DTEngine/Core/Texture.h>
#include <DTEngine/Core/Materials/DefaultMaterial.h>
#include <DTEngine/Core/SceneObjectComponents/Transform.h>
#include <DTEngine/Core/SceneObjectComponents/MeshRenderer.h>

#include "App.h"
#include "CarManager.h"
#include "Car.h"

#define CAR_COLORS 4

namespace Benchmark
{
	struct CarManager::Cache
	{
		DTEngine::SharedPtr<DTCore::Mesh> carMesh;
		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> carMat[CAR_COLORS];
	};

	CarManager::CarManager(DTCore::SceneObject &sceneObject, size_t spawnTime):
		SceneObjectComponent(sceneObject),
		mSpawnTime(0),
		mLineDistr(0, 1),
		mSpawnTimeDistr(spawnTime, 2*spawnTime)
	{
		unsigned seed = (unsigned) std::chrono::system_clock::now().time_since_epoch().count();
		mRandGenerator.seed(seed);

		mCache = new CarManager::Cache();

		initCache();
	}

	CarManager::~CarManager()
	{
		delete mCache;
	}

	void CarManager::addCar()
	{
		unsigned int lane = mLineDistr(mRandGenerator); 
		unsigned int matId = mRandGenerator() % CAR_COLORS;

		addCar(lane, matId);
	}

	void CarManager::addCar(unsigned char laneId, unsigned char materialId)
	{
		DTSceneObjectComponents::Transform *transform;
		glm::vec3 dest;

		DTCore::SceneObject *obj = mSceneObject.createChild("car");

		transform = obj->component<DTSceneObjectComponents::Transform>();
		
		if (laneId == 0)
		{
			transform->translateTo(50, 0, -2);
			transform->rotateByAngles(0, -90, 0);

			dest = glm::vec3(-50, 0, -2);
		}
		else
		{
			transform->translateTo(-50, 0, 2);
			transform->rotateByAngles(0, 90, 0);

			dest = glm::vec3(50, 0, 2);
		}

		Car *carScript = obj->createComponent<Car>(dest);

		DTSceneObjectComponents::MeshRenderer *meshRenderer = obj->createComponent<DTSceneObjectComponents::MeshRenderer>();

		meshRenderer->setMesh(mCache->carMesh);

		if(App::getInstance().useMaterialReuse())
			meshRenderer->setMaterial(mCache->carMat[materialId]);
		else
		{
			meshRenderer->setMaterial(createMaterial(materialId));
		}
	}

	void CarManager::update(DTEngine::Millisecs deltaTime)
	{
		mDeltaTime += deltaTime;

		if (mDeltaTime >= mSpawnTime)
		{
			addCar();

			mDeltaTime = DTEngine::Millisecs(0);
			mSpawnTime = DTEngine::Millisecs(mSpawnTimeDistr(mRandGenerator));
		}
	}

	void CarManager::initCache()
	{
		DTCore::ResourceManager &resMgr = DTCore::ResourceManager::getInstance();

		mCache->carMesh = resMgr.loadMesh("./Data/Mesh/car.fbx");
		
		for (size_t i = 0; i < CAR_COLORS; ++i)
		{
			mCache->carMat[i] = createMaterial(i);
		}
	}

	DTEngine::SharedPtr<DTMaterials::DefaultMaterial> CarManager::createMaterial(size_t type)
	{
		DTEngine::SharedPtr<DTCore::Texture> diffuseTexture = DTCore::ResourceManager::getInstance().loadTexture("./Data/Textures/car" + std::to_string(type) + "-diffuse.png");

		DTEngine::SharedPtr<DTMaterials::DefaultMaterial> material;

		material = std::make_shared<DTMaterials::DefaultMaterial>();
		material->setDiffuseTexture(diffuseTexture);

		return material;
	}


}