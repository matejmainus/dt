#pragma once

#include "Scene.h"

namespace Benchmark
{
	class PrototypeScene : public Scene
	{
	public:
		explicit PrototypeScene();
		~PrototypeScene();

	private:
		DTCore::SceneObject* initCamera(DTCore::SceneObject *parent);
		DTCore::SceneObject* initObjects(DTCore::SceneObject *parent);
	
	};
}