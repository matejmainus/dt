#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Core/SceneManager.h>
#include <DTEngine/Core/ResourceManager.h>
#include <DTEngine/Core/SceneObject.h>
#include <DTEngine/Core/Mesh.h>
#include <DTEngine/Core/Materials/DefaultMaterial.h>
#include <DTEngine/Core/SceneObjectComponents/Transform.h>
#include <DTEngine/Core/SceneObjectComponents/MeshRenderer.h>
#include <DTEngine/Core/SceneObjectComponents/Camera.h>

#include "PrototypeScene.h"
#include "App.h"

namespace Benchmark
{
	PrototypeScene::PrototypeScene()
	{
		mScene = DTCore::SceneManager::getInstance().createScene("PrototypeTestScene");
		mScene->setAsMain();
		mScene->setBackgroundColor(DTCore::Color(0,0,0));

		DTCore::SceneObject *rootObj = &mScene->rootObject();

		initCamera(rootObj);
		initObjects(rootObj);
	}

	PrototypeScene::~PrototypeScene()
	{
		DTCore::SceneManager::getInstance().removeScene(mScene);
	}

	DTCore::SceneObject* PrototypeScene::initCamera(DTCore::SceneObject *parent)
	{
		DTCore::SceneObject *camObj = parent->createChild("camera");

		DTSceneObjectComponents::Camera *camera = camObj->createComponent<DTSceneObjectComponents::Camera>(0.7f, 1.77f, 0.1f, 1000.0f);
		camera->setRenderTarget(App::getInstance().window());
		camera->setAsMain();

		DTSceneObjectComponents::Transform *cameraTransform = camObj->component<DTSceneObjectComponents::Transform>();
		cameraTransform->translateTo(0, 0, -600);
		//CameraTransform *camTransformScript = camObj->createComponent<CameraTransform>(glm::vec2(11.7, -0.5), glm::vec3(0, 0, 0), 200.0f, 0.005f, 4.0f);

		return camObj;
	}

	DTCore::SceneObject * PrototypeScene::initObjects(DTCore::SceneObject * parent)
	{
		const int MAX = (int)sqrt(App::getInstance().objectCount());
		
		float xOffset = 5.0f;
		float yOffset = 5.0f;

		DTCore::ResourceManager &resMgr = DTCore::ResourceManager::getInstance();
		DTEngine::SharedPtr<DTCore::Mesh> carMesh = resMgr.loadMesh("./Data/Mesh/car.fbx");
		
		DTEngine::SharedPtr<DTCore::Texture> diffuseTexture = DTCore::ResourceManager::getInstance().loadTexture("./Data/Textures/car0-diffuse.png");

		for (int y = 0; y < MAX; ++y)
		{
			for (int x = 0; x < MAX; ++x)
			{
				DTCore::SceneObject *car = parent->createChild("car");

				DTSceneObjectComponents::Transform *transform = car->component<DTSceneObjectComponents::Transform>();
				transform->translateTo(
					(x - MAX / 2) * xOffset,
					(y - MAX / 2) * yOffset,
					0.0f);

				DTEngine::SharedPtr<DTMaterials::DefaultMaterial> material = std::make_shared<DTMaterials::DefaultMaterial>();
				material->setDiffuseTexture(diffuseTexture);


				DTSceneObjectComponents::MeshRenderer *meshRenderer = car->createComponent<DTSceneObjectComponents::MeshRenderer>();
				meshRenderer->setMesh(carMesh);
				meshRenderer->setMaterial(material);
			}
		}

		return nullptr;
	}

}


