#pragma once

#include <string>
#include <fstream>

namespace Benchmark
{
	class StatsWriter
	{
	public:
		explicit StatsWriter(const std::string &statsFile, const std::string &usageFile);
		~StatsWriter();

		void writeHeader();
		void writeFrameStats();
		void writeUsageStats();

	private:
		std::ofstream mStatsFile;
		std::ofstream mUsageFile;
	};

}