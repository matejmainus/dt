#pragma once

#include <vector>
#include <random>

#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Engine/Engine.h>
#include <DTEngine/Core/SceneObjectComponent.h>
#include <DTEngine/Core/SceneObjectComponents/Transform.h>

namespace Benchmark
{
	class Car : public DTCore::SceneObjectComponent
	{
	public:
		explicit Car(DTCore::SceneObject &sceneObj, glm::vec3 destination);
		~Car();

		void update(DTEngine::Millisecs deltaTime) override;

	private:
		glm::vec3 mStart;
		glm::vec3 mDest;

		DTEngine::Millisecs mTime;

		DTSceneObjectComponents::Transform *mTransform;
	};

}