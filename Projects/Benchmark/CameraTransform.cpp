#include <DTEngine/Core/SceneObject.h>
#include <DTEngine/Core/SceneObjectComponents/Transform.h>
#include <DTEngine/Core/InputManager.h>
#include <DTEngine/Core/EventManager.h>

#include "CameraTransform.h"

#include <iostream>
#include <algorithm>

#define MAX_ZOOM 250
#define MIN_ZOOM 3

namespace Benchmark
{
	CameraTransform::CameraTransform(DTCore::SceneObject &sceneObject, 
		glm::vec2 startPoint,
		glm::vec3 lookPoint, 
		float radius, float orbitSpeed, float zoomSpeed):
		SceneObjectComponent(sceneObject),
		mMouseX(startPoint.x),
		mMouseY(startPoint.y),
		mLookPoint(lookPoint),
		mRadius(radius),
		mOrbitSpeed(orbitSpeed),
		mZoomSpeed(zoomSpeed)
	{
		mTransform = sceneObject.component<DTSceneObjectComponents::Transform>();

		DTCore::InputManager::getInstance().lockMouse(true);
		DTCore::EventManager::getInstance().addHandler(this);
	}

	CameraTransform::~CameraTransform()
	{
		DTCore::EventManager::getInstance().removeHandler(this);
	}

	bool CameraTransform::handleEvent(const SDL_Event* evt)
	{
		if (evt->type == SDL_MOUSEWHEEL)
		{
			if ((evt->wheel.y > 0 && mRadius > MIN_ZOOM) || (evt->wheel.y < 0 && mRadius < MAX_ZOOM))
				mRadius -= evt->wheel.y * mZoomSpeed;

			return true;
		}

		return false;
	}

	void CameraTransform::update(DTEngine::Millisecs deltaTime)
	{
		DTCore::InputManager &inputMgr = DTCore::InputManager::getInstance();

		int mouseX = 0;
		int mouseY = 0;

		inputMgr.mouseRelativePosition(&mouseX, &mouseY);
	
		mMouseX += mouseX * deltaTime.count() * mOrbitSpeed;
		mMouseY += mouseY * deltaTime.count() * mOrbitSpeed;

		mMouseY = std::min(0.0f, std::max(-glm::half_pi<float>()+0.01f, mMouseY));
		
		float x = mRadius * -sinf(mMouseX) * cosf(mMouseY);
		float y = mRadius * -sinf(mMouseY);
		float z = -mRadius * cosf(mMouseX) * cosf(mMouseY);

		mTransform->translateTo(x, y, z);
		mTransform->lookAt(mLookPoint);
	}

}