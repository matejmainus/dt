#pragma once

#include <DTEngine/Core/App.h>
#include <DTEngine/Engine/Singleton.h>
#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Renderer/Window.h>
#include <DTEngine/Engine/Flags.h>

namespace Benchmark
{
	class MainScene;
	class PrototypeScene;
	class StatsWriter;
	class Scene;

	class App : public DTEngine::Core::App, public DTEngine::Singleton<App>
	{
	public:
		enum class FlagBits
		{
			none = 0,
			materialReuse = 1,
		};

		enum class SceneType
		{
			default,
			prototype
		};

		typedef DTEngine::Flags<App::FlagBits> Flags;

	public:
		explicit App();
		virtual ~App();

		void setFlags(Flags flags);
		inline bool useMaterialReuse() const { return mFlags & FlagBits::materialReuse; }

		void setObjectCount(size_t objCount);
		inline size_t objectCount() const { return mObjCount; }

		void setScene(SceneType id);
		inline SceneType sceneId() const { return mSceneType; }

		virtual void init() override;
		virtual void shutdown() override;

		virtual void start() override;
		virtual void stop() override;

		virtual const std::string & name() const override;

		const DTEngine::SharedPtr<DTRenderer::Window>& window() const;

		void update(const DTEngine::Millisecs &deltaTime) override;

	private:
		Scene *mAppScene;

		StatsWriter *mStatsWriter;

		DTEngine::SharedPtr<DTRenderer::Window> mWindow;

		Flags mFlags;
		size_t mObjCount;
		SceneType mSceneType;
	};

}