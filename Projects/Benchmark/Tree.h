#pragma once

#include <vector>
#include <random>

#include <DTEngine/Engine/Namespaces.h>
#include <DTEngine/Engine/Engine.h>
#include <DTEngine/Core/SceneObjectComponent.h>
#include <DTEngine/Core/SceneObjectComponents/Transform.h>

#include "EnvManager.h"

namespace Benchmark
{
	class Tree : public DTCore::SceneObjectComponent
	{
	public:
		explicit Tree(DTCore::SceneObject &sceneObj, EnvManager* envMgr, DTEngine::Millisecs growUpTime);
		~Tree();

		void update(DTEngine::Millisecs deltaTime) override;

	private:
		DTEngine::Millisecs mGrowUpTime;
		DTEngine::Millisecs mGrowTime;

		size_t mGrowLevel;

		EnvManager *mEnvMgr;
	};

}