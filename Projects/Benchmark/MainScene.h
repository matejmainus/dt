#pragma once

#include "Scene.h"

namespace Benchmark
{
	class MainScene : public Scene
	{
	public:
		explicit MainScene();
		~MainScene();

	private:
		DTCore::SceneObject* initCamera(DTCore::SceneObject *parent);
		DTCore::SceneObject* initLand(DTCore::SceneObject *parent);
		DTCore::SceneObject* initShop(DTCore::SceneObject *parent);
		DTCore::SceneObject* initRoad(DTCore::SceneObject *parent);
		DTCore::SceneObject* initEnvironment(DTCore::SceneObject *parent);
		DTCore::SceneObject* initCars(DTCore::SceneObject *parent);
		DTCore::SceneObject* initClouds(DTCore::SceneObject *parent);
	};
}