#include <DTEngine/Core/ResourceManager.h>
#include <DTEngine/Core/SceneObject.h>
#include <DTEngine/Engine/glm.h>
#include <DTEngine/Core/Mesh.h>
#include <DTEngine/Core/Texture.h>
#include <DTEngine/Core/Materials/SimpleMaterial.h>
#include <DTEngine/Core/SceneObjectComponents/Transform.h>
#include <DTEngine/Core/SceneObjectComponents/MeshRenderer.h>

#include "CloudManager.h"
#include "Cloud.h"

#define CLOUD_TYPES 3
#define CLOUD_DISTANCE 5.0f

namespace Benchmark
{
	struct CloudManager::Cache
	{
		DTEngine::SharedPtr<DTCore::Mesh> cloudMesh[CLOUD_TYPES];
		DTEngine::SharedPtr<DTMaterials::SimpleMaterial> cloudMat;
	};

	CloudManager::CloudManager(DTCore::SceneObject &sceneObject, glm::vec3 areaSize) :
		SceneObjectComponent(sceneObject),
		mAreaSize(areaSize),
		cloudIndex(0),
		mRandDistrib(-1.0f, 1.0f)
	{
		unsigned seed = (unsigned) std::chrono::system_clock::now().time_since_epoch().count();
		mRandGenerator.seed(seed);

		mCache = new CloudManager::Cache();

		initCache();
	}

	CloudManager::~CloudManager()
	{
		delete mCache;
	}

	void CloudManager::addCloud()
	{
		DTSceneObjectComponents::Transform *transform;

		DTCore::SceneObject *obj = mSceneObject.createChild("cloud");

		transform = obj->component<DTSceneObjectComponents::Transform>();
		transform->translateTo(generateCloudPosition());

		Cloud *cloud = obj->createComponent<Cloud>(*this);

		DTSceneObjectComponents::MeshRenderer *meshRenderer = obj->createComponent<DTSceneObjectComponents::MeshRenderer>();

		meshRenderer->setMesh(mCache->cloudMesh[cloudIndex % CLOUD_TYPES]);
		meshRenderer->setMaterial(mCache->cloudMat);
		
		cloudIndex++;
	}

	glm::vec3 CloudManager::getCloudDestination(glm::vec3 pos)
	{
		glm::vec3 dir(mRandDistrib(mRandGenerator), mRandDistrib(mRandGenerator), mRandDistrib(mRandGenerator));

		return glm::max(glm::min(pos + (dir * CLOUD_DISTANCE), mAreaSize), -mAreaSize);
	}

	void CloudManager::initCache()
	{
		DTCore::ResourceManager &resMgr = DTCore::ResourceManager::getInstance();

		mCache->cloudMat = std::make_shared<DTMaterials::SimpleMaterial>();
		mCache->cloudMat->setDiffuseColor(DTCore::Color(1.0f, 1.0f, 1.0f));

		for (size_t i = 0; i < CLOUD_TYPES; ++i)
		{
			mCache->cloudMesh[i] = resMgr.loadMesh("./Data/Mesh/cloud"+std::to_string(i)+".fbx");
		}
	}

	glm::vec3 CloudManager::generateCloudPosition()
	{
		return glm::vec3(mRandDistrib(mRandGenerator), mRandDistrib(mRandGenerator), mRandDistrib(mRandGenerator)) * mAreaSize;
	}

}