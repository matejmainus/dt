#include <DTEngine/Core/SceneObject.h>
#include <glm/gtx/norm.hpp>

#include "Car.h"

namespace Benchmark
{
	Car::Car(DTCore::SceneObject &sceneObj, glm::vec3 destination) :
		SceneObjectComponent(sceneObj),
		mDest(destination)
	{
		mTransform = sceneObj.component<DTSceneObjectComponents::Transform>();
		mStart = mTransform->position();
	}

	Car::~Car()
	{
	}

	void Car::update(DTEngine::Millisecs deltaTime)
	{
		mTime += deltaTime;

		float factor = mTime.count() * 0.0003f;
		glm::vec3 pos = glm::mix(mStart, mDest, factor);
		
		mTransform->translateTo(pos);

		if (factor >= 1.0f)
		{
			mSceneObject.destroy();
			return;
		}
	}
}
