import subprocess
import time
import os
import shutil
import psutil
import csv
from datetime import datetime

runTime=60
gpu="0"
objs=[2000, 5000, 8000]

def runBenchmark(id, renderFlags = "0", appFlags = "0", threads = "5") :

	targetDir = "Results/"+id;

	if os.path.exists(targetDir) :
		shutil.rmtree(targetDir)
	
	os.makedirs(targetDir);

	for i in objs :
		procArgs = ["Benchmark.exe"]
		procArgs.append(renderFlags)
		procArgs.append(appFlags)
		procArgs.append(str(i))
		procArgs.append(threads)
		procArgs.append(gpu)

		print(procArgs)
		proc = subprocess.Popen(procArgs, shell=False, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

		csvFile = open(targetDir+"/"+str(i)+"_cpu_usage.csv", 'w', newline='')
		csvWriter = csv.writer(csvFile, delimiter=',')
			
		dt = datetime.now()
		psProc = psutil.Process(proc.pid)
		while (datetime.now() - dt).seconds < runTime :
			row = []

			cpuUsage = psutil.cpu_percent(interval=0.1, percpu = True)
			procCpuUsage = psProc.cpu_percent(interval=0.1) 
			
			row.append(procCpuUsage)
			row.extend(cpuUsage)
			csvWriter.writerow(row)

			time.sleep(0.1)

		proc.terminate()
		proc.wait()

		csvFile.flush()
		csvFile.close()

		os.rename("stats.csv", targetDir+"/"+str(i)+"_stats.csv")
		os.rename("usage.csv", targetDir+"/"+str(i)+"_usage.csv")

		time.sleep(2)

#runBenchmark("None", renderFlags="0", appFlags="1")
#runBenchmark("StBuffs", renderFlags="1", appFlags="1")
#runBenchmark("MemPool", renderFlags="3", appFlags="1")
#runBenchmark("Sort", renderFlags="7", appFlags="1")
#runBenchmark("PusConsts", renderFlags="11", appFlags="1")

runBenchmark("1Thread", renderFlags="11", appFlags="1", threads="1")
#runBenchmark("5Thread", renderFlags="11", appFlags="1")