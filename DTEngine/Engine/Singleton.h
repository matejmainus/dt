#pragma once

#include <cassert>

namespace DTEngine
{
	template<typename T>
	class Singleton
	{
	public:
		Singleton()
		{
			assert(sInstance == nullptr);
			sInstance = static_cast<T*>(this);
		}

		virtual ~Singleton()
		{}

		static inline void create()
		{
			new T();
		}

		static inline void destroy()
		{
			delete sInstance;
		}

		static inline T& getInstance()
		{
			return (*sInstance);
		}

		static inline T* getInstancePtr()
		{
			return sInstance;
		}

	protected:
		static T* sInstance;

	private:
		Singleton(const Singleton<T> &);
		Singleton& operator=(const Singleton<T> &);
	};

	template<typename T>
	T* Singleton<T>::sInstance = nullptr;
	
}