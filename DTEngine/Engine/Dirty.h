#pragma once

#include <mutex>

#include "DTEngine/Engine/Engine.h"

namespace DTEngine
{
	class Dirty
	{

	public:
		explicit Dirty() :
			mDirty(true)
		{}

		void markAsDirty()
		{
			mDirtyMutex.lock();

			mDirty = true;

			mDirtyMutex.unlock();
		}

	protected:
		mutable std::mutex mDirtyMutex;

		bool mDirty;
	};
}