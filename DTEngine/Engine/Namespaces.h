#pragma once

namespace DTEngine
{
	namespace Core
	{
		namespace SceneObjectComponents
		{}

		namespace Materials
		{}
	}

	namespace Renderer
	{}
}

namespace DTCore = DTEngine::Core;
namespace DTRenderer = DTEngine::Renderer;
namespace DTSceneObjectComponents = DTEngine::Core::SceneObjectComponents;
namespace DTMaterials = DTEngine::Core::Materials;
