#include <iostream>

#include "DTEngine/Engine/glm.h"

std::ostream &operator<< (std::ostream &out, const glm::vec3 &vec) {

	out << "[" << vec.x << " " << vec.y << " " << vec.z << "]";

	return out;
}

std::ostream &operator<< (std::ostream &out, const glm::mat4 &mat) {

	out << "[" << std::endl;

	for (glm::length_t y = 0; y < 4; ++y)
	{
		out << mat[y][0] << " " << mat[y][1] << " " << mat[y][0] << std::endl;
	}

	out << "]";

	return out;
}
