#pragma once

#include <mutex>
#include <memory>
#include <chrono>

namespace DTEngine
{
	typedef std::lock_guard<std::mutex> MutexGuard;
	typedef std::unique_lock<std::mutex> MutexUniqueLock;

	template<typename T>
	using SharedPtr = std::shared_ptr<T>;

	template<typename T>
	using WeakPtr = std::weak_ptr<T>;

	typedef std::chrono::duration<int, std::milli> Millisecs;
}