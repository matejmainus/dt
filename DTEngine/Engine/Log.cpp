#include <iostream>

#include "DTEngine/Engine/Engine.h"
#include "DTEngine/Engine/Log.h"

namespace DTEngine
{

	Log::Log()
	{
		mLogFile.open("error.log");
	}

	Log::~Log()
	{
		mLogFile.close();
	}

	void Log::log(const Log::Level level, const std::string &msg)
	{
		MutexGuard locker(mGuard);

		std::cerr << level << ": " << msg << std::endl;
		mLogFile << level << ": " << msg << std::endl;

		mLogFile.flush();
	}

	std::ostream& operator<<(std::ostream& os, Log::Level level)
	{
		switch (level)
		{
		case Log::Level::DEBUG:
			os << "Debug";
			break;
		case Log::Level::INFO:
			os << "Info";
			break;
		case Log::Level::ERR:
			os << "Error";
			break;
		default:
			os.setstate(std::ios_base::failbit);
			break;
		}

		return os;
	}

}