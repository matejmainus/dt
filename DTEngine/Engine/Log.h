#pragma once

#include <string>
#include <fstream>
#include <mutex>

#include "DTEngine/Engine/Singleton.h"

namespace DTEngine
{
	class Log : public Singleton<Log>
	{
	public:
		enum class Level
		{
			DEBUG,
			INFO,
			ERR,
		};

		explicit Log();
		~Log();
		
		static inline void debug(const std::string &msg)
		{
			sInstance->log(Log::Level::DEBUG, msg);
		}

		static inline void info(const std::string &msg)
		{
			sInstance->log(Log::Level::INFO, msg);
		}
		static inline void error(const std::string &msg)
		{
			sInstance->log(Log::Level::ERR, msg);
		}

		void log(const Log::Level level, const std::string &msg);
	private:
		std::mutex mGuard;
		std::ofstream mLogFile;
	};

	std::ostream& operator<<(std::ostream& os, Log::Level level);
}