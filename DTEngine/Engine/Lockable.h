#pragma once

#include <mutex>

namespace DTEngine
{
	class Lockable
	{
	public:
		inline void lock() { mGuard.lock(); }
		inline bool try_lock() { return mGuard.try_lock(); }
		inline void unlock() { mGuard.unlock(); }

		inline std::mutex& mutex() const { return mGuard; }

	protected:
		mutable std::mutex mGuard;
	};

	typedef std::lock_guard<Lockable> LockableGuard;
}