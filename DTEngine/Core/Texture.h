#pragma once

#include "DTEngine/Core/Prereq.h"

#include "DTEngine/Engine/Flags.h"
#include "DTEngine/Renderer/Entities/GPUTexture.h"

#include <FreeImage.h>

namespace DTEngine
{
	namespace Core
	{
		class Texture
		{
		public:

			enum class FlagBits
			{
				none = 0,
				hostOnly = 1
			};

			typedef Flags<Texture::FlagBits> Flags;

		public:
			explicit Texture(size_t textureId, FIBITMAP *bitmap, Texture::Flags flags = Texture::FlagBits::none);
			~Texture();
			
			size_t width() const;
			size_t height() const;

			inline size_t id() const { return mTextureId; }

			void texel(float x, float y, RGBQUAD *out) const;
			void texel(size_t x, size_t y, RGBQUAD *out) const;

			inline DTEngine::Renderer::Entities::GPUTexture* gpuTexture() const { return mGpuTexture; }

		private:
			friend class DTEngine::Renderer::Entities::GPUTexture;

			size_t mTextureId;
			FIBITMAP *mBitmap;

			DTEngine::Renderer::Entities::GPUTexture *mGpuTexture;
		};
	}
}