#pragma once

#include <atomic>

#include "DTEngine/Core/Prereq.h"

namespace DTEngine
{
	namespace Core
	{
		class SceneObject;

		class SceneObjectComponent
		{
		public:
			explicit SceneObjectComponent(SceneObject &sceneObj);
			virtual ~SceneObjectComponent();

			inline SceneObject& sceneObject() { return mSceneObject; }

			void destroy();

			bool active() const;
			void setActive(bool active);

			virtual void update(Millisecs time);

		protected:
			SceneObject &mSceneObject;

			std::atomic<bool> mActive;
		};

	}
}