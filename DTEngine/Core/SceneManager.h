#pragma once

#include <vector>
#include <map>

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Engine/Singleton.h"

namespace DTEngine
{
	namespace Core
	{
		class Scene;

		class SceneManager : public Singleton<SceneManager>
		{
		public:
			typedef std::map<std::string, Scene*> ScenesMap;

		public:
			explicit SceneManager();
			~SceneManager();

			Scene* createScene(const std::string &id);
			Scene* getScene(const std::string &id) const;
			void removeScene(Scene* scene);
			void scenes(std::vector<Scene *> &scenes) const;

			void updateScenes(Millisecs time);
			void postUpdateScenes();

			void init();
			void shutdown();

		private:
			mutable std::mutex mScenesMutex;

			ScenesMap mScenes;
		};
	}
}

