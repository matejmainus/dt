#pragma once

#include <thread>
#include <vector>
#include <atomic>

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Core/Color.h"

namespace DTEngine
{
	namespace Core
	{
		class SceneObject;

		class Scene 
		{
		public:
			explicit Scene(const std::string &id);
			~Scene();

			const std::string getId() const;

			bool active() const;
			void setActive(bool active);

			void setAsMain();

			inline SceneObject& rootObject() const { return *mRootObject; }

			inline Color& backgroundColor() { return mBackgroundColor; }
			void setBackgroundColor(Color &color);

		private:

		private:
			std::string mId;
		
			std::atomic<bool> mActive;

			SceneObject *mRootObject;

			Color mBackgroundColor;
		};

	}
}
