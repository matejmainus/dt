#include <SDL.h>
#include <algorithm>

#include "DTEngine/Engine/Log.h"

#include "DTEngine/Core/EventManager.h"
#include "DTEngine/Core/EventHandler.h"
#include "DTEngine/Core/Kernel.h"

namespace DTEngine
{
namespace Core
{
	EventManager::EventManager() :
		Singleton<EventManager>()
	{
		Log::info("EventManager created");
	}

	EventManager::~EventManager()
	{
		Log::info("EventManager destroyed");
	}

	void EventManager::init()
	{
		if (SDL_InitSubSystem(SDL_INIT_EVENTS) != 0)
			throw std::runtime_error("Could not init SDL events subsystem");
	}

	void EventManager::shutdown()
	{
		SDL_QuitSubSystem(SDL_INIT_EVENTS);
	}

	void EventManager::addHandler(EventHandler * handler)
	{
		MutexGuard locker(mHandlersMutex);

		mHandlers.push_back(handler);
	}

	void EventManager::removeHandler(EventHandler * handler)
	{
		MutexGuard locker(mHandlersMutex);

		std::remove_if(mHandlers.begin(), mHandlers.end(), [handler](const EventHandler *item) {
			return &handler == &item;
		});
	}

	void EventManager::processEvents()
	{
		MutexGuard locker(mHandlersMutex);

		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				Kernel::getInstance().stop();
				break;
			}

			for (EventHandler* const &eventHandler : mHandlers)
			{
				if (eventHandler->handleEvent(&event))
					break;
			}
		}
	}

}
}

