#include "DTEngine/Core/Mesh.h"

namespace DTEngine
{
namespace Core
{

	Mesh::Mesh(size_t meshId, aiMesh *mesh) :
		mAiMesh(mesh),
		mMeshId(meshId),
		mGPUMesh(nullptr)
	{

	//	createTriangle();

		mGPUMesh = new DTEngine::Renderer::Entities::GPUMesh(*this);
		mGPUMesh->markAsDirty();
	}

	Mesh::~Mesh()
	{
		if (mGPUMesh)
			delete mGPUMesh;

	//	if(mAiMesh)
//			delete mAiMesh;
	}

	void Mesh::createTriangle()
	{
		mAiMesh = new aiMesh();
		mAiMesh->mNumVertices = 3;
		mAiMesh->mVertices = new aiVector3D[3]{
			{0.5f, -0.5f, 0.0f}, 
			{-0.5f, 0.5f, 0.0f}, 
			{-0.5f, -0.5f, 0.0f} };

		mAiMesh->mNumFaces = 1;
		mAiMesh->mFaces = new aiFace[1];

		mAiMesh->mFaces->mNumIndices = 3;
		mAiMesh->mFaces->mIndices = new unsigned int[3] {0,1,2};
	}
}
}