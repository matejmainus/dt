#pragma once

#include <atomic>

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Engine/Singleton.h"

namespace DTEngine
{
	namespace Core
	{
		class App;

		class Kernel : public Singleton<Kernel>
		{
		public:
			explicit Kernel(App &app);
			~Kernel();

			void run();

			bool running();
			void stop();

			const App& app() const;

		private:
			void init();
			void shutdown();

		private:
			App &mApp;

			std::atomic<bool> mRun;
		};
	}
}
