#include "DTEngine/Core/SceneObjectComponent.h"
#include "DTEngine/Core/SceneObject.h"

namespace DTEngine
{
namespace Core
{

	SceneObjectComponent::SceneObjectComponent(SceneObject &sceneObj) :
		mSceneObject(sceneObj),
		mActive(true)
	{
	}

	SceneObjectComponent::~SceneObjectComponent()
	{
	}

	void SceneObjectComponent::update(Millisecs time)
	{
	}

	void SceneObjectComponent::destroy()
	{
		mSceneObject.removeComponent(this);
	}

	bool SceneObjectComponent::active() const
	{
		return mActive;
	}

	void SceneObjectComponent::setActive(bool active)
	{
		mActive = active;
	}

}		
}