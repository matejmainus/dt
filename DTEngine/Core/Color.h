#pragma once

#include "DTEngine/Engine/glm.h"

#include "DTEngine/Core/Prereq.h"

namespace DTEngine
{
	namespace Core
	{
		class Color
		{
		public:
			explicit Color();
			explicit Color(int r, int g, int b, int a = 255);
			explicit Color(float r, float g, float b, float a = 1.0f);
			explicit Color(glm::vec4 vec);
			~Color();

			inline glm::vec4 intVec() const { return mColor * 255.0f; }
			inline glm::vec4 normVec() const { return mColor; }

		private:
			glm::vec4 mColor;
		};


	}
}
