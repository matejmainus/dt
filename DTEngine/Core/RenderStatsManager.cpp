#include "RenderStatsManager.h"

namespace DTEngine
{
namespace Core
{
	void RenderStatsManager::FrameStats::clear()
	{
		//memset(this, 0, sizeof(RenderStatsManager::FrameStats));

		frameTime = DTEngine::Millisecs(0);
		drawTime = DTEngine::Millisecs(0);
		updateTime = DTEngine::Millisecs(0);
		gpuFrameTime = DTEngine::Millisecs(0);

		cpuUsage = 0;

		drawCallCount = 0;
		copyCallCount = 0;
		memoryMapCount = 0;
		pipelineBindCount = 0;
		descriptorUpdateCount = 0;
		descriptorCopyCount = 0;
		descriptorBindCount = 0;
		queueSubmitCount = 0;
	}

	RenderStatsManager::RenderStatsManager()
	{
		mCurrentFrame = new FrameStats();
		mLastFrame = new FrameStats();

		mUsageStats = new UsageStats();
	}

	RenderStatsManager::~RenderStatsManager()
	{
	}

	void RenderStatsManager::swap()
	{
		FrameStats *tmp = mLastFrame;
		mLastFrame = mCurrentFrame;
		mCurrentFrame = tmp;

		mCurrentFrame->clear();
	}

	/*UsageStats*/
}
}