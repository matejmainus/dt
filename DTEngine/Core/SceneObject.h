#pragma once

#include <vector>
#include <algorithm>
#include <atomic>

#include "DTEngine/Core/Prereq.h"

namespace DTEngine
{
	namespace Core
	{
		namespace SceneObjectComponents
		{
			class Transform;
		}
		
		class SceneObjectComponent;
		class Scene;

		class SceneObject
		{
		public:
			typedef std::vector<SceneObjectComponent *> SceneObjectComponentVec;
			typedef std::vector<SceneObject *> ChildsVec;

		public:
			explicit SceneObject(Scene &scene, SceneObject *parent, const std::string &name = "");
			~SceneObject();

			const std::string& name() const;

			void setParent(SceneObject *parent);
			void destroy();

			bool active() const;
			void setActive(bool active);

			void update(Millisecs time);
			void postUpdate();

			void components(std::vector<SceneObjectComponent *> &components);
			void childs(std::vector<SceneObject *> &childs) const;

			template<typename T>
			T* createComponent();

			template<typename T, typename... Args> 
			T* createComponent(Args &&... args);

			template<typename T>
			T* component() const;

			template<>
			inline SceneObjectComponents::Transform* component() const
			{
				return mTransform;
			}

			template<typename T>
			void components(std::vector<T *> &components);

			SceneObject* createChild(const std::string &name = "");

			void removeChild(SceneObject* child);
			void removeComponent(SceneObjectComponent* component);

		private:
			template<typename T>
			void addComponent(T* component);

		private:
			mutable std::mutex mComponentsMutex;
			mutable std::mutex mChildsMutex;

			Scene &mScene;
			SceneObject *mParent;

			std::string mName;
			std::atomic<bool> mActive;

			SceneObjectComponents::Transform* mTransform;

			SceneObjectComponentVec mComponents;
			ChildsVec mChilds;

			ChildsVec mRemovedChilds;
			SceneObjectComponentVec mRemovedComponents;
		};

		template<typename T>
		inline T* SceneObject::createComponent()
		{
			T* component = new T(*this);

			addComponent(component);

			return component;
		}

		template<typename T, typename... Args>
		inline T* SceneObject::createComponent(Args &&... args)
		{
			T* component = new T(*this, args...);

			addComponent(component);

			return component;
		}

		template<typename T>
		inline T* SceneObject::component() const
		{
			mComponentsMutex.lock();

			T *ptr;
			for (SceneObjectComponent* const &soc : mComponents)
			{
				ptr = dynamic_cast<T *>(soc);
				if (ptr)
				{
					mComponentsMutex.unlock();
					return ptr;
				}
			}

			mComponentsMutex.unlock();

			throw new std::runtime_error("Scene object does not contain requested component");
		}

		template<typename T>
		inline void SceneObject::addComponent(T* component)
		{
			mComponentsMutex.lock();

			mComponents.push_back(component);

			mComponentsMutex.unlock();
		}

		template<typename T>
		inline void SceneObject::components(std::vector<T*> &components)
		{
			mComponentsMutex.lock();

			T* ptr;
			for (SceneObjectComponent* const &soc : mComponents)
			{
				ptr = dynamic_cast<T *>(soc);
				if (ptr)
					components.push_back(ptr);
			}

			mComponentsMutex.unlock();
		}

	}
}

