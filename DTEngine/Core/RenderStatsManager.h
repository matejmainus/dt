#pragma once

#include "DTEngine/Core/Prereq.h"

#include "DTEngine/Engine/Singleton.h"
#include "DTEngine/Engine/Lockable.h"

#include <atomic>

namespace DTEngine
{
	namespace Core
	{
		class RenderStatsManager : public Singleton<RenderStatsManager>
		{
		public:
			struct FrameStats
			{
				void clear();

				DTEngine::Millisecs frameTime;

				DTEngine::Millisecs updateTime;
				DTEngine::Millisecs drawTime;

				DTEngine::Millisecs gpuFrameTime;
				size_t cpuUsage;

				std::atomic<size_t> drawCallCount;
				std::atomic<size_t> copyCallCount;
				std::atomic<size_t> memoryMapCount;
				std::atomic<size_t> pipelineBindCount;
				std::atomic<size_t> descriptorUpdateCount;
				std::atomic<size_t> descriptorCopyCount;
				std::atomic<size_t> descriptorBindCount;
				std::atomic<size_t> queueSubmitCount;
			};

			struct UsageStats
			{
				std::atomic<size_t> gpuMemoryAllocSize;
				std::atomic<size_t> descriptorSetCount;
				std::atomic<size_t> gpuBufferCount;
				std::atomic<size_t> gpuImageCount;
			};

		public:
			explicit RenderStatsManager();
			~RenderStatsManager();

			inline FrameStats* currentFrame() const { return mCurrentFrame; }
			inline FrameStats* lastFrame() const { return mLastFrame; }

			inline UsageStats* usageStats() const { return mUsageStats; }

			void swap();

		private:
			FrameStats* mLastFrame;
			FrameStats* mCurrentFrame;

			UsageStats* mUsageStats;
		};
	}
}
