#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <FreeImage.h>

#include "DTEngine/Core/ResourceManager.h"
#include "DTEngine/Core/Mesh.h"
#include "DTEngine/Core/Texture.h"

namespace DTEngine
{
	namespace Core
	{
		ResourceManager::ResourceManager() :
			Singleton(),
			mMeshId(0),
			mTextureId(0)
		{
		}

		ResourceManager::~ResourceManager()
		{
		}

		void ResourceManager::init()
		{
		}

		void ResourceManager::shutdown()
		{
			mMeshes.clear();
			mTextures.clear();
		}

		SharedPtr<Mesh> ResourceManager::loadMesh(const std::string & path)
		{
			MutexGuard locker(mMeshesMutex);

			SharedPtr<Mesh> mesh;

			MeshesMap::iterator it = mMeshes.find(path);
			if (it != mMeshes.end())
			{
				mesh = it->second;
				if (mesh)
					return mesh;
			}

			Assimp::Importer importer;

			importer.ReadFile(path,
				aiProcess_CalcTangentSpace | 
				aiProcess_FlipWindingOrder |
				aiProcess_PreTransformVertices |
				aiProcess_Triangulate);
			
			aiScene* scene = importer.GetOrphanedScene();

			if ((!scene) || scene->mNumMeshes == 0)
				throw std::runtime_error(std::string(importer.GetErrorString())+" :"+path);

			mesh = std::make_shared<Mesh>(mMeshId, scene->mMeshes[0]);

			mMeshes[path] = mesh;
			mMeshId++;

			return mesh;
		}

		SharedPtr<Texture> ResourceManager::loadTexture(const std::string & path)
		{
			MutexGuard locker(mTexturesMutex);

			SharedPtr<Texture> texture;

			TexturesMap::iterator it = mTextures.find(path);
			if (it != mTextures.end())
			{
				texture = it->second;
				if (texture)
					return texture;
			}

			FREE_IMAGE_FORMAT format = FreeImage_GetFIFFromFilename(path.c_str());

			FIBITMAP *bitmapRaw = FreeImage_Load(format, path.c_str());
			if(bitmapRaw == NULL)
				throw std::runtime_error("Texture could not be loaded: "+path);

			texture = std::make_shared<Texture>(mTextureId, bitmapRaw);

			mTextures[path] = texture;
			mTextureId++;

			return texture;
		}

	}
}