#pragma once

#include <SDL.h>

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Engine/Singleton.h"

namespace DTEngine
{
	namespace Core
	{
		class InputManager : public Singleton<InputManager>
		{
		public:
			explicit InputManager();
			~InputManager();

			bool keyState(SDL_Scancode keyCode) const;
			bool mouseButtonState(unsigned char buttonCode) const;

			void lockMouse(bool lock);

			void mousePosition(int *x, int *y) const;
			void mouseRelativePosition(int *x, int *y) const;
		};
	}

}