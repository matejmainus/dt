#include "DTEngine/Core/Viewport.h"
#include "DTEngine/Core/SceneObject.h"
#include "DTEngine/Core/SceneObjectComponents/Transform.h"

#include "DTEngine/Renderer/Compositor.h"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace DTEngine
{
namespace Core
{
	Viewport::Viewport(SceneObject &sceneObject,
			float fov, float ratio, float near, float far) :
		mSceneObject(sceneObject),
		mFov(fov), mAspectRatio(ratio), mNear(near), mFar(far)
	{
		mGPUViewport = new DTEngine::Renderer::Entities::GPUViewport(*this);
	}

	Viewport::~Viewport()
	{
		if(mGPUViewport)
			delete mGPUViewport;
	}

	void Viewport::setRenderTarget(const SharedPtr<DTEngine::Renderer::RenderTarget>& target)
	{
		mRenderTarget = target;
	}

	void Viewport::setAsMain()
	{
		DTEngine::Renderer::Compositor::getInstance().setActiveViewport(this);
	}

	glm::mat4 Viewport::viewMatrix() const
	{
		SceneObjectComponents::Transform *t = mSceneObject.component<DTEngine::Core::SceneObjectComponents::Transform>();

		return glm::toMat4(t->rotation()) * glm::translate(t->position());
	}

	glm::mat4 Viewport::projectionMatrix() const
	{
		return glm::perspective(mFov, mAspectRatio, mNear, mFar);
	}
}
}