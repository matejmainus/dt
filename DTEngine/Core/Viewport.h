#pragma once

#include "DTEngine/Engine/glm.h"

#include "DTEngine/Core/Prereq.h"

#include "DTEngine/Renderer/RenderTarget.h"
#include "DTEngine/Renderer/Entities/GPUViewport.h"

namespace DTEngine
{
	namespace Core
	{
		class SceneObject;

		class Viewport
		{
		public:
			explicit Viewport(SceneObject &sceneObject, 
				float fov, float ratio, float near, float far);
			~Viewport();

			inline SceneObject& sceneObject() { return mSceneObject; }

			void setRenderTarget(const SharedPtr<DTEngine::Renderer::RenderTarget> &target);
			inline const SharedPtr<DTEngine::Renderer::RenderTarget>& renderTarget() const {
				return mRenderTarget;
			}

			void setAsMain();

			inline DTEngine::Renderer::Entities::GPUViewport* gpuViewport() const { return mGPUViewport; }

			glm::mat4 viewMatrix() const;
			glm::mat4 projectionMatrix() const;

		private:
			friend class DTEngine::Renderer::Entities::GPUViewport;

			SceneObject &mSceneObject;

			SharedPtr<DTEngine::Renderer::RenderTarget> mRenderTarget;

			float mFov;
			float mAspectRatio;
			float mNear;
			float mFar;

			DTEngine::Renderer::Entities::GPUViewport *mGPUViewport;
		};
	}
}