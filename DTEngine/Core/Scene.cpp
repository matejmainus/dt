#include <SDL.h>
#include <chrono>
#include <iostream>
#include <algorithm>
#include <assert.h>

#include "DTEngine/Core/Scene.h"
#include "DTEngine/Core/SceneObject.h"

#include "DTEngine/Renderer/Compositor.h"

namespace DTEngine
{
namespace Core
{

	static const size_t MAX_GPU_LIGHTS = 10;

	Scene::Scene(const std::string &id) :
		mId(id),
		mActive(true),
		mBackgroundColor()
	{
		mRootObject = new SceneObject(*this, nullptr, "Root");
	}

	Scene::~Scene()
	{
		delete mRootObject;
	}

	const std::string Scene::getId() const
	{
		return mId;
	}

	bool Scene::active() const
	{
		return mActive;
	}

	void Scene::setActive(bool active)
	{
		mActive = active;
	}

	void Scene::setAsMain()
	{
		DTEngine::Renderer::Compositor::getInstance().setActiveScene(this);
	}

	void Scene::setBackgroundColor(Color& color)
	{
		mBackgroundColor = color;
	}
}
}
