#pragma once

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Renderer/GPURenderer.h"

namespace DTEngine
{
	namespace Core
	{
		class Renderer
		{
		public:
			explicit Renderer() {}
			virtual ~Renderer() {}

			inline DTEngine::Renderer::GPURenderer* gpuRenderer() const { return mGpuRenderer; }

		protected:
			DTEngine::Renderer::GPURenderer *mGpuRenderer;
		};

	}
}