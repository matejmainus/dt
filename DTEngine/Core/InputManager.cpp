#include "DTEngine/Engine/Log.h"

#include "DTEngine/Core/InputManager.h"

namespace DTEngine
{
namespace Core
{

	InputManager::InputManager() :
		Singleton<InputManager>()
	{
		Log::info("InputManager created");
	}

	InputManager::~InputManager()
	{
		Log::info("InputManager destroyed");
	}

	bool InputManager::keyState(SDL_Scancode keyCode) const
	{
		const Uint8 *mask = SDL_GetKeyboardState(nullptr);

		return mask[keyCode] == 1;

		return true;
	}

	bool InputManager::mouseButtonState(unsigned char buttonCode) const
	{
		Uint32 mask = SDL_GetMouseState(nullptr, nullptr);

		return (mask & SDL_BUTTON(buttonCode)) == 1;
		return 0;
	}

	void InputManager::lockMouse(bool lock)
	{
		SDL_SetRelativeMouseMode(lock ? SDL_TRUE : SDL_FALSE);
	}

	void InputManager::mousePosition(int *x, int *y) const
	{
		SDL_GetMouseState(x, y);
	}

	void InputManager::mouseRelativePosition(int * x, int * y) const
	{
		SDL_GetRelativeMouseState(x, y);
	}

}
}