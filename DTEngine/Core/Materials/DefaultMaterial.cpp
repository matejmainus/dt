#include "DTEngine/Core/Materials/DefaultMaterial.h"

#include "DTEngine/Renderer/Materials/DefaultGPUMaterial.h"

#define MATERIAL_ID 1

namespace DTEngine
{
namespace Core
{
namespace Materials
{

	DefaultMaterial::DefaultMaterial() :
		Material(MATERIAL_ID),
		mDiffuseColor(),
		mDiffuseTexture(nullptr)
	{
		mGpuMaterial = new Renderer::Materials::DefaultGPUMaterial(*this);
	}


	DefaultMaterial::~DefaultMaterial()
	{
	}

	void DefaultMaterial::setDiffuseTexture(const SharedPtr<Texture>& texture)
	{
		mDiffuseTexture = texture;

		mGpuMaterial->markAsDirty();
	}

	void DefaultMaterial::setDiffuseColor(Color& diffuseColor)
	{
		mDiffuseColor = diffuseColor;

		mGpuMaterial->markAsDirty();
	}

}
}
}