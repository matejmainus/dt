#pragma once

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Core/Material.h"
#include "DTEngine/Core/Color.h"

#include "DTEngine/Renderer/Materials/SimpleGPUMaterial.h"

namespace DTEngine
{
	namespace Core
	{
		namespace Materials
		{
			class SimpleMaterial : public Core::Material
			{
			public:
				explicit SimpleMaterial();
				virtual ~SimpleMaterial();

				Color& diffuseColor() { return mDiffuseColor; }
				void setDiffuseColor(Color& diffuseColor);

			private:

				friend class DTEngine::Renderer::Materials::SimpleGPUMaterial;

				Color mDiffuseColor;
			};
		}
	}
}