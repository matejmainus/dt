#include "DTEngine/Core/Materials/SimpleMaterial.h"

#include "DTEngine/Renderer/Materials/SimpleGPUMaterial.h"

#define MATERIAL_ID 2

namespace DTEngine
{
namespace Core
{
namespace Materials
{

	SimpleMaterial::SimpleMaterial() :
		Material(MATERIAL_ID),
		mDiffuseColor()
	{
		mGpuMaterial = new Renderer::Materials::SimpleGPUMaterial(*this);
	}


	SimpleMaterial::~SimpleMaterial()
	{
	}

	void SimpleMaterial::setDiffuseColor(Color & diffuseColor)
	{
		mDiffuseColor = diffuseColor;

		mGpuMaterial->markAsDirty();
	}

}
}
}