#pragma once

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Core/Material.h"
#include "DTEngine/Core/Color.h"

#include "DTEngine/Renderer/Materials/DefaultGPUMaterial.h"

namespace DTEngine
{
	namespace Core
	{
		class Texture;

		namespace Materials
		{
			class DefaultMaterial : public Core::Material
			{
			public:
				explicit DefaultMaterial();
				virtual ~DefaultMaterial();

				const SharedPtr<Texture>& diffuseTexture() const { return mDiffuseTexture; }
				void setDiffuseTexture(const SharedPtr<Texture>& texture);

				Color& diffuseColor() { return mDiffuseColor; }
				void setDiffuseColor(Color& diffuseColor);

			private:

				friend class DTEngine::Renderer::Materials::DefaultGPUMaterial;

				SharedPtr<Texture> mDiffuseTexture;
				Color mDiffuseColor;
			};
		}
	}
}