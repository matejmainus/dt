#pragma once

#include <vector>

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Engine/Singleton.h"

namespace DTEngine
{
	namespace Core
	{
		class EventHandler;

		class EventManager : public Singleton<EventManager>
		{
		public:
			typedef std::vector<EventHandler *> EventHandlersVec;

		public:
			explicit EventManager();
			~EventManager();

			void init();
			void shutdown();

			void addHandler(EventHandler *handler);
			void removeHandler(EventHandler *handler);

			void processEvents();

		private:
			mutable std::mutex mHandlersMutex;

			EventHandlersVec mHandlers;
		};

	}
}


