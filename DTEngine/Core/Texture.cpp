#include "Texture.h"

namespace DTEngine
{
namespace Core
{
	Texture::Texture(size_t textureId, FIBITMAP *bitmap, Texture::Flags flags) :
		mBitmap(bitmap),
		mTextureId(mTextureId),
		mGpuTexture(nullptr)
	{

		if (!(flags & Texture::FlagBits::hostOnly))
		{
			mGpuTexture = new Renderer::Entities::GPUTexture(*this);
		}
	}

	Texture::~Texture()
	{
		if (mGpuTexture)
			delete mGpuTexture;

		if(mBitmap)
			FreeImage_Unload(mBitmap);
	}

	size_t Texture::width() const
	{
		return FreeImage_GetWidth(mBitmap);
	}

	size_t Texture::height() const
	{
		return FreeImage_GetHeight(mBitmap);
	}

	void Texture::texel(float x, float y, RGBQUAD *out) const
	{
		FreeImage_GetPixelColor(mBitmap, (unsigned int) (x * width()), (unsigned int) (y * height()), out);
	}

	void Texture::texel(size_t x, size_t y, RGBQUAD * out) const
	{
		FreeImage_GetPixelColor(mBitmap, (unsigned int) x, (unsigned int) y, out);
	}

}
}
