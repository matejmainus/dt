#pragma once

#include <assimp/scene.h>
#include <assimp/mesh.h>

#include "DTEngine/Core/Prereq.h"

#include "DTEngine/Renderer/Entities/GPUMesh.h"

namespace DTEngine
{
	namespace Core
	{
		class Mesh
		{
		public:
			explicit Mesh(size_t meshId, aiMesh *mesh);
			~Mesh();

			inline size_t verticesCount() const { return mAiMesh->mNumVertices; }
			inline size_t indexiesCount() const { return mAiMesh->mNumFaces * mAiMesh->mFaces->mNumIndices; }
			inline size_t facesCount() const { return mAiMesh->mNumFaces; }
			inline size_t faceIndexiesCount() const { return  mAiMesh->mFaces->mNumIndices; }

			inline size_t id() const { return mMeshId; }

			inline DTEngine::Renderer::Entities::GPUMesh* gpuMesh() const { return mGPUMesh; }

		private:
			void createTriangle();

		private:
			friend class DTEngine::Renderer::Entities::GPUMesh;

			size_t mMeshId;
			aiMesh *mAiMesh;

			DTEngine::Renderer::Entities::GPUMesh *mGPUMesh;
		};

	}
}