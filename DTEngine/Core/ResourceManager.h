#pragma once

#include <map>

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Engine/Singleton.h"

namespace DTEngine
{
	namespace Core
	{
		class Mesh;
		class Texture;

		class ResourceManager : public Singleton<ResourceManager>
		{
		public:
			explicit ResourceManager();
			~ResourceManager();

			void init();
			void shutdown();

			SharedPtr<Mesh> loadMesh(const std::string &path);
			SharedPtr<Texture> loadTexture(const std::string &path);

		private:
			typedef std::map<std::string, SharedPtr<Mesh>> MeshesMap;
			typedef std::map<std::string, SharedPtr<Texture>> TexturesMap;

		private:
			mutable std::mutex mMeshesMutex;
			mutable std::mutex mTexturesMutex;

			size_t mMeshId;
			MeshesMap mMeshes;

			size_t mTextureId;
			TexturesMap mTextures;
		};
	}
}

