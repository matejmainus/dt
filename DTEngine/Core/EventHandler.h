#pragma once

#include <SDL.h>

#include "DTEngine/Core/Prereq.h"

namespace DTEngine
{
	namespace Core
	{
		class EventHandler
		{
		public:
			virtual bool handleEvent(const SDL_Event *evt) = 0;
		};
	}
}

