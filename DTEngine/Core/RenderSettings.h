#pragma once

#include "DTEngine/Core/Prereq.h"

#include "DTEngine/Engine/Singleton.h"
#include "DTEngine/Engine/Flags.h"

namespace DTEngine
{
	namespace Core
	{
		class RenderSettings : public Singleton<RenderSettings>
		{
		public:
			enum class FlagBits
			{
				none = 0,
				stagingBuffers = 1,
				memoryPools = 2,
				treeSort = 4,
				pushConstants = 8
			};

			typedef Flags<RenderSettings::FlagBits> Flags;
			
		public:
			explicit RenderSettings();
			~RenderSettings();

			void setFlags(RenderSettings::Flags flags);
			inline RenderSettings::Flags flags() const { return mFlags; }

			void setRenderThreads(size_t count);
			inline size_t renderThreadsCount() const { return mRenderThreadCount; }

			void setSelectedGpu(size_t pos);
			inline size_t selectedGpu() const { return mSelectedGPU; };

			inline bool useStagingBuffers() const { return mFlags & FlagBits::stagingBuffers; }
			inline bool useMemoryPools() const { return mFlags & FlagBits::memoryPools; }
			inline bool useTreeSort() const { return mFlags & FlagBits::treeSort; }
			inline bool usePushConstants() const { return mFlags & FlagBits::pushConstants; }

		private:
			Flags mFlags;

			size_t mRenderThreadCount;
			size_t mSelectedGPU;
		};

		RenderSettings::Flags operator|(RenderSettings::FlagBits bit0, RenderSettings::FlagBits bit1);
	}
}


