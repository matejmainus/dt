#include <algorithm>

#include "DTEngine/Core/SceneObject.h"
#include "DTEngine/Core/SceneObjectComponent.h"
#include "DTEngine/Core/SceneObjectComponents/Transform.h"

namespace DTEngine 
{
namespace Core
{

	SceneObject::SceneObject(Scene &scene, SceneObject *parent, const std::string &name) :
		mName(name),
		mScene(scene),
		mParent(parent),
		mActive(true)
	{
		mTransform = new SceneObjectComponents::Transform(*this);
	}

	SceneObject::~SceneObject()
	{
		for(SceneObjectComponent* const &component : mComponents)
			delete component;

		for (SceneObject* const &child : mChilds)
			delete child;

		mComponents.clear();
		mChilds.clear();
	}

	const std::string & SceneObject::name() const
	{
		return mName;
	}

	void SceneObject::setParent(SceneObject *parent)
	{
		MutexGuard locker(mChildsMutex);

		if(mParent)
		{
			MutexGuard locker(mParent->mChildsMutex);

			mChilds.erase(std::remove(mParent->mChilds.begin(), mParent->mChilds.end(), this));
		}

		mParent = parent;
		mParent->mChilds.push_back(this);
	}

	void SceneObject::destroy()
	{
		if (mParent)
			mParent->removeChild(this);
	}

	void SceneObject::components(std::vector<SceneObjectComponent *> &components)
	{
		mComponentsMutex.lock();

		components = mComponents;

		mComponentsMutex.unlock();
	}

	SceneObject* SceneObject::createChild(const std::string &name)
	{
		SceneObject* obj = new SceneObject(mScene, this, name);

		mChildsMutex.lock();

		mChilds.push_back(obj);

		mChildsMutex.unlock();

		return obj;
	}

	void SceneObject::removeChild(SceneObject* child)
	{
		mChildsMutex.lock();

		mRemovedChilds.push_back(child);

		mChildsMutex.unlock();
	}

	void SceneObject::removeComponent(SceneObjectComponent* component)
	{
		mComponentsMutex.lock();

		mRemovedComponents.push_back(component);

		mComponentsMutex.unlock();
	}

	void SceneObject::childs(std::vector<SceneObject*> &childs) const
	{
		mChildsMutex.lock();

		childs = mChilds;

		mChildsMutex.unlock();
	}

	void SceneObject::postUpdate()
	{
		
		mComponentsMutex.lock();

		for (SceneObjectComponent* const &compToRemove : mRemovedComponents)
		{
			mComponents.erase(std::remove(mComponents.begin(), mComponents.end(), compToRemove));

			delete compToRemove;
		}

		mRemovedComponents.clear();

		mComponentsMutex.unlock();
	
		mComponentsMutex.lock();

		for (SceneObject* const &childToRemove : mRemovedChilds)
		{
			mChilds.erase(std::remove(mChilds.begin(), mChilds.end(), childToRemove));

			delete childToRemove;
		}

		mRemovedChilds.clear();

		for (SceneObject* const &child : mChilds)
		{
			child->postUpdate();
		}
			
		mComponentsMutex.unlock();
	}

	void SceneObject::update(Millisecs time)
	{
		{
			SceneObjectComponentVec componentsCopy;
			components(componentsCopy);

			for (SceneObjectComponent* const &component : componentsCopy)
			{
				if(component->active())
					component->update(time);
			}
		}

		{
			ChildsVec childsCopy;
			childs(childsCopy);

			for (SceneObject* const &child : childsCopy)
			{
				if(child->active())
					child->update(time);
			}
		}
	}

	bool SceneObject::active() const
	{
		return mActive;
	}

	void SceneObject::setActive(bool active)
	{
		mActive = active;
	}

}
}