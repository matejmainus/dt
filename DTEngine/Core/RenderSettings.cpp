#include "RenderSettings.h"

namespace DTEngine
{
	namespace Core
	{
		RenderSettings::RenderSettings() :
			mFlags(FlagBits::none),
			mRenderThreadCount(std::thread::hardware_concurrency() + 1),
			mSelectedGPU(0)
		{
		}


		RenderSettings::~RenderSettings()
		{
		}

		void RenderSettings::setFlags(Flags flags)
		{
			mFlags = flags;
		}

		void RenderSettings::setRenderThreads(size_t count)
		{
			mRenderThreadCount = count;
		}

		void RenderSettings::setSelectedGpu(size_t pos)
		{
			mSelectedGPU = pos;
		}

		RenderSettings::Flags operator|(RenderSettings::FlagBits bit0, RenderSettings::FlagBits bit1)
		{
			return RenderSettings::Flags(bit0) | bit1;
		}
	}
}