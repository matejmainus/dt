#include <iostream>

#include "DTEngine/Core/SceneManager.h"
#include "DTEngine/Engine/Log.h"
#include "DTEngine/Core/Scene.h"
#include "DTEngine/Core/SceneObject.h"

namespace DTEngine
{ 
namespace Core
{ 

	SceneManager::SceneManager() :
		Singleton<SceneManager>()
	{
		Log::info("SceneManager created");
	}

	SceneManager::~SceneManager()
	{
		Log::info("SceneManager destroyed");
	}

	void SceneManager::init()
	{
	}

	void SceneManager::shutdown()
	{
		mScenes.clear();
	}

	Scene* SceneManager::createScene(const std::string &id)
	{
		Scene* scene = new Scene(id);

		MutexGuard locker(mScenesMutex);

		mScenes[id] = scene;

		return scene;
	}

	Scene* SceneManager::getScene(const std::string &id) const
	{
		MutexGuard locker(mScenesMutex);

		return mScenes.at(id);
	}

	void SceneManager::removeScene(Scene* scene)
	{
		MutexGuard locker(mScenesMutex);

		mScenes.erase(scene->getId());

		delete scene;
	}

	void SceneManager::scenes(std::vector<Scene*>& scenes) const
	{
		MutexGuard lock(mScenesMutex);

		for (const std::pair<std::string, Scene *> &scene : mScenes)
			scenes.push_back(scene.second);
	}

	void SceneManager::updateScenes(Millisecs time)
	{
		std::vector<Scene *> scenesCopy;
		scenes(scenesCopy);

		for (Scene* const &scene : scenesCopy)
		{
			if (scene->active())
				scene->rootObject().update(time);
		}
	}

	void SceneManager::postUpdateScenes()
	{
		std::vector<Scene *> scenesCopy;
		scenes(scenesCopy);

		for (Scene* const &scene : scenesCopy)
		{
			if (scene->active())
				scene->rootObject().postUpdate();
		}
	}

}
}