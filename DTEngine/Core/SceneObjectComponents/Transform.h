#pragma once

#include "DTEngine/Engine/glm.h"

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Core/SceneObjectComponent.h"

namespace DTEngine
{
	namespace Core
	{
		namespace SceneObjectComponents
		{

			class Transform : public SceneObjectComponent
			{
			public:
				explicit Transform(SceneObject &obj);
				~Transform();

				inline glm::vec3 position() const { return mPosition; }
				inline glm::quat rotation() const { return mRotation; }
				inline glm::vec3 scale() const { return mScale; }

				inline void translateBy(float x, float y, float z) { translateBy(glm::vec3(x, y, z)); }
				void translateBy(glm::vec3 vec);
				void rotate(glm::quat quat);
				inline void rotateByAngles(float x, float y, float z) { rotateByAngles(glm::vec3(x, y, z)); }
				inline void rotateByAngles(glm::vec3 angles) { rotateByRadians(glm::radians(angles)); }
				inline void rotateByRadians(float x, float y, float z) { rotateByRadians(glm::vec3(x, y, z)); }
				void rotateByRadians(glm::vec3 rads);
				inline void scale(float x, float y, float z) { scale(glm::vec3(x, y, z)); }
				void scale(glm::vec3 ratio);

				inline void translateTo(float x, float y, float z) { translateTo(glm::vec3(x, y, z)); }
				void translateTo(glm::vec3 vec);
				inline void rotateToAngles(float x, float y, float z) { rotateToAngles(glm::vec3(x, y, z)); }
				inline void rotateToAngles(glm::vec3 angles) { rotateToRadians(glm::radians(angles)); }
				inline void rotateToRadians(float x, float y, float z) { rotateToRadians(glm::vec3(x, y, z)); }
				void rotateToRadians(glm::vec3 rads);
				void rotateTo(glm::quat quat);
				inline void scaleTo(float x, float y, float z) { scaleTo(glm::vec3(x, y, z)); }
				void scaleTo(glm::vec3 ratio);

				void lookAt(glm::vec3 point);

				glm::mat4 matrix() const;

			private:
				glm::vec3 mPosition;
				glm::quat mRotation;
				glm::vec3 mScale;
			};

		}
	}
}