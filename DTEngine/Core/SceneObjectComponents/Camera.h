#pragma once

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Core/SceneObjectComponent.h"
#include "DTEngine/Core/Viewport.h"

namespace DTEngine
{
	namespace Core
	{
		namespace SceneObjectComponents
		{
			class Camera : public SceneObjectComponent, public Viewport
			{
			public:
				explicit Camera(SceneObject &sceneObject,
					float fov, float ratio, float near, float far);
				~Camera();
			};

		}
	}
}
