#pragma once

#include "DTEngine/Core/Prereq.h"
#include "DTEngine/Core/SceneObjectComponent.h"
#include "DTEngine/Core/Renderer.h"

namespace DTEngine
{
	namespace Core
	{
		class Mesh;
		class Material;

	namespace SceneObjectComponents
	{

		class MeshRenderer : public SceneObjectComponent, public Renderer
		{
		public:
			explicit MeshRenderer(SceneObject &object);
			~MeshRenderer();

			inline const SharedPtr<Mesh>& mesh() const { return mMesh; }
			void setMesh(SharedPtr<Mesh> mesh);

			inline const SharedPtr<Material>& material() const { return mMaterial; }
			void setMaterial(SharedPtr<Material> material);

		private:
			SharedPtr<Mesh> mMesh;
			SharedPtr<Material> mMaterial;
		};
	}
	}
}