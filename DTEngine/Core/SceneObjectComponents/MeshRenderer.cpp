#include "DTEngine/Core/SceneObjectComponents/MeshRenderer.h"
#include "DTEngine/Renderer/GPUMeshRenderer.h"

namespace DTEngine
{
namespace Core
{
namespace SceneObjectComponents
{

	MeshRenderer::MeshRenderer(SceneObject &object) :
		SceneObjectComponent(object),
		Renderer()
	{
		mGpuRenderer = new DTEngine::Renderer::GPUMeshRenderer(*this);
	}

	MeshRenderer::~MeshRenderer()
	{
		if(mGpuRenderer)
			delete mGpuRenderer;
	}

	void MeshRenderer::setMesh(SharedPtr<Mesh> mesh)
	{
		mMesh = mesh;
	}

	void MeshRenderer::setMaterial(SharedPtr<Material> material)
	{
		mMaterial = material;
	}

}
}
}