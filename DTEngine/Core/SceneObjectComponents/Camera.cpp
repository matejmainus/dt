#include "DTEngine/Core/SceneObjectComponents/Camera.h"

namespace DTEngine
{
namespace Core
{
namespace SceneObjectComponents
{

	Camera::Camera(SceneObject &sceneObject,
		float fov, float ratio, float near, float far) :
		SceneObjectComponent(sceneObject),
		Viewport(sceneObject, fov, ratio, near, far)
	{
	}

	Camera::~Camera()
	{
	}
}
}
}