#include "DTEngine/Core/SceneObjectComponents/Transform.h"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/matrix_decompose.hpp>

#include <iostream>

namespace DTEngine
{
namespace Core
{
namespace SceneObjectComponents
{

	Transform::Transform(SceneObject &sceneObject) :
		SceneObjectComponent(sceneObject)
	{
		mPosition = glm::vec3(0, 0, 0);
		mRotation = glm::quat();
		mScale = glm::vec3(1, 1, 1);
	}

	Transform::~Transform()
	{
	}

	void Transform::translateBy(glm::vec3 vec)
	{
		mPosition += vec;
	}

	void Transform::rotate(glm::quat quat)
	{
		mRotation *= quat;
	}

	void Transform::rotateByRadians(glm::vec3 rads)
	{
		rotate(glm::quat(rads));
	}

	void Transform::scale(glm::vec3 ratio)
	{
		mScale += ratio;
	}

	void Transform::translateTo(glm::vec3 vec)
	{
		mPosition = vec;
	}

	void Transform::rotateToRadians(glm::vec3 rads)
	{
		rotateTo(glm::quat(rads));
	}

	void Transform::rotateTo(glm::quat quat)
	{
		mRotation = quat;
	}

	void Transform::scaleTo(glm::vec3 ratio)
	{
		mScale = ratio;
	}

	void Transform::lookAt(glm::vec3 point)
	{
		glm::mat4 mat = glm::lookAtLH(mPosition, point, glm::vec3(0, 1, 0));
		
		mRotation = glm::quat_cast(mat);
	}

	glm::mat4 Transform::matrix() const
	{
		return glm::translate(mPosition) *
			glm::scale(mScale) *
			glm::mat4_cast(mRotation) *
			glm::translate(-mPosition) *
			glm::translate(mPosition);
	}

}
}
}