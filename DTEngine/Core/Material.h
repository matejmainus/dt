#pragma once

#include "DTEngine/Core/Prereq.h"

#include "DTEngine/Renderer/Entities/GPUMaterial.h"

namespace DTEngine
{
	namespace Core
	{
		class Material
		{
		public:
			explicit Material(size_t materialId) :
				mMaterialId(materialId),
				mGpuMaterial(nullptr)
			{}

			virtual ~Material()
			{
				if (mGpuMaterial)
					delete mGpuMaterial;
			}

			inline size_t id() const { return mMaterialId; }

			inline DTEngine::Renderer::Entities::GPUMaterial* gpuMaterial() const { return mGpuMaterial; }

		protected:
			friend class DTEngine::Renderer::Entities::GPUMaterial;

			size_t mMaterialId;

			DTEngine::Renderer::Entities::GPUMaterial *mGpuMaterial;
		};

	}
}