#include "Color.h"

namespace DTEngine
{
namespace Core
{

	Color::Color(int r, int g, int b, int a)
	{
		mColor = (glm::vec4(r, g, b, a) / 255.0f);
	}

	Color::Color(float r, float g, float b, float a)
	{
		mColor = glm::vec4(r, g, b, a);
	}

	Color::Color(glm::vec4 vec)
	{
		mColor = vec;
	}

	Color::Color():
		mColor(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f))
	{}

	Color::~Color()
	{}

}
}

