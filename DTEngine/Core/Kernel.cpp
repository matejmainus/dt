#include <stdexcept>
#include <chrono>

#include "DTEngine/Engine/Log.h"

#include "DTEngine/Core/Kernel.h"
#include "DTEngine/Core/App.h"
#include "DTEngine/Core/EventManager.h"
#include "DTEngine/Core/InputManager.h"
#include "DTEngine/Core/SceneManager.h"
#include "DTEngine/Core/ResourceManager.h"
#include "DTEngine/Core/RenderSettings.h"
#include "DTEngine/Core/RenderStatsManager.h"

#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/Compositor.h"

namespace DTEngine
{
namespace Core
{

	Kernel::Kernel(App &app) :
		Singleton<Kernel>(),
		mApp(app),
		mRun(true)
	{
		Log::create();

		Log::info("Kernel created");

		RenderStatsManager::create();

		Renderer::RenderSystem::create();
		Renderer::Compositor::create();

		EventManager::create();
		InputManager::create();
		ResourceManager::create();
		SceneManager::create();

		init();

		mApp.init();
	}

	Kernel::~Kernel()
	{
		Renderer::Compositor::getInstance().wait();

		mApp.shutdown();

		shutdown();

		SceneManager::destroy();
		ResourceManager::destroy();
		InputManager::destroy();
		EventManager::destroy();

		Renderer::Compositor::destroy();
		Renderer::RenderSystem::destroy();

		RenderStatsManager::destroy();

		Log::info("Kernel destroyed");

		Log::destroy();
	}

	void Kernel::init()
	{
		ResourceManager::getInstance().init();
		EventManager::getInstance().init();
		SceneManager::getInstance().init();

		Renderer::RenderSystem::getInstance().init();
		Renderer::Compositor::getInstance().init();
	}

	void Kernel::shutdown()
	{
		Renderer::Compositor::getInstance().shutdown();

		SceneManager::getInstance().shutdown();
		ResourceManager::getInstance().shutdown();
		EventManager::getInstance().shutdown();

		Renderer::RenderSystem::getInstance().shutdown();
	}

	void Kernel::stop()
	{
		mRun = false;
	}

	const App& Kernel::app() const
	{
		return mApp;
	}

	bool Kernel::running()
	{
		return mRun;
	}

	void Kernel::run()
	{
		typedef std::chrono::high_resolution_clock Clock;
		Clock::time_point startTime, endTime, updateShot;

		DTEngine::Millisecs deltaTime = DTEngine::Millisecs(0);

		mApp.start();

		while (running())
		{
			startTime = Clock::now();

			EventManager::getInstance().processEvents();

			mApp.update(deltaTime);

			SceneManager::getInstance().updateScenes(deltaTime);

			updateShot = Clock::now();

			SceneManager::getInstance().postUpdateScenes();

			Renderer::Compositor::getInstance().draw();

			if (deltaTime.count() != 0)
				Renderer::Compositor::getInstance().wait();

			endTime = Clock::now();
			deltaTime = std::chrono::duration_cast<DTEngine::Millisecs>(endTime - startTime);

			RenderStatsManager::FrameStats *frameStats = RenderStatsManager::getInstance().currentFrame();
			frameStats->frameTime = deltaTime;
			frameStats->drawTime = std::chrono::duration_cast<DTEngine::Millisecs>(endTime - updateShot);
			frameStats->updateTime = std::chrono::duration_cast<DTEngine::Millisecs>(updateShot - startTime);
	
			RenderStatsManager::getInstance().swap();
		}

		mApp.stop();
	}

}
}
