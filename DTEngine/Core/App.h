#pragma once

#include "DTEngine/Core/Prereq.h"

namespace DTEngine
{
	namespace Core
	{
		class App
		{
		public:
			explicit App() {}
			virtual ~App() {}

			virtual void init() = 0;
			virtual void shutdown() = 0;

			virtual void start() = 0;
			virtual void stop() = 0;

			virtual const std::string& name() const = 0;

			virtual void update(const Millisecs &time) {}
		};

	}
}