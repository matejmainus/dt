#include "DTEngine/Renderer/GPUQueue.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/CommandPool.h"

#include "DTEngine/Core/RenderStatsManager.h"

namespace DTEngine
{
namespace Renderer
{

	GPUQueue::GPUQueue(GPUDevice &device, uint32_t queueFamilyIndex) :
		mDevice(device),
		mFamilyIndex(queueFamilyIndex)
	{
		mVkQueue = mDevice.vkDevice().getQueue(queueFamilyIndex, 0);

		vk::CommandPoolCreateInfo commandPoolCreateInfo;
		commandPoolCreateInfo
			.flags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer)
			.queueFamilyIndex(queueFamilyIndex);

		mGlobalCmdPool = new CommandPool(device, commandPoolCreateInfo);
	}

	GPUQueue::~GPUQueue()
	{
		delete mGlobalCmdPool;
	}

	uint32_t GPUQueue::familyIndex() const
	{
		return mFamilyIndex;
	}

	void GPUQueue::submit(uint32_t submitSize, vk::SubmitInfo* submitInfos, vk::Fence fence)
	{
		Core::RenderStatsManager::getInstance().currentFrame()->queueSubmitCount++;

		mQueueMutex.lock();

		mVkQueue.submit(submitSize, submitInfos, fence);

		mQueueMutex.unlock();
	}

	void GPUQueue::submit(vk::SubmitInfo &submitInfo, vk::Fence fence)
	{
		mQueueMutex.lock();

		mVkQueue.submit(1, &submitInfo, fence);

		mQueueMutex.unlock();
	}

	void GPUQueue::waitToIdle() const
	{
		mQueueMutex.lock();

		mVkQueue.waitIdle();

		mQueueMutex.unlock();
	}
}
}