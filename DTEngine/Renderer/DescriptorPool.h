#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Engine/Lockable.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class DescriptorSet;

		class DescriptorPool : public Lockable
		{
		public:
			explicit DescriptorPool(GPUDevice &device, vk::DescriptorPoolCreateInfo &createInfo);
			~DescriptorPool();

			SharedPtr<DescriptorSet> allocateDescriptorSet(vk::DescriptorSetLayout layout);

			void freeDescriptorSets();

		private:
			GPUDevice &mDevice;

			vk::DescriptorPoolCreateInfo mVkCreateInfo;

			vk::DescriptorPool mVkPool;
		};

	}
}
