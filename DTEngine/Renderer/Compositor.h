#pragma once

#include "DTEngine/Engine/Singleton.h"

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Core
	{
		class Scene;
		class Viewport;
	}

	namespace Renderer
	{
		class RenderTarget;
		class RenderWorker;
		class CommandBuffer;
		class GPUDevice;

		class Compositor : public Singleton<Compositor>
		{
		public:
			explicit Compositor();
			~Compositor();

			void init();
			void shutdown();

			void setActiveScene(Core::Scene *scene);
			inline Core::Scene* activeScene() const { return mActiveScene; }

			void setActiveViewport(Core::Viewport *viewport);
			inline Core::Viewport* activeViewport() const { return mActiveViewport; }

			void wait();
			void draw();

			inline vk::RenderPass vkRenderPass() const { return mVkRenderPass; }

		private:
			void createRenderPass();
			void createTimerQueryPool();

			void prepareSynchronization();
			void destroySynchronization();

			void calcGPUTime();

			void prepareRenderTarget(SharedPtr<CommandBuffer> cmdBuff, const SharedPtr<RenderTarget> &target,
				vk::AccessFlags srcFlags, vk::AccessFlags dstFlags,
				vk::ImageLayout oldLayout, vk::ImageLayout newLayout);

		private:
			std::mutex mGuard;

			GPUDevice *mDevice;

			RenderWorker *mRenderWorker;

			Core::Scene *mActiveScene;
			Core::Viewport *mActiveViewport;

			vk::RenderPass mVkRenderPass;
			vk::Semaphore mVkPresentSemaphore;
			vk::Event mVkCopyEvent;
			vk::QueryPool mVkTimerQueryPool;

			float mTimestampPeriod;
		};

	}
}