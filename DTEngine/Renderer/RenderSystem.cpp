#include <vector>

#include "DTEngine/Core/Kernel.h"
#include "DTEngine/Core/App.h"
#include "DTEngine/Core/RenderSettings.h"
#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/VkExtLoader.h"
#include "DTEngine/Renderer/Window.h"

#include "DTEngine/Renderer/Entities/GPUViewport.h"
#include "DTEngine/Renderer/Entities/GPUSceneObject.h"

#include "DTEngine/Engine/Log.h"

namespace DTEngine
{
namespace Renderer
{
	const std::string ENGINE_NAME = "DTEngine";

	RenderSystem::RenderSystem() :
		Singleton<RenderSystem>(),
		mDevice(nullptr)
	{
		Log::info("RenderSystem created");
	}

	RenderSystem::~RenderSystem()
	{
		Log::info("RenderSystem destroyed");
	}

	void RenderSystem::init()
	{
		initVKInstance();

#ifdef VK_DEBUG
		initVkDebug();
#endif

		initVKDevice();
	}

	void RenderSystem::shutdown()
	{
		Entities::GPUSceneObject::destroyDescriptorLayout();
		Entities::GPUViewport::destroyDescriptorLayout();
		
		if (mDevice)
			delete mDevice;
		
#ifdef VK_DEBUG
		destroyVkDebug();
#endif
		vkDestroyInstance(mVkInstance, nullptr);
	}

	SharedPtr<Window> RenderSystem::createWindow(const std::string & name, unsigned int width, unsigned int height)
	{
		return std::make_shared<Window>(*mDevice, name, width, height);
	}

	void RenderSystem::initVKInstance()
	{
		const DTEngine::Core::App &app = DTEngine::Core::Kernel::getInstance().app();

		vk::ApplicationInfo appInfo;
		appInfo.pApplicationName(app.name().c_str());
		appInfo.pEngineName(ENGINE_NAME.c_str());
		appInfo.apiVersion(VK_API_VERSION_1_0);

		std::vector<const char*> extensions = { VK_KHR_SURFACE_EXTENSION_NAME };
		std::vector<const char*> layers;

#ifdef VK_DEBUG
		extensions.insert(extensions.end(),
		{
			VK_EXT_DEBUG_REPORT_EXTENSION_NAME
		});

		RenderSystem::activeVkLayers(layers);
#endif

#if _WINDOWS
		extensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#endif

		vk::InstanceCreateInfo instanceCreateInfo;
		instanceCreateInfo.pApplicationInfo(&appInfo);
		instanceCreateInfo.enabledExtensionCount((uint32_t) extensions.size());
		instanceCreateInfo.ppEnabledExtensionNames(extensions.data());
		instanceCreateInfo.enabledLayerCount((uint32_t) layers.size());
		instanceCreateInfo.ppEnabledLayerNames(layers.data());

		mVkInstance = vk::createInstance(instanceCreateInfo);
	}

	void RenderSystem::initVkDebug()
	{
		VkDebugReportCallbackCreateInfoEXT dbgCreateInfo;
		dbgCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
		dbgCreateInfo.pNext = NULL;
		dbgCreateInfo.pfnCallback = (PFN_vkDebugReportCallbackEXT) RenderSystem::vkMsgCallback;
		dbgCreateInfo.pUserData = NULL;
		dbgCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;

		PFN_vkCreateDebugReportCallbackEXT fpCreateDebugReportCallbackEXT;
		VK_GET_INSTANCE_PROC_ADDR(mVkInstance, CreateDebugReportCallbackEXT);

		VkResult err = fpCreateDebugReportCallbackEXT(
			mVkInstance,
			&dbgCreateInfo,
			NULL,
			&mVkMsgCallback);
		assert(!err);
	}

	void RenderSystem::initVKDevice()
	{
		std::vector<vk::PhysicalDevice> physicalDevices = mVkInstance.enumeratePhysicalDevices();

		if (physicalDevices.empty())
			throw std::runtime_error("No suitable GPU found");
		else if (physicalDevices.size() > 1)
			Log::info("Founded more then one GPU");

		vk::PhysicalDevice physDevice = physicalDevices[Core::RenderSettings::getInstance().selectedGpu()];

		mDevice = new GPUDevice(physDevice);

		Entities::GPUSceneObject::createPushConstantRange();
		Entities::GPUSceneObject::createDescriptorLayout();
		Entities::GPUViewport::createDescriptorLayout();
	}

	void RenderSystem::activeVkLayers(std::vector<const char*>& layers)
	{
		layers.insert(layers.end(),
		{
			"VK_LAYER_GOOGLE_threading",
			//"VK_LAYER_LUNARG_object_tracker",
			"VK_LAYER_LUNARG_core_validation",
			"VK_LAYER_LUNARG_parameter_validation",
			"VK_LAYER_LUNARG_swapchain",
			"VK_LAYER_LUNARG_device_limits",
			"VK_LAYER_LUNARG_image",
			"VK_LAYER_GOOGLE_unique_objects",
		});
	}

	void RenderSystem::destroyVkDebug()
	{
		PFN_vkDestroyDebugReportCallbackEXT fpDestroyDebugReportCallbackEXT;
		VK_GET_INSTANCE_PROC_ADDR(mVkInstance, DestroyDebugReportCallbackEXT);

		fpDestroyDebugReportCallbackEXT(mVkInstance, mVkMsgCallback, nullptr);
	}

	uint32_t RenderSystem::findQueueIndex(vk::PhysicalDevice phyDevice, vk::QueueFlags flags)
	{
		std::vector<vk::QueueFamilyProperties> queueProps = phyDevice.getQueueFamilyProperties();
		std::vector<vk::QueueFamilyProperties>::iterator it = queueProps.begin();

		for (uint32_t queueIndex = 0; it != queueProps.end() ; ++it, ++queueIndex)
		{
			if ((queueProps[queueIndex].queueFlags() & flags) == flags)
				return queueIndex;
		}

		throw std::runtime_error("No graphics qeue found for selected GPU");
	}

	VkBool32 RenderSystem::vkMsgCallback(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t srcObject,
		size_t location,
		int32_t msgCode,
		const char* pLayerPrefix,
		const char* pMsg,
		void* pUserData)
	{
		if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
		{
			Log::error("VKError: " + std::string(pMsg));
		}
		else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
		{
			Log::info("VKWarm: " + std::string(pMsg));
		}

		return false;
	}


}
}