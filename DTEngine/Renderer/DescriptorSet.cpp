#include "DTEngine/Renderer/DescriptorSet.h"
#include "DTEngine/Renderer/GPUDevice.h"

#include "DTEngine/Core/RenderStatsManager.h"

namespace DTEngine
{
namespace Renderer
{

	DescriptorSet::DescriptorSet(GPUDevice &device, vk::DescriptorSetAllocateInfo &allocInfo, bool freeOnDestroy) :
		mDevice(device),
		mVkAllocInfo(allocInfo),
		mFreeOnDestroy(freeOnDestroy)
	{
		mVkDescriptorSet = mDevice.vkDevice().allocateDescriptorSets(allocInfo)[0];

		Core::RenderStatsManager::getInstance().usageStats()->descriptorSetCount++;
	}

	DescriptorSet::~DescriptorSet()
	{
		if(mFreeOnDestroy)
			mDevice.vkDevice().freeDescriptorSets(mVkAllocInfo.descriptorPool(), 1, &mVkDescriptorSet);

		Core::RenderStatsManager::getInstance().usageStats()->descriptorSetCount--;
	}

}
}