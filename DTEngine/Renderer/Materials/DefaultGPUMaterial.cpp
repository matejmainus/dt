#include "DTEngine/Renderer/Materials/DefaultGPUMaterial.h"
#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/Pipeline.h"
#include "DTEngine/Renderer/PipelineManager.h"
#include "DTEngine/Renderer/Compositor.h"
#include "DTEngine/Renderer/GPUBuffer.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"
#include "DTEngine/Renderer/DescriptorPool.h"
#include "DTEngine/Renderer/DescriptorSet.h"
#include "DTEngine/Renderer/StagingBufferPool.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/GPUImage.h"
#include "DTEngine/Renderer/Sampler.h"
#include "DTEngine/Renderer/DrawParams.h"
#include "DTEngine/Renderer/Entities/GPUMesh.h"
#include "DTEngine/Renderer/Entities/GPUViewport.h"
#include "DTEngine/Renderer/Entities/GPUTexture.h"
#include "DTEngine/Renderer/Entities/GPUSceneObject.h"

#include "DTEngine/Core/RenderSettings.h"
#include "DTEngine/Core/Materials/DefaultMaterial.h"
#include "DTEngine/Core/SceneObjectComponents/Transform.h"
#include "DTEngine/Core/Texture.h"

namespace DTEngine
{
	namespace Renderer
	{
		namespace Materials
		{
			vk::DescriptorSetLayout DefaultGPUMaterial::sVkDescriptorSetLayout = nullptr;
			Pipeline* DefaultGPUMaterial::sPipeline = nullptr;

			struct MaterialBuffer
			{
				glm::vec4 diffuseColor;
			};

			DefaultGPUMaterial::DefaultGPUMaterial(Core::Materials::DefaultMaterial &material) :
				GPUMaterial(material),
				mDiffuseView(nullptr),
				mDescriptorSet(nullptr)
			{
				GPUDevice &device = RenderSystem::getInstance().gpuDevice();

				if (!sPipeline)
					createPipelineInstance();

				const vk::BufferCreateFlags flags;
				const uint32_t queues = device.graphicsQueue().familyIndex();

				vk::BufferCreateInfo bufferCreateInfo(flags,
					sizeof(MaterialBuffer),
					vk::BufferUsageFlagBits::eUniformBuffer | vk::BufferUsageFlagBits::eTransferDst,
					vk::SharingMode::eExclusive,
					1,
					&queues);

				mBuffer = std::make_shared<GPUBuffer>(device, bufferCreateInfo);

				vk::MemoryPropertyFlags memoryType = Core::RenderSettings::getInstance().useStagingBuffers() ? vk::MemoryPropertyFlagBits::eDeviceLocal : vk::MemoryPropertyFlagBits::eHostVisible;

				if (Core::RenderSettings::getInstance().useMemoryPools())
					mBuffer->bindToPoolGpuMemory(memoryType);
				else
					mBuffer->bindToInherentGpuMemory(memoryType);

				mVkBufferBinding.buffer(*mBuffer)
					.offset(0)
					.range(sizeof(MaterialBuffer));
			}

			DefaultGPUMaterial::~DefaultGPUMaterial()
			{
				mBuffer.reset();
				mDescriptorSet.reset();
			}

			void DefaultGPUMaterial::bufferData(DrawParams &drawParams)
			{
				if (!mDirty)
					return;

				MutexGuard dirtyGuard(mDirtyMutex);

				if (!mDirty)
					return;

				const Core::Materials::DefaultMaterial &defaultMaterial = material<Core::Materials::DefaultMaterial>();

				if (defaultMaterial.mDiffuseTexture)
					defaultMaterial.mDiffuseTexture->gpuTexture()->bufferData(drawParams);

				if (Core::RenderSettings::getInstance().useStagingBuffers())
				{
					bufferDataStaging(drawParams);
				}
				else
				{
					bufferDataRaw(drawParams);
				}

				if (!mDescriptorSet)
					bind(drawParams);
				else
				{
					mDiffuseView.reset();
					bindTextures(drawParams);
				}

				mDirty = false;
			}

			void DefaultGPUMaterial::copyData(void *dstBuffer)
			{
				const Core::Materials::DefaultMaterial &defaultMaterial = material<Core::Materials::DefaultMaterial>();

				MaterialBuffer matBuffer;
				matBuffer.diffuseColor = defaultMaterial.mDiffuseColor.normVec();

				memcpy(dstBuffer, &matBuffer, sizeof(MaterialBuffer));
			}

			void DefaultGPUMaterial::bufferDataRaw(DrawParams &drawParams)
			{
				void *bufferPtr = mBuffer->map();

				copyData(bufferPtr);

				mBuffer->unmap();
			}

			void DefaultGPUMaterial::bufferDataStaging(DrawParams &drawParams)
			{
				SharedPtr<GPUBuffer> stageBuffer;
				vk::DeviceSize offset;

				void *bufferPtr = drawParams.stagingPool->map(mBuffer->memoryRequirements(), stageBuffer, offset, StagingBufferPool::PageType::Buffer);

				copyData(bufferPtr);

				vk::BufferCopy buffCopy;
				buffCopy.srcOffset(offset)
					.dstOffset(0)
					.size(mBuffer->memoryRequirements().size());

				drawParams.copyCmdBuff->vkCommandBuffer().copyBuffer(*stageBuffer, *mBuffer, 1, &buffCopy);
			}


			void DefaultGPUMaterial::bindTextures(DrawParams & drawParams)
			{
				const Core::Materials::DefaultMaterial &defaultMaterial = material<Core::Materials::DefaultMaterial>();

				Entities::GPUTexture *diffuseTexture;

				if (defaultMaterial.diffuseTexture())
				{
					diffuseTexture = defaultMaterial.diffuseTexture()->gpuTexture();

					vk::ComponentMapping mapping;
					vk::ImageSubresourceRange range(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

					mDiffuseView = diffuseTexture->image()->view(vk::ImageViewType::e2D, vk::Format::eR32G32B32A32Sfloat, mapping, range);;

					mVkDiffuseBinding.sampler(diffuseTexture->sampler())
						.imageView(*mDiffuseView)
						.imageLayout(vk::ImageLayout::eGeneral);

					vk::WriteDescriptorSet writeTextureDescriptroSet;
					writeTextureDescriptroSet.dstSet(*mDescriptorSet)
						.descriptorCount(1)
						.descriptorType(vk::DescriptorType::eCombinedImageSampler)
						.pImageInfo(&mVkDiffuseBinding)
						.dstBinding(1);

					drawParams.descriptorUpdate->push_back(writeTextureDescriptroSet);
				}
			}

			void DefaultGPUMaterial::bind(DrawParams &drawParams)
			{
				mDescriptorSet = drawParams.descriptorPool->allocateDescriptorSet(sVkDescriptorSetLayout);

				vk::WriteDescriptorSet writeBufferDescriptorSet;
				writeBufferDescriptorSet.dstSet(*mDescriptorSet)
					.descriptorCount(1)
					.descriptorType(vk::DescriptorType::eUniformBuffer)
					.pBufferInfo(&mVkBufferBinding)
					.dstBinding(0);

				drawParams.descriptorUpdate->push_back(writeBufferDescriptorSet);

				bindTextures(drawParams);
			}

			void DefaultGPUMaterial::createDescriptorLayout()
			{
				GPUDevice &device = RenderSystem::getInstance().gpuDevice();

				std::array<vk::DescriptorSetLayoutBinding, 2> descritproSetBinding;
				descritproSetBinding[0].descriptorType(vk::DescriptorType::eUniformBuffer)
					.descriptorCount(1)
					.stageFlags(vk::ShaderStageFlagBits::eFragment)
					.pImmutableSamplers(false)
					.binding(0);

				descritproSetBinding[1].descriptorType(vk::DescriptorType::eCombinedImageSampler)
					.descriptorCount(1)
					.stageFlags(vk::ShaderStageFlagBits::eFragment)
					.pImmutableSamplers(false)
					.binding(1);

				vk::DescriptorSetLayoutCreateInfo descriptorLayout;
				descriptorLayout.bindingCount((uint32_t)descritproSetBinding.size())
					.pBindings(descritproSetBinding.data());

				sVkDescriptorSetLayout = device.vkDevice().createDescriptorSetLayout(descriptorLayout);
			}

			void DefaultGPUMaterial::createPipelineInstance()
			{
				GPUDevice &device = RenderSystem::getInstance().gpuDevice();

				createDescriptorLayout();

				std::vector<vk::DescriptorSetLayout> layouts;
				std::vector<vk::PushConstantRange> constants;

				layouts.push_back(Entities::GPUViewport::vkDescriptorSetLayout());
				layouts.push_back(sVkDescriptorSetLayout);

				if (Core::RenderSettings::getInstance().usePushConstants())
				{
					constants.push_back(Entities::GPUSceneObject::vkPushConstantRange());
				}
				else
				{
					layouts.push_back(Entities::GPUSceneObject::vkDescriptorSetLayout());
				}

				vk::PipelineLayoutCreateInfo pipelineLayoutCreateInfo;
				pipelineLayoutCreateInfo
					.setLayoutCount((uint32_t)layouts.size())
					.pSetLayouts(layouts.data())
					.pushConstantRangeCount((uint32_t)constants.size())
					.pPushConstantRanges(constants.data());

				/** Pipeline setup **/
				vk::PipelineInputAssemblyStateCreateInfo inputAssemblyState;
				inputAssemblyState.topology(vk::PrimitiveTopology::eTriangleList)
					.primitiveRestartEnable(false);

				vk::PipelineRasterizationStateCreateInfo rasterizationState;
				rasterizationState.polygonMode(vk::PolygonMode::eFill)
					.lineWidth(1.0f)
					.cullMode(vk::CullModeFlagBits::eBack)
					.frontFace(vk::FrontFace::eClockwise)
					.depthClampEnable(false)
					.rasterizerDiscardEnable(false)
					.depthBiasEnable(false);

				vk::PipelineColorBlendAttachmentState blendAttachmentState[1] = {};
				blendAttachmentState[0]
					.colorWriteMask((vk::ColorComponentFlagBits) 0xf)
					.blendEnable(false);

				vk::PipelineColorBlendStateCreateInfo colorBlendState;
				colorBlendState.attachmentCount(1)
					.pAttachments(blendAttachmentState);

				vk::StencilOpState stencilState;
				stencilState.failOp(vk::StencilOp::eKeep)
					.passOp(vk::StencilOp::eKeep)
					.compareOp(vk::CompareOp::eAlways);

				vk::PipelineDepthStencilStateCreateInfo depthStencilState;
				depthStencilState.depthTestEnable(true)
					.depthWriteEnable(true)
					.depthCompareOp(vk::CompareOp::eLess)
					.depthBoundsTestEnable(false)
					.stencilTestEnable(false)
					.back(stencilState)
					.front(stencilState);

				vk::PipelineViewportStateCreateInfo viewportState;
				viewportState.viewportCount(1)
					.scissorCount(1);

				std::vector<vk::DynamicState> dynamicStateEnables;
				dynamicStateEnables.push_back(vk::DynamicState::eViewport);
				dynamicStateEnables.push_back(vk::DynamicState::eScissor);

				vk::PipelineDynamicStateCreateInfo dynamicState;
				dynamicState.dynamicStateCount((uint32_t)dynamicStateEnables.size())
					.pDynamicStates(dynamicStateEnables.data());

				vk::PipelineMultisampleStateCreateInfo multisampleState;
				multisampleState.rasterizationSamples(vk::SampleCountFlagBits::e1);

				vk::VertexInputAttributeDescription attributeDescriptions[3];
				attributeDescriptions[0]
					.binding(0)
					.location(0)
					.format(vk::Format::eR32G32B32A32Sfloat)
					.offset(offsetof(Entities::GPUMesh::VertexData, pos));

				attributeDescriptions[1]
					.binding(0)
					.location(1)
					.format(vk::Format::eR32G32B32A32Sfloat)
					.offset(offsetof(Entities::GPUMesh::VertexData, normal));

				attributeDescriptions[2]
					.binding(0)
					.location(2)
					.format(vk::Format::eR32G32Sfloat)
					.offset(offsetof(Entities::GPUMesh::VertexData, uv));

				vk::VertexInputBindingDescription bindingDescription[1];
				bindingDescription[0]
					.binding(0)
					.stride(sizeof(Entities::GPUMesh::VertexData))
					.inputRate(vk::VertexInputRate::eVertex);

				vk::PipelineVertexInputStateCreateInfo vertexState;
				vertexState.vertexBindingDescriptionCount(1)
					.pVertexBindingDescriptions(bindingDescription)
					.vertexAttributeDescriptionCount(3)
					.pVertexAttributeDescriptions(attributeDescriptions);

				vk::PipelineShaderStageCreateInfo shaderStages[2];

				if (Core::RenderSettings::getInstance().usePushConstants())
				{
					shaderStages[0].pName("main")
						.stage(vk::ShaderStageFlagBits::eVertex)
						.module(device.pipelineManager().shader("./Data/DefaultMaterial/mat_push.vert.spv"));
				}
				else
				{
					shaderStages[0].pName("main")
						.stage(vk::ShaderStageFlagBits::eVertex)
						.module(device.pipelineManager().shader("./Data/DefaultMaterial/mat.vert.spv"));
				}

				shaderStages[1].pName("main")
					.stage(vk::ShaderStageFlagBits::eFragment)
					.module(device.pipelineManager().shader("./Data/DefaultMaterial/mat.frag.spv"));

				vk::GraphicsPipelineCreateInfo pipelineCreateInfo;
				pipelineCreateInfo.pVertexInputState(&vertexState)
					.pInputAssemblyState(&inputAssemblyState)
					.pRasterizationState(&rasterizationState)
					.pColorBlendState(&colorBlendState)
					.pMultisampleState(&multisampleState)
					.pViewportState(&viewportState)
					.pDepthStencilState(&depthStencilState)
					.stageCount(2)
					.pStages(shaderStages)
					.pDynamicState(&dynamicState)
					.renderPass(Compositor::getInstance().vkRenderPass());

				sPipeline = device.pipelineManager().graphicsPipeline(pipelineCreateInfo, pipelineLayoutCreateInfo);
			}

		}
	}
}
