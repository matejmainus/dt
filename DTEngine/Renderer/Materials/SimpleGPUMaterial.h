#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/Entities/GPUMaterial.h"
#include "DTEngine/Renderer/DescriptorSet.h"

#include "DTEngine/Engine/Namespaces.h"

namespace DTEngine
{
	namespace Core
	{
		namespace Materials
		{
			class SimpleMaterial;
		};
	}

	namespace Renderer
	{
		class GPUDevice;
		class PipelineManager;
		class GPUImageView;

		struct DrawParams;

		namespace Materials
		{
			class SimpleGPUMaterial : public Entities::GPUMaterial
			{
			public:
				explicit SimpleGPUMaterial(Core::Materials::SimpleMaterial& material);
				virtual ~SimpleGPUMaterial();

				virtual void bufferData(DrawParams &drawParams) override;

				inline virtual Pipeline& pipeline() override { return *sPipeline; }
				virtual vk::DescriptorSet vkDescriptorSet() override { return *mDescriptorSet; }

			private:
				inline void bind(DrawParams &drawParams);

				inline void bufferDataRaw(DrawParams &drawParams);
				inline void bufferDataStaging(DrawParams &drawParams);

				inline void copyData(void *dstBuffer);

				static void createDescriptorLayout();
				static void createPipelineInstance();

			private:
				SharedPtr<GPUBuffer> mBuffer;
				vk::DescriptorBufferInfo mVkBufferBinding;

				static Renderer::Pipeline* sPipeline;
				static vk::DescriptorSetLayout sVkDescriptorSetLayout;

				SharedPtr<DescriptorSet> mDescriptorSet;
			};
		}
	}
}

