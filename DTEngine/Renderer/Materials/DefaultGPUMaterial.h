#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/Entities/GPUMaterial.h"
#include "DTEngine/Renderer/DescriptorSet.h"

#include "DTEngine/Engine/Namespaces.h"

namespace DTEngine
{
	namespace Core
	{
		namespace Materials
		{
			class DefaultMaterial;
		};
	}

	namespace Renderer
	{
		class GPUDevice;
		class PipelineManager;
		class GPUImageView;

		struct DrawParams;

		namespace Materials
		{
			class DefaultGPUMaterial : public Entities::GPUMaterial
			{
			public:
				explicit DefaultGPUMaterial(Core::Materials::DefaultMaterial& material);
				virtual ~DefaultGPUMaterial();

				virtual void bufferData(DrawParams &drawParams) override;

				inline virtual Pipeline& pipeline() override { return *sPipeline; }
				virtual vk::DescriptorSet vkDescriptorSet() override { return *mDescriptorSet; }

			private:
				inline void bind(DrawParams &drawParams);
				inline void bindTextures(DrawParams &drawParams);

				inline void bufferDataRaw(DrawParams &drawParams);
				inline void bufferDataStaging(DrawParams &drawParams);

				inline void copyData(void *dstBuffer);

				static void createDescriptorLayout();
				static void createPipelineInstance();

			private:
				SharedPtr<GPUImageView> mDiffuseView;
				SharedPtr<GPUBuffer> mBuffer;
				vk::DescriptorBufferInfo mVkBufferBinding;
				vk::DescriptorImageInfo mVkDiffuseBinding;

				static Renderer::Pipeline* sPipeline;
				static vk::DescriptorSetLayout sVkDescriptorSetLayout;

				SharedPtr<DescriptorSet> mDescriptorSet;
			};
		}
	}
}

