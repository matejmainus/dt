#include "DTEngine/Renderer/GPUResource.h"
#include "DTEngine/Renderer/GPUMemory.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUMemoryManager.h"

namespace DTEngine
{
namespace Renderer
{

	GPUResource::GPUResource(GPUDevice &device) :
		mDevice(device),
		mMemory(nullptr)
	{
		mSubMemory.clear();
	}

	GPUResource::~GPUResource()
	{
		freeGpuMemory();
	}

	void GPUResource::allocateGpuMemory(vk::MemoryRequirements & requirements, vk::MemoryPropertyFlags flags)
	{
		mSubMemory.clear();

		mMemory = mDevice.memoryManager().allocateMemory(requirements, flags);
	}

	void GPUResource::suballocateGpuMemory(vk::MemoryRequirements &requirements, const SharedPtr<GPUMemory> &gpuMemory)
	{
		mMemory = gpuMemory;
		mSubMemory.clear();

		if (!mMemory->subAllocate(requirements, mSubMemory))
			throw std::runtime_error("Could not suballocate memory");
	}

	void GPUResource::poolGpuMemory(vk::MemoryRequirements & requirements, vk::MemoryPropertyFlags flags)
	{
		mMemory = mDevice.memoryManager().pullMemory(requirements, mSubMemory, flags);
	}

	void GPUResource::freeGpuMemory()
	{
		if (mSubMemory.size != 0)
		{
			mMemory->subFree(mSubMemory);
			mSubMemory.clear();
		}

		mMemory.reset();
	}

	void GPUResource::addView(const SharedPtr<void>& view)
	{
		lock();
		
		mViews.push_back(view);

		unlock();
	}

	void GPUResource::destroyView(const SharedPtr<void>& view)
	{
		lock();

		std::remove(mViews.begin(), mViews.end(), view);

		unlock();
	}

	vk::MemoryRequirements GPUResource::memoryRequirements() const
	{
		return mVkMemoryRequirements;
	}

	void GPUResource::bindToGpuMemory(const SharedPtr<GPUMemory>& gpuMemory)
	{
		suballocateGpuMemory(memoryRequirements(), gpuMemory);

		bind(mMemory->vkMemory(), mSubMemory.offset);
	}

	void GPUResource::bindToInherentGpuMemory(vk::MemoryPropertyFlags flags)
	{
		allocateGpuMemory(memoryRequirements(), flags);

		bind(mMemory->vkMemory(), 0);
	}

	void GPUResource::bindToPoolGpuMemory(vk::MemoryPropertyFlags flags)
	{
		poolGpuMemory(memoryRequirements(), flags);

		bind(mMemory->vkMemory(), mSubMemory.offset);
	}

	void* GPUResource::map(vk::DeviceSize offset, vk::DeviceSize size)
	{
		return (char*) mMemory->map(offset, size) + mSubMemory.offset;
	}

	void GPUResource::unmap()
	{
		mMemory->unmap();
	}


}
}