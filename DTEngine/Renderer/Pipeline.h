#pragma once

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;

		class Pipeline
		{
		public:
			explicit Pipeline(size_t pipelineId, GPUDevice &device, vk::PipelineCache cache,
				vk::GraphicsPipelineCreateInfo &pipelineCreateInfo, vk::PipelineLayoutCreateInfo &pipelineLayoutCreateInfo);
			~Pipeline();

			inline vk::Pipeline vkPipeline() const { return mVkPipeline; }
			operator vk::Pipeline() const { return mVkPipeline; }

			inline vk::PipelineLayout vkPipelineLayout() const { return mVkPipelineLayout; }

		private:
			GPUDevice &mDevice;

			vk::Pipeline mVkPipeline;
			vk::PipelineLayout mVkPipelineLayout;

			size_t mPipelineId;
		};

	}
}