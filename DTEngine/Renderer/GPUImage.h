#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/GPUResource.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUImageView;
		class CommandBuffer;

		class GPUImage : public GPUResource
		{
		public:
			explicit GPUImage(GPUDevice &device, vk::ImageCreateInfo &createInfo);
			explicit GPUImage(GPUDevice &device, vk::Image externalImage);
			virtual ~GPUImage();

			inline vk::Image vkImage() const { return mVkImage; };
			operator vk::Image() const { return mVkImage; }

			void barrier(vk::CommandBuffer cmdBuff,
				vk::ImageLayout oldLayout, vk::ImageLayout newLayout,
				vk::AccessFlags srcAccessMask, vk::AccessFlags dstAccessMask,
				vk::ImageSubresourceRange range,
				vk::PipelineStageFlags srcStage = vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlags dstStage = vk::PipelineStageFlagBits::eTopOfPipe);

			inline const vk::ImageCreateInfo& vkCreateInfo() const { return mVkCreateInfo; }

			SharedPtr<GPUImageView> view(vk::ImageViewType type, vk::Format format, vk::ComponentMapping &mapping, vk::ImageSubresourceRange &range);

		protected:
			virtual void bind(vk::DeviceMemory deviceMemory, vk::DeviceSize offset);

		private:
			vk::Image mVkImage;

			vk::ImageCreateInfo mVkCreateInfo;
		};

		class GPUImageView
		{
		public:
			explicit GPUImageView(GPUImage &image, vk::ImageViewCreateInfo &createInfo);
			~GPUImageView();

			inline vk::ImageView vkView() const { return mVkView; }
			operator vk::ImageView() const { return mVkView; }

		private:
			GPUImage &mImage;

			vk::ImageView mVkView;
		};

	}
}