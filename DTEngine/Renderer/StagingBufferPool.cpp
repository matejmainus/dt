#include "DTEngine/Renderer/StagingBufferPool.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"
#include "DTEngine/Renderer/GPUMemory.h"
#include "DTEngine/Renderer/GPUBuffer.h"
#include "DTEngine/Renderer/GPUMemoryManager.h"

namespace DTEngine
{
	namespace Renderer
	{
		struct StagingBufferPool::Page
		{
			SharedPtr<GPUBuffer> buffer;
			void *map;
			StagingBufferPool::PageType type;
		};

		StagingBufferPool::StagingBufferPool(GPUDevice &device, vk::DeviceSize pageSize, vk::DeviceSize pageAlignment, vk::DeviceSize pageCount):
			mDevice(device),
			mPageSize(pageSize),
			mPageAlignment(pageAlignment),
			mMemoryMgr(device.memoryManager())
		{

			for (size_t i = 0; i < pageCount; ++i)
			{
				mPages.push_back(createNewPage(pageSize));
			}
		}

		StagingBufferPool::~StagingBufferPool()
		{
			for (Page* const &page : mPages)
				delete page;

			mPages.clear();
		}

		void* StagingBufferPool::map(vk::MemoryRequirements & requirements, SharedPtr<GPUBuffer> &page, vk::DeviceSize &offset, PageType type)
		{
			return map(requirements.size(), requirements.alignment(), page, offset, type);
		}

		void* StagingBufferPool::map(vk::DeviceSize size, vk::DeviceSize alignment, SharedPtr<GPUBuffer> &buffer, vk::DeviceSize &offset, PageType type)
		{
			MutexGuard locker(mPagesGuard);

			GPUMemory::GPUSubMemory allocInfo;

			Page* freePage = nullptr;

			for (Page* const &page : mPages)
			{
				if ((page->type == type || page->type == PageType::None) &&
					page->buffer->boundMemory()->subAllocate(size, alignment, allocInfo))
				{
					freePage = page;
					break;
				}
			}

			if (!freePage)
			{
				freePage = createNewPage(pageAllocSize(size));
				mPages.push_back(freePage);

				freePage->buffer->boundMemory()->subAllocate(size, alignment, allocInfo);
			}

			freePage->type = type;

			buffer = freePage->buffer;
			offset = allocInfo.offset;

			return ((char *) freePage->map) + allocInfo.offset;
		}

		void StagingBufferPool::flushPages()
		{
			MutexGuard locker(mPagesGuard);

			std::vector<vk::MappedMemoryRange> ranges;

			vk::MappedMemoryRange range;

			for (Page* const &page : mPages)
			{
				if(page->type != PageType::None)
				{
					range.memory(*page->buffer->boundMemory());
					range.size(page->buffer->boundMemory()->size());

					ranges.push_back(range);
				}
			}

			if(!ranges.empty())
				mDevice.vkDevice().flushMappedMemoryRanges(ranges);
		}

		void StagingBufferPool::freePages()
		{
			MutexGuard locker(mPagesGuard);
			
			for (Page* const &page : mPages)
			{
				page->buffer->boundMemory()->subFreeAll();
				page->type = PageType::None;
			}
		}

		vk::DeviceSize StagingBufferPool::pageAllocSize(vk::DeviceSize requestedSize) const
		{
			return GPUMemory::size(std::max(mPageSize, requestedSize), mPageAlignment);
		}

		StagingBufferPool::Page* StagingBufferPool::createNewPage(vk::DeviceSize pageSize) const
		{
			const uint32_t queue = mDevice.graphicsQueue().familyIndex();

			Page *newPage = new Page();
			newPage->type = PageType::None;

			vk::BufferCreateInfo buffCreateInfo(vk::BufferCreateFlags(),
				pageSize,
				vk::BufferUsageFlagBits::eTransferSrc,
				vk::SharingMode::eExclusive,
				1,
				&queue);

			newPage->buffer = std::make_shared<GPUBuffer>(mDevice, buffCreateInfo);
			newPage->buffer->bindToInherentGpuMemory(vk::MemoryPropertyFlagBits::eHostVisible);

			newPage->map = newPage->buffer->map();

			return newPage;
		}
	}
}