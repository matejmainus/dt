#pragma once

#include "DTEngine/Renderer/Prereq.h"

#include "DTEngine/Engine/Dirty.h"

namespace DTEngine
{
	namespace Core
	{
		class Mesh;
	}

	namespace Renderer
	{
		class GPUBuffer;
		struct DrawParams;

		namespace Entities
		{
			class GPUMesh : public Dirty
			{
			public:
				struct VertexData
				{
					glm::vec4 pos;
					glm::vec4 normal;
					glm::vec2 uv;
					glm::vec2 padding;
				};

			public:
				explicit GPUMesh(DTEngine::Core::Mesh &mesh);
				~GPUMesh();

				void bufferData(DrawParams &drawParams);

				uint32_t verticesCount() const;
				uint32_t indexiesCount() const;

				inline const SharedPtr<GPUBuffer>& buffer() const { return mBuffer; }
				vk::DeviceSize vertexOffset() const;
				vk::DeviceSize indexOffset() const;

			private:
				void bufferDataRaw(DrawParams &drawParams);
				void bufferDataStaging(DrawParams &drawParams);

				void createBuffers(vk::MemoryPropertyFlags memoryProp);
				void copyData(void *vertexMemory, void *indexMemory);

			private:
				DTEngine::Core::Mesh &mMesh;

				SharedPtr<GPUBuffer> mBuffer;
			};


		}
	}
}
