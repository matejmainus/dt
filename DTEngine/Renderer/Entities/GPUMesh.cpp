#include <assimp/mesh.h>

#include "DTEngine/Renderer/Entities/GPUMesh.h"
#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/GPUBuffer.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"
#include "DTEngine/Renderer/GPUMemory.h"
#include "DTEngine/Renderer/GPUMemoryManager.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/DrawParams.h"
#include "DTEngine/Renderer/StagingBufferPool.h"

#include "DTEngine/Core/Mesh.h"
#include "DTEngine/Core/RenderSettings.h"

namespace DTEngine
{
namespace Renderer
{
namespace Entities
{
	GPUMesh::GPUMesh(DTEngine::Core::Mesh &mesh) :
		mMesh(mesh)
	{
	}

	GPUMesh::~GPUMesh()
	{
	}

	uint32_t GPUMesh::verticesCount() const
	{
		return (uint32_t) mMesh.verticesCount();
	}

	uint32_t GPUMesh::indexiesCount() const
	{
		return (uint32_t) mMesh.indexiesCount();
	}

	vk::DeviceSize GPUMesh::vertexOffset() const
	{
		return 0;
	}

	vk::DeviceSize GPUMesh::indexOffset() const
	{
		return mMesh.verticesCount() * sizeof(VertexData);
	}

	void GPUMesh::bufferData(DrawParams &drawParams)
	{
		if (!mDirty)
			return;

		MutexGuard dirtyGuard(mDirtyMutex);

		if (!mDirty)
			return;

		if (Core::RenderSettings::getInstance().useStagingBuffers())
			bufferDataStaging(drawParams);
		else
			bufferDataRaw(drawParams);

		mDirty = false;
	}

	void GPUMesh::bufferDataRaw(DrawParams & drawParams)
	{
		createBuffers(vk::MemoryPropertyFlagBits::eHostVisible);

		void* bindPoint = mBuffer->map();

		copyData((char *)bindPoint + vertexOffset(), (char *)bindPoint + indexOffset());

		mBuffer->unmap();
	}

	void GPUMesh::bufferDataStaging(DrawParams &drawParams)
	{
		createBuffers(vk::MemoryPropertyFlagBits::eDeviceLocal);
		
		SharedPtr<GPUBuffer> stageBuff;
		vk::DeviceSize buffOffset;

		void* bindPoint = drawParams.stagingPool->map(mBuffer->memoryRequirements(), stageBuff, buffOffset, StagingBufferPool::PageType::Buffer);

		copyData((char *) bindPoint + vertexOffset(), (char *) bindPoint + indexOffset());

		std::array<vk::BufferCopy, 1> buffCopy;
		buffCopy[0]
			.srcOffset(buffOffset)
			.dstOffset(0)
			.size(mBuffer->vkCreateInfo().size());

		drawParams.copyCmdBuff->copyBuffer(*stageBuff, *mBuffer, (uint32_t) buffCopy.size(), buffCopy.data());
	}

	void GPUMesh::createBuffers(vk::MemoryPropertyFlags memoryProp)
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();

		const vk::BufferCreateFlags flags;
		const uint32_t queues = device.graphicsQueue().familyIndex();

		vk::BufferCreateInfo bufferCreateInfo(flags,
			mMesh.verticesCount() * sizeof(VertexData) + mMesh.indexiesCount() * sizeof(glm::uint16),
			vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferDst,
			vk::SharingMode::eExclusive,
			1,
			&queues);

		mBuffer = std::make_shared<GPUBuffer>(device, bufferCreateInfo);

		SharedPtr<GPUMemory> gpuMemory = device.memoryManager().allocateMemory(
		{ mBuffer->memoryRequirements() },
			memoryProp);

		mBuffer->bindToGpuMemory(gpuMemory);
	}

	void GPUMesh::copyData(void * vertexMemory, void * indexMemory)
	{
		VertexData *vertex = (VertexData *) vertexMemory;
		glm::uint16 *index = (glm::uint16 *) indexMemory;

		aiVector3D vec3;
		VertexData *currVertex;
		for (size_t i = 0; i < mMesh.verticesCount(); ++i)
		{
			currVertex = &vertex[i];

			vec3 = mMesh.mAiMesh->mVertices[i];
			currVertex->pos = glm::vec4(vec3.x, -vec3.y, vec3.z, 1.0f);

			if (mMesh.mAiMesh->mNormals)
				vec3 = mMesh.mAiMesh->mNormals[i];
			else
				vec3 = aiVector3D(0, -1, 0);

			currVertex->normal = glm::vec4(vec3.x, -vec3.y, vec3.z, 1.0f);

			if (mMesh.mAiMesh->mTextureCoords[0])
				vec3 = mMesh.mAiMesh->mTextureCoords[0][i];
			else
				vec3 = aiVector3D(0.5, 0.5, 0.5);

			currVertex->uv = glm::vec2(vec3.x, vec3.y);
		}

		for (size_t i = 0; i < mMesh.facesCount(); ++i)
		{
			for (size_t j = 0; j < mMesh.faceIndexiesCount(); ++j)
				index[j + i * mMesh.faceIndexiesCount()] = (glm::uint16) mMesh.mAiMesh->mFaces[i].mIndices[j];
		}
	}


}
}
}