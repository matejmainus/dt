#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/DrawParams.h"

#include "DTEngine/Engine/Dirty.h"

#include <FreeImage.h>

namespace DTEngine
{
	namespace Core
	{
		class Texture;
	}

	namespace Renderer
	{
		class GPUImage;
		class Sampler;

		namespace Entities
		{
			class GPUTexture : public Dirty
			{
			public:
				explicit GPUTexture(DTEngine::Core::Texture &texture);
				~GPUTexture();

				inline const SharedPtr<GPUImage>& image() const { return mImage; }
				inline Sampler& sampler() const { return *mSampler; }

				void bufferData(DrawParams &drawParams);
			private:
				vk::Format format(int bpp, FREE_IMAGE_TYPE type) const;
				
				void createSampler();

				void bufferDataRaw(DrawParams &drawParams);
				void bufferDataStaging(DrawParams &drawParams);

			private:
				DTEngine::Core::Texture &mTexture;

				SharedPtr<GPUImage> mImage;
				Sampler *mSampler;
			};


		}
	}
}
