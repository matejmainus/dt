#include "DTEngine/Renderer/Entities/GPUViewport.h"
#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUBuffer.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"
#include "DTEngine/Renderer/DescriptorSet.h"
#include "DTEngine/Renderer/DescriptorPool.h"
#include "DTEngine/Renderer/StagingBufferPool.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/DrawParams.h"

#include "DTEngine/Core/RenderSettings.h"

#include "DTEngine/Core/SceneObject.h"
#include "DTEngine/Core/Viewport.h"
#include "DTEngine/Core/SceneObjectComponents/Transform.h"

namespace DTEngine
{
namespace Renderer
{

namespace Entities
{
	vk::DescriptorSetLayout GPUViewport::sVkDescriptorSetLayout = nullptr;

	struct ViewportBuffer
	{
		glm::mat4 viewMatrix;
		glm::mat4 projMatrix;
	};

	GPUViewport::GPUViewport(DTEngine::Core::Viewport &viewport) :
		mViewport(viewport),
		mDescriptorSet(nullptr)
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();

		const uint32_t queue = device.graphicsQueue().familyIndex();

		vk::BufferCreateInfo bufferCreateInfo(vk::BufferCreateFlags(),
			sizeof(ViewportBuffer),
			vk::BufferUsageFlagBits::eUniformBuffer | vk::BufferUsageFlagBits::eTransferDst,
			vk::SharingMode::eExclusive,
			1,
			&queue);

		mBuffer = std::make_shared<GPUBuffer>(device, bufferCreateInfo);

		vk::MemoryPropertyFlags memoryType = Core::RenderSettings::getInstance().useStagingBuffers() ? vk::MemoryPropertyFlagBits::eDeviceLocal : vk::MemoryPropertyFlagBits::eHostVisible;

		if (Core::RenderSettings::getInstance().useMemoryPools())
			mBuffer->bindToPoolGpuMemory(memoryType);
		else
			mBuffer->bindToInherentGpuMemory(memoryType);

		mVkBufferBinding.buffer(*mBuffer)
			.offset(0)
			.range(sizeof(ViewportBuffer));
	}

	GPUViewport::~GPUViewport()
	{
		mBuffer.reset();
		mDescriptorSet.reset();
	}

	void GPUViewport::bind(DrawParams &drawParams)
	{
		mDescriptorSet = drawParams.descriptorPool->allocateDescriptorSet(sVkDescriptorSetLayout);

		vk::WriteDescriptorSet writeDescriptorSet;
		writeDescriptorSet.dstSet(*mDescriptorSet)
			.descriptorCount(1)
			.descriptorType(vk::DescriptorType::eUniformBuffer)
			.pBufferInfo(&mVkBufferBinding)
			.dstBinding(0);

		drawParams.descriptorUpdate->push_back(writeDescriptorSet);
	}

	inline void GPUViewport::bufferDataRaw(DrawParams & drawParams)
	{
		void *bufferPtr = mBuffer->map();

		copyData(bufferPtr);

		mBuffer->unmap();
	}

	inline void GPUViewport::bufferDataStaging(DrawParams & drawParams)
	{
		SharedPtr<GPUBuffer> stageBuffer;
		vk::DeviceSize offset;

		void *bufferPtr = drawParams.stagingPool->map(mBuffer->memoryRequirements(), stageBuffer, offset, StagingBufferPool::PageType::Buffer);

		copyData(bufferPtr);

		vk::BufferCopy buffCopy;
		buffCopy.srcOffset(offset)
			.dstOffset(0)
			.size(mBuffer->vkCreateInfo().size());

		drawParams.copyCmdBuff->copyBuffer(*stageBuffer, *mBuffer, 1, &buffCopy);
	}

	inline void GPUViewport::copyData(void * dstBuffer)
	{
		ViewportBuffer viewportBuffer;
		viewportBuffer.viewMatrix = mViewMatrix;
		viewportBuffer.projMatrix = mProjMatrix;

		memcpy(dstBuffer, &viewportBuffer, sizeof(ViewportBuffer));
	}

	void GPUViewport::bufferData(DrawParams &drawParams)
	{
		glm::mat4 viewMatrix = mViewport.viewMatrix();
		glm::mat4 projMatrix = mViewport.projectionMatrix();

		MutexGuard dirtyGuard(mDirtyMutex);

		if (!mDirty && mViewMatrix == viewMatrix && mProjMatrix == projMatrix)
			return;

		mViewMatrix = viewMatrix;
		mProjMatrix = projMatrix;

		if (Core::RenderSettings::getInstance().useStagingBuffers())
		{
			bufferDataStaging(drawParams);
		}
		else
		{
			bufferDataRaw(drawParams);
		}

		if(!mDescriptorSet)
			bind(drawParams);

		mDirty = false;
	}

	void GPUViewport::createDescriptorLayout()
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();

		std::array<vk::DescriptorSetLayoutBinding, 1> descritproSetBinding;
		descritproSetBinding[0].descriptorType(vk::DescriptorType::eUniformBuffer)
			.descriptorCount(1)
			.stageFlags(vk::ShaderStageFlagBits::eVertex)
			.pImmutableSamplers(false)
			.binding(0);

		vk::DescriptorSetLayoutCreateInfo descriptorLayout;
		descriptorLayout.bindingCount((uint32_t) descritproSetBinding.size())
			.pBindings(descritproSetBinding.data());

		sVkDescriptorSetLayout = device.vkDevice().createDescriptorSetLayout(descriptorLayout);
	}

	void GPUViewport::destroyDescriptorLayout()
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();

		device.vkDevice().destroyDescriptorSetLayout(sVkDescriptorSetLayout);
	}

}
}
}