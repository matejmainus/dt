#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/GPURenderer.h"
#include "DTEngine/Renderer/DescriptorSet.h"

#include "DTEngine/Engine/Dirty.h"

namespace DTEngine
{
	namespace Core
	{
		class Viewport;
	}

	namespace Renderer
	{
		class GPUBuffer;

		struct DrawParams;

		namespace Entities
		{
			class GPUViewport : public Dirty
			{
			public:
				explicit GPUViewport(DTEngine::Core::Viewport &viewport);
				~GPUViewport();

				void bufferData(DrawParams &drawParams);
	
				inline vk::DescriptorSet vkDescriptorSet() const { return *mDescriptorSet; }

				static void createDescriptorLayout();
				static void destroyDescriptorLayout();
				static vk::DescriptorSetLayout& vkDescriptorSetLayout() { return sVkDescriptorSetLayout; }

			private:
				inline void bind(DrawParams &drawParams);
				inline void bufferDataRaw(DrawParams &drawParams);
				inline void bufferDataStaging(DrawParams &drawParams);

				inline void copyData(void *dstBuffer);

			private:
				static vk::DescriptorSetLayout sVkDescriptorSetLayout;

				Core::Viewport &mViewport;

				glm::mat4 mViewMatrix;
				glm::mat4 mProjMatrix;

				SharedPtr<GPUBuffer> mBuffer;
				vk::DescriptorBufferInfo mVkBufferBinding;

				SharedPtr<DescriptorSet> mDescriptorSet;
			};

		}
	}
}