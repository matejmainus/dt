#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/GPURenderer.h"
#include "DTEngine/Renderer/DescriptorSet.h"

#include "DTEngine/Engine/Dirty.h"

namespace DTEngine
{
	namespace Core
	{
		class SceneObject;
	}

	namespace Renderer
	{
		class GPUBuffer;

		struct DrawParams;

		namespace Entities
		{
			class GPUSceneObject : public Dirty
			{
			public:
				explicit GPUSceneObject(DTEngine::Core::SceneObject &sceneObject);
				~GPUSceneObject();
				
				void bufferData(DrawParams &drawParams);
				void bufferConstants(DrawParams &drawParams);

				inline vk::DescriptorSet vkDescriptorSet() const { return *mDescriptorSet; }

				static void createPushConstantRange();
				static vk::PushConstantRange& vkPushConstantRange() { return sVkPushConstantRange; }

				static void createDescriptorLayout();
				static void destroyDescriptorLayout();
				static vk::DescriptorSetLayout vkDescriptorSetLayout() { return sVkDescriptorSetLayout; }

			private:
				inline void bind(DrawParams &drawParams);
				inline void bufferDataRaw(DrawParams &drawParams);
				inline void bufferDataStaging(DrawParams &drawParams);

				inline void copyData(void *dstBuffer);

			private:
				static vk::DescriptorSetLayout sVkDescriptorSetLayout;
				static vk::PushConstantRange sVkPushConstantRange;

				Core::SceneObject &mSceneObject;

				glm::mat4 mModelMatrix;
				glm::mat4 mViewMatrix;
				glm::mat4 mNormalMatrix;

				SharedPtr<GPUBuffer> mBuffer;
				vk::DescriptorBufferInfo mVkBufferBinding;

				SharedPtr<DescriptorSet> mDescriptorSet;
			};

		}
	}
}