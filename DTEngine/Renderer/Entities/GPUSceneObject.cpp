#include "DTEngine/Renderer/Entities/GPUSceneObject.h"
#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUBuffer.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"
#include "DTEngine/Renderer/DescriptorSet.h"
#include "DTEngine/Renderer/DescriptorPool.h"
#include "DTEngine/Renderer/StagingBufferPool.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/DrawParams.h"

#include "DTEngine/Core/RenderSettings.h"

#include "DTEngine/Core/SceneObject.h"
#include "DTEngine/Core/SceneObjectComponents/Transform.h"

#include <glm/gtc/matrix_inverse.hpp>

namespace DTEngine
{
	namespace Renderer
	{
		namespace Entities
		{
			vk::DescriptorSetLayout GPUSceneObject::sVkDescriptorSetLayout = nullptr;
			vk::PushConstantRange GPUSceneObject::sVkPushConstantRange;

			struct ObjectBuffer
			{
				glm::mat4 modelMatrix;
				glm::mat4 normalMatrix;
			};

			GPUSceneObject::GPUSceneObject(DTEngine::Core::SceneObject &sceneObject) :
				mSceneObject(sceneObject)
			{

				if (!Core::RenderSettings::getInstance().usePushConstants())
				{
					GPUDevice &device = RenderSystem::getInstance().gpuDevice();

					const uint32_t queue = device.graphicsQueue().familyIndex();

					vk::BufferCreateInfo bufferCreateInfo(vk::BufferCreateFlags(),
						sizeof(ObjectBuffer),
						vk::BufferUsageFlagBits::eUniformBuffer | vk::BufferUsageFlagBits::eTransferDst,
						vk::SharingMode::eExclusive,
						1,
						&queue);

					mBuffer = std::make_shared<GPUBuffer>(device, bufferCreateInfo);

					vk::MemoryPropertyFlags memoryType = Core::RenderSettings::getInstance().useStagingBuffers() ? vk::MemoryPropertyFlagBits::eDeviceLocal : vk::MemoryPropertyFlagBits::eHostVisible;

					if (Core::RenderSettings::getInstance().useMemoryPools())
						mBuffer->bindToPoolGpuMemory(memoryType);
					else
						mBuffer->bindToInherentGpuMemory(memoryType);

					mVkBufferBinding.buffer(*mBuffer)
						.offset(0)
						.range(sizeof(ObjectBuffer));
				}
			}

			GPUSceneObject::~GPUSceneObject()
			{
				mBuffer.reset();
				mDescriptorSet.reset();
			}

			void GPUSceneObject::bind(DrawParams &drawParams)
			{
				mDescriptorSet = drawParams.descriptorPool->allocateDescriptorSet(sVkDescriptorSetLayout);

				vk::WriteDescriptorSet writeDescriptorSet;
				writeDescriptorSet.dstSet(*mDescriptorSet)
					.descriptorCount(1)
					.descriptorType(vk::DescriptorType::eUniformBuffer)
					.pBufferInfo(&mVkBufferBinding)
					.dstBinding(0);

				drawParams.descriptorUpdate->push_back(writeDescriptorSet);
			}

			void GPUSceneObject::bufferData(DrawParams &drawParams)
			{
				MutexGuard dirtyGuard(mDirtyMutex);

				if (mModelMatrix == drawParams.modelMatrix && mViewMatrix == drawParams.viewMatrix && !mDirty)
					return;

				mModelMatrix = drawParams.modelMatrix;
				mViewMatrix = drawParams.viewMatrix;
				mNormalMatrix = glm::mat4(glm::inverseTranspose(glm::mat3(drawParams.viewMatrix * drawParams.modelMatrix)));

				if(!Core::RenderSettings::getInstance().usePushConstants())
				{ 
					if (Core::RenderSettings::getInstance().useStagingBuffers())
					{
						bufferDataStaging(drawParams);
					}
					else
					{
						bufferDataRaw(drawParams);
					}

					if (!mDescriptorSet)
						bind(drawParams);
				}

				mDirty = false;
			}

			void GPUSceneObject::bufferConstants(DrawParams & drawParams)
			{
				std::array<uint8_t, sizeof(ObjectBuffer)> data;
				copyData(data.data()); 

				drawParams.drawCmdBuff->vkCommandBuffer().pushConstants(drawParams.currentPipelineLayout, vk::ShaderStageFlagBits::eVertex, 0, (uint32_t) data.size(), data.data());
			}

			void GPUSceneObject::bufferDataRaw(DrawParams & drawParams)
			{
				void *bufferPtr = mBuffer->map();

				copyData(bufferPtr);

				mBuffer->unmap();
			}

			void GPUSceneObject::bufferDataStaging(DrawParams & drawParams)
			{
				SharedPtr<GPUBuffer> stageBuffer;
				vk::DeviceSize offset;

				void* bufferPtr = drawParams.stagingPool->map(mBuffer->memoryRequirements(), stageBuffer, offset, StagingBufferPool::PageType::Buffer);

				copyData(bufferPtr);

				vk::BufferCopy buffCopy;
				buffCopy.srcOffset(offset)
					.dstOffset(0)
					.size(mBuffer->vkCreateInfo().size());

				drawParams.copyCmdBuff->copyBuffer(*stageBuffer, *mBuffer, 1, &buffCopy);
			}

			void GPUSceneObject::copyData(void * dstBuffer)
			{
				ObjectBuffer objBuffer;
				objBuffer.modelMatrix = mModelMatrix;
				objBuffer.normalMatrix = mNormalMatrix;

				memcpy(dstBuffer, &objBuffer, sizeof(ObjectBuffer));
			}

			void GPUSceneObject::createPushConstantRange()
			{
				sVkPushConstantRange.size((uint32_t) sizeof(ObjectBuffer))
					.stageFlags(vk::ShaderStageFlagBits::eVertex)
					.offset(0);
			}

			void GPUSceneObject::createDescriptorLayout()
			{
				GPUDevice &device = RenderSystem::getInstance().gpuDevice();

				std::array<vk::DescriptorSetLayoutBinding, 1> descritproSetBinding;
				descritproSetBinding[0].descriptorType(vk::DescriptorType::eUniformBuffer)
					.descriptorCount(1)
					.stageFlags(vk::ShaderStageFlagBits::eVertex)
					.pImmutableSamplers(false)
					.binding(0);

				vk::DescriptorSetLayoutCreateInfo descriptorLayout;
				descriptorLayout.bindingCount((uint32_t)descritproSetBinding.size())
					.pBindings(descritproSetBinding.data());

				sVkDescriptorSetLayout = device.vkDevice().createDescriptorSetLayout(descriptorLayout);
			}

			void GPUSceneObject::destroyDescriptorLayout()
			{
				GPUDevice &device = RenderSystem::getInstance().gpuDevice();

				device.vkDevice().destroyDescriptorSetLayout(sVkDescriptorSetLayout);
			}
		}
	}

}

