#include "DTEngine/Renderer/Entities/GPUTexture.h"

#include "DTEngine/Core/Texture.h"
#include "DTEngine/Core/RenderSettings.h"

#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUImage.h"
#include "DTEngine/Renderer/GPUBuffer.h"
#include "DTEngine/Renderer/Sampler.h"
#include "DTEngine/Renderer/DrawParams.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/StagingBufferPool.h"

namespace DTEngine
{
namespace Renderer
{
namespace Entities
{
	GPUTexture::GPUTexture(DTEngine::Core::Texture &texture) : 
		mTexture(texture),
		mSampler(nullptr)
	{
		createSampler();
	}

	GPUTexture::~GPUTexture()
	{
		if (mSampler)
			delete mSampler;
	}

	void GPUTexture::bufferData(DrawParams &drawParams)
	{
		if (!mDirty)
			return;

		MutexGuard dirtyGuard(mDirtyMutex);

		if (!mDirty)
			return;

		if (Core::RenderSettings::getInstance().useStagingBuffers())
			bufferDataStaging(drawParams);
		else
			bufferDataRaw(drawParams);

		mDirty = false;
	}
	
	vk::Format GPUTexture::format(int bpp, FREE_IMAGE_TYPE type) const
	{
		return vk::Format::eR32G32B32A32Sfloat;
	}

	void GPUTexture::createSampler()
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();

		vk::SamplerCreateInfo createInfo;
		createInfo.magFilter(vk::Filter::eLinear)
			.minFilter(vk::Filter::eLinear)
			.mipmapMode(vk::SamplerMipmapMode::eLinear)
			.addressModeU(vk::SamplerAddressMode::eRepeat)
			.addressModeV(vk::SamplerAddressMode::eRepeat)
			.addressModeW(vk::SamplerAddressMode::eRepeat)
			.minLod(0.0f)
			.maxLod(1.0f)
			.maxAnisotropy(1)
			.anisotropyEnable(false)
			.compareEnable(false)
			.borderColor(vk::BorderColor::eFloatOpaqueBlack);

		mSampler = new Sampler(device, createInfo);
	}

	void GPUTexture::bufferDataRaw(DrawParams &drawParams)
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();

		FIBITMAP *bmp = FreeImage_ConvertToRGBAF(mTexture.mBitmap);

		vk::Extent3D extent((uint32_t) mTexture.width(), (uint32_t) mTexture.height(), 1);

		size_t bmpSize = 4 * sizeof(float) * extent.width() * extent.height() * extent.depth();

		vk::ImageCreateInfo createInfo;
		createInfo.imageType(vk::ImageType::e2D)
			.samples(vk::SampleCountFlagBits::e1)
			.mipLevels(1)
			.arrayLayers(1)
			.sharingMode(vk::SharingMode::eExclusive)
			.initialLayout(vk::ImageLayout::ePreinitialized)
			.tiling(vk::ImageTiling::eLinear)
			.usage(vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferDst)
			.format(vk::Format::eR32G32B32A32Sfloat)
			.extent(extent);

		mImage = std::make_shared<GPUImage>(device, createInfo);
		mImage->bindToInherentGpuMemory(vk::MemoryPropertyFlagBits::eHostVisible);

		void* imagePtr = mImage->map();

		memcpy(imagePtr, FreeImage_GetBits(bmp), bmpSize);

		mImage->unmap();

		vk::CommandBuffer cmdBuff = drawParams.copyCmdBuff->vkCommandBuffer();

		mImage->barrier(cmdBuff, vk::ImageLayout::ePreinitialized, vk::ImageLayout::eShaderReadOnlyOptimal,
			vk::AccessFlagBits::eTransferWrite | vk::AccessFlagBits::eHostWrite, vk::AccessFlagBits::eShaderRead,
			vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));

		FreeImage_Unload(bmp);
	}

	void GPUTexture::bufferDataStaging(DrawParams &drawParams)
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();

		FIBITMAP *bmp = FreeImage_ConvertToRGBAF(mTexture.mBitmap);

		vk::Extent3D extent((uint32_t) mTexture.width(), (uint32_t)mTexture.height(), 1);

		size_t bmpSize = 4 * sizeof(float) * extent.width() * extent.height() * extent.depth();

		vk::ImageCreateInfo createInfo;
		createInfo.imageType(vk::ImageType::e2D)
			.samples(vk::SampleCountFlagBits::e1)
			.mipLevels(1)
			.arrayLayers(1)
			.sharingMode(vk::SharingMode::eExclusive)
			.initialLayout(vk::ImageLayout::ePreinitialized)
			.tiling(vk::ImageTiling::eOptimal)
			.usage(vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferDst)
			.format(vk::Format::eR32G32B32A32Sfloat)
			.extent(extent);

		mImage = std::make_shared<GPUImage>(device, createInfo);
		mImage->bindToInherentGpuMemory(vk::MemoryPropertyFlagBits::eDeviceLocal);

		DTEngine::SharedPtr<GPUBuffer> stageBuffer;
		vk::DeviceSize stBufferOffset;

		void* bufferPtr = drawParams.stagingPool->map(mImage->memoryRequirements(), stageBuffer, stBufferOffset, StagingBufferPool::PageType::Image);

		memcpy(bufferPtr, FreeImage_GetBits(bmp), bmpSize);

		vk::BufferImageCopy buffCopy;
		buffCopy.bufferOffset(stBufferOffset)
			.imageOffset(vk::Offset3D(0, 0, 0))
			.imageSubresource(vk::ImageSubresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1))
			.imageExtent(extent);

		vk::CommandBuffer cmdBuff = drawParams.copyCmdBuff->vkCommandBuffer();

		mImage->barrier(cmdBuff, vk::ImageLayout::ePreinitialized, vk::ImageLayout::eTransferDstOptimal,
			vk::AccessFlagBits::eHostWrite | vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eTransferWrite,
			vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));

		drawParams.copyCmdBuff->copyBufferToImage(*stageBuffer, *mImage, vk::ImageLayout::eTransferDstOptimal, 1, &buffCopy);

		mImage->barrier(cmdBuff, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal,
			vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eShaderRead,
			vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));

		FreeImage_Unload(bmp);
	}
}
}
}