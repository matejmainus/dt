#pragma once

#include "DTEngine/Renderer/Prereq.h"

#include "DTEngine/Engine/Lockable.h"
#include "DTEngine/Engine/Dirty.h"

namespace DTEngine
{
	namespace Core
	{
		class Material;

		namespace SceneObjectComponents
		{
			class Transform;
		}
	}

	namespace Renderer
	{
		class Pipeline;
		class GPUBuffer;
		class DescriptorSet;

		struct DrawParams;

		namespace Entities
		{
			class GPUMaterial : public Lockable, public Dirty
			{
			public:
				explicit GPUMaterial(Core::Material &material);
				virtual ~GPUMaterial();

				virtual void bufferData(DrawParams &drawParams) = 0;

				virtual Pipeline& pipeline() = 0;
				virtual vk::DescriptorSet vkDescriptorSet() = 0;

				template<typename T>
				inline T& material() const { return (T&) (mMaterial); };

			protected:
				Core::Material &mMaterial;
			};
		}

	}


}
