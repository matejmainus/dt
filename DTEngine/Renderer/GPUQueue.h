#pragma once

#include <vector>

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class CommandPool;

		class GPUQueue
		{
		public:
			explicit GPUQueue(GPUDevice &device, uint32_t queueFamilyIndex);
			virtual ~GPUQueue();

			uint32_t familyIndex() const;

			void submit(uint32_t submitSize, vk::SubmitInfo* submitInfos, vk::Fence fence = VK_NULL_HANDLE);
			void submit(vk::SubmitInfo &submitInfo, vk::Fence fence = VK_NULL_HANDLE);

			inline CommandPool& globalCommandPool() const { return *mGlobalCmdPool; }

			void waitToIdle() const;

		protected:
			mutable std::mutex mQueueMutex;

			GPUDevice &mDevice;

			vk::Queue mVkQueue;

			uint32_t mFamilyIndex;

			CommandPool *mGlobalCmdPool;
		};

	}
}