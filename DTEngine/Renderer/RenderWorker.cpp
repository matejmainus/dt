#include "DTEngine/Renderer/RenderWorker.h"
#include "DTEngine/Renderer/GPURenderer.h"
#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"
#include "DTEngine/Renderer/CommandPool.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/DescriptorPool.h"
#include "DTEngine/Renderer/StagingBufferPool.h"
#include "DTEngine/Renderer/DescriptorSet.h"
#include "DTEngine/Renderer/RenderTarget.h"
#include "DTEngine/Renderer/DrawParams.h"

#include "DTEngine/Core/RenderSettings.h"
#include "DTEngine/Core/Scene.h"
#include "DTEngine/Core/SceneObject.h"
#include "DTEngine/Core/SceneObjectComponent.h"
#include "DTEngine/Core/SceneObjectComponents/Transform.h"
#include "DTEngine/Core/Renderer.h"
#include "DTEngine/Core/Viewport.h"

#include <vector>

#define DEFAULT_STAGE_PAGE_SIZE 8 * 1024
#define DEFAULT_STAGE_PAGE_ALIGNMENT 4 *1024
#define INITIAL_STAGE_POOL_SIZE 2

namespace DTEngine
{
namespace Renderer
{
	class RenderThread
	{
	public:
		RenderThread(RenderWorker *worker) :
			mWorker(worker),
			mDevice(RenderSystem::getInstance().gpuDevice())
		{
			mThread = new std::thread(&RenderThread::run, this);

			createCommandPool();
			createDescriptorPool();

			mStagingPool = new StagingBufferPool(mDevice, DEFAULT_STAGE_PAGE_SIZE, DEFAULT_STAGE_PAGE_ALIGNMENT, INITIAL_STAGE_POOL_SIZE);
		}

		~RenderThread()
		{
			delete mDescriptorPool;
			delete mCommandPool;
			delete mStagingPool;

			delete mThread;
		}
	
		void run()
		{
			while (mWorker->mActive)
			{
				mWorker->fetchNode(mCurrentNode);

				if (mCurrentNode == nullptr)
					continue;

				RenderPhaseParams& phaseParams = mWorker->mPhaseParams;

				SharedPtr<CommandBuffer> drawCmdBuff = mCommandPool->allocCommandBuffer(vk::CommandBufferLevel::eSecondary);
				SharedPtr<CommandBuffer> copyCmdBuff = mCommandPool->allocCommandBuffer(vk::CommandBufferLevel::eSecondary);

				vk::CommandBufferInheritanceInfo drawInheritanceInfo;
				drawInheritanceInfo.framebuffer(phaseParams.frameBuffer)
					.occlusionQueryEnable(false)
					.pipelineStatistics(vk::QueryPipelineStatisticFlagBits())
					.renderPass(phaseParams.renderPass)
					.subpass(phaseParams.subPassId);

				vk::CommandBufferBeginInfo drawCmdBeginInfo;
				drawCmdBeginInfo.flags(vk::CommandBufferUsageFlagBits::eRenderPassContinue | vk::CommandBufferUsageFlagBits::eOneTimeSubmit)
					.pInheritanceInfo(&drawInheritanceInfo);

				vk::CommandBufferInheritanceInfo copyInheritanceInfo;

				vk::CommandBufferBeginInfo copyCmdBeginInfo;
				copyCmdBeginInfo.flags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit)
					.pInheritanceInfo(&copyInheritanceInfo);

				drawCmdBuff->vkCommandBuffer().begin(drawCmdBeginInfo);
				copyCmdBuff->vkCommandBuffer().begin(copyCmdBeginInfo);

				vk::Viewport viewport(0.0f, 0.0f, 
					(float) phaseParams.target->width(),
					(float) phaseParams.target->height(),
					0.0f, 1.0f
				);

				vk::Rect2D scissor(
					vk::Offset2D(0, 0),
					vk::Extent2D(phaseParams.target->width(), phaseParams.target->height())
				);

				drawCmdBuff->vkCommandBuffer().setViewport(0, 1, &viewport);
				drawCmdBuff->vkCommandBuffer().setScissor(0, 1, &scissor);
	
				std::vector<vk::WriteDescriptorSet> updateDescriptorSet;
				std::vector<vk::CopyDescriptorSet> copyDescriptorSet;

				mDrawParams.drawCmdBuff = drawCmdBuff.get();
				mDrawParams.copyCmdBuff = copyCmdBuff.get();
				mDrawParams.descriptorUpdate = &updateDescriptorSet;
				
				mDrawParams.currentPipeline = nullptr;
				mDrawParams.currentMaterial = nullptr;
				mDrawParams.currentVertexBuffer = nullptr;
				mDrawParams.currentIndexBuffer = nullptr;

				mDrawParams.viewMatrix = mWorker->mPhaseParams.viewport->viewMatrix();
				mDrawParams.projectionMatrix = mWorker->mPhaseParams.viewport->projectionMatrix();

				mWorker->mPhaseParams.viewport->gpuViewport()->bufferData(mDrawParams);
				mDrawParams.viewport = phaseParams.viewport->gpuViewport()->vkDescriptorSet();

				for (const RenderContext &context : mCurrentNode->childs)
				{
					mDrawParams.modelMatrix = context.modelMatrix;

					context.renderer->prepare(mDrawParams);
					context.renderer->bufferData(mDrawParams);
				}

				mDevice.updateDescriptorSets(updateDescriptorSet, copyDescriptorSet);
			
				for (const RenderContext &context : mCurrentNode->childs)
				{
					context.renderer->draw(mDrawParams);
				}

				drawCmdBuff->vkCommandBuffer().end();
				copyCmdBuff->vkCommandBuffer().end();

				executeSubCommand(mWorker->mPhaseParams.drawCommandBuffer, mWorker->mPhaseParams.commandPool, drawCmdBuff);
				executeSubCommand(mWorker->mPhaseParams.copyCommandBuffer, mWorker->mPhaseParams.commandPool, copyCmdBuff);

				mWorker->endNode(mCurrentNode);
			}
		}

		void executeSubCommand(const SharedPtr<CommandBuffer> &primary, CommandPool* pool, const SharedPtr<CommandBuffer> &secondary)
		{
			vk::CommandBuffer vkSecCmdBuff = secondary->vkCommandBuffer();

			std::lock(*primary, *pool);
	
			primary->vkCommandBuffer().executeCommands(1, &vkSecCmdBuff);

			pool->unlock();
			primary->unlock();
		}

		void createCommandPool()
		{
			vk::CommandPoolCreateInfo cmdPoolCreateInfo;
			cmdPoolCreateInfo.queueFamilyIndex(mDevice.graphicsQueue().familyIndex());

			mCommandPool = new CommandPool(mDevice, cmdPoolCreateInfo);
		}

		void createDescriptorPool()
		{
			std::array<vk::DescriptorPoolSize, 2> sizes;
			sizes[0].type(vk::DescriptorType::eUniformBuffer).descriptorCount(5000);
			sizes[1].type(vk::DescriptorType::eCombinedImageSampler).descriptorCount(2000);

			vk::DescriptorPoolCreateInfo descPoolCreateInfo;
			descPoolCreateInfo.poolSizeCount((uint32_t)sizes.size())
				.pPoolSizes(sizes.data())
				.maxSets(7000)
				.flags(vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet);

			mDescriptorPool = new DescriptorPool(mDevice, descPoolCreateInfo);
		}

		void join()
		{
			mThread->join();
		}

		void prepare()
		{
			mCommandPool->freeCommandBuffers();
			mStagingPool->freePages();
		}

		void end()
		{
			mStagingPool->flushPages();
		}

		void begin()
		{
			RenderPhaseParams& phaseParams = mWorker->mPhaseParams;

			mDrawParams.descriptorPool = mDescriptorPool;
			mDrawParams.stagingPool = mStagingPool;
		}

	public:
		RenderWorker *mWorker;

		std::thread *mThread;
		RenderNodeSPtr mCurrentNode;

		GPUDevice &mDevice;

		DescriptorPool *mDescriptorPool;
		CommandPool *mCommandPool;
		StagingBufferPool *mStagingPool;

		DrawParams mDrawParams;
	};

	RenderWorker::RenderWorker() :
		mActive(true)
	{
		mCurrentNode = mTree.end();

		initRenderThreads(DTEngine::Core::RenderSettings::getInstance().renderThreadsCount());
	}

	RenderWorker::~RenderWorker()
	{
		mActive = false;

		stopRenderThreads();

		for (RenderThread* const &renderThread : mThreads)
			delete renderThread;

		mThreads.clear();
	}

	void RenderWorker::prepare()
	{
		for (RenderThread* const &renderThread : mThreads)
			renderThread->prepare();
	}

	void RenderWorker::initRenderThreads(size_t count)
	{
		for (size_t i = 0; i < count; ++i)
		{
			RenderThread *thread = new RenderThread(this);
			mThreads.push_back(thread);
		}
	}

	void RenderWorker::stopRenderThreads()
	{
		mTree.lock();

		mTree.clear();
		mCurrentNode = mTree.end();

		mTree.unlock();

		mTreeCondition.notify_all();
		mJoinCondition.notify_all();

		for (RenderThread* const &renderThread : mThreads)
		{
			renderThread->join();
		}
	}

	void RenderWorker::renderScene(const RenderPhaseParams &params)
	{
		mPhaseParams = params;

		RenderContext context;
		context.renderer = nullptr;
		context.modelMatrix = glm::mat4();
		context.hash = 0;

		mTree.lock();

		beginTraverse();
		traverseScene(context, mPhaseParams.scene->rootObject());
		endTraverse();

		mTree.unlock();

		beginRender();
	}

	void RenderWorker::join()
	{
		std::unique_lock<std::mutex> lock(mJoinConditionMutex);
		while (isRenderRunning() && mActive)
		{
			mJoinCondition.wait(lock);
		}
	}

	void RenderWorker::fetchNode(SharedPtr<RenderNode> &node)
	{
		std::unique_lock<std::mutex> waitLock(mTreeCondMutex);
		while (isCurrentAtEnd() && mActive)
			mTreeCondition.wait(waitLock);

		mTree.lock();

		do
		{
			if (mCurrentNode == mTree.end())
			{
				node = nullptr;
				break;
			}

			node = *mCurrentNode;

			mCurrentNode++;
		} while (node->childs.size() == 0);

		mTree.unlock();
	}

	void RenderWorker::endNode(SharedPtr<RenderNode> &node)
	{
		mTree.lock();

		node = nullptr;

		mTree.unlock();

		if (!isRenderRunning())
			endRender();
	}
	
	bool RenderWorker::isCurrentAtEnd() const
	{
		MutexGuard guard(mTree.mutex());

		return mCurrentNode == mTree.cend();
	}

	void RenderWorker::beginTraverse()
	{
		mTree.clear();
		mCurrentNode = mTree.end();
	}

	void RenderWorker::traverseScene(const RenderContext &context, const Core::SceneObject &sceneObject)
	{
		Core::SceneObject::ChildsVec childs;

		Core::SceneObjectComponents::Transform *transform;
		std::vector<Core::Renderer *> renderers;

		if (!sceneObject.active())
			return;

		sceneObject.childs(childs);

		RenderContext childContext = context;
		glm::mat4 parentMatrix = childContext.modelMatrix;

		for (Core::SceneObject* const &child : childs)
		{
			renderers.clear();
			child->components<Core::Renderer>(renderers);

			transform = child->component<Core::SceneObjectComponents::Transform>();

			childContext.modelMatrix = parentMatrix * transform->matrix();

			for (Core::Renderer* const &renderer : renderers)
			{
				if (renderer->gpuRenderer()->canRender())
				{
					childContext.renderer = renderer->gpuRenderer();
					childContext.hash = renderer->gpuRenderer()->hash();

					mTree.add(childContext);
				}
			}

			traverseScene(childContext, *child);
		}
	}

	void RenderWorker::endTraverse()
	{
		mTree.toNodes();
	}

	void RenderWorker::beginRender()
	{
		for (RenderThread* const &renderThread : mThreads)
			renderThread->begin();

		mTreeCondMutex.lock();

		mCurrentNode = mTree.begin();

		mTreeCondMutex.unlock();

		mTreeCondition.notify_all();
	}

	void RenderWorker::endRender()
	{
		for (RenderThread* const &renderThread : mThreads)
			renderThread->end();

		MutexGuard guard(mJoinConditionMutex);
		mJoinCondition.notify_all();
	}

	bool RenderWorker::isRenderRunning() const
	{
		MutexGuard guard(mTree.mutex());

		if (mCurrentNode != mTree.cend())
			return true;

		for (RenderThread* const &thread : mThreads)
		{
			if (thread->mCurrentNode != nullptr)
				return true;
		}

		return false;
	}
}
}