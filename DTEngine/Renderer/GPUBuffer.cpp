#include "DTEngine/Renderer/GPUBuffer.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUMemory.h"

#include "DTEngine/Core/RenderStatsManager.h"

namespace DTEngine
{
namespace Renderer
{

	GPUBuffer::GPUBuffer(GPUDevice &device, vk::BufferCreateInfo &createInfo) :
		GPUResource(device),
		mVkCreateInfo(createInfo)
	{
		mVkBuffer = mDevice.vkDevice().createBuffer(createInfo);

		mVkMemoryRequirements = mDevice.vkDevice().getBufferMemoryRequirements(mVkBuffer);

		Core::RenderStatsManager::getInstance().usageStats()->gpuBufferCount++;
	}

	GPUBuffer::~GPUBuffer()
	{
		mDevice.vkDevice().destroyBuffer(mVkBuffer);

		Core::RenderStatsManager::getInstance().usageStats()->gpuBufferCount--;
	}

	void GPUBuffer::bind(vk::DeviceMemory deviceMemory, vk::DeviceSize offset)
	{
		mDevice.vkDevice().bindBufferMemory(mVkBuffer, deviceMemory, offset);
	}

	SharedPtr<GPUBufferView> GPUBuffer::view(vk::Format format, vk::DeviceSize offset, vk::DeviceSize range)
	{
		vk::BufferViewCreateFlags flags;

		vk::BufferViewCreateInfo createInfo(flags, mVkBuffer, format, offset, range);

		SharedPtr<GPUBufferView> viewSptr = std::make_shared<GPUBufferView>(*this, createInfo);

		addView(viewSptr);

		return viewSptr;
	}

	/** VIEW ***/

	GPUBufferView::GPUBufferView(GPUBuffer &buffer, vk::BufferViewCreateInfo &createInfo) :
		mBuffer(buffer)
	{
		mVkView = mBuffer.device().vkDevice().createBufferView(createInfo);
	}

	GPUBufferView::~GPUBufferView()
	{
		mBuffer.device().vkDevice().destroyBufferView(mVkView);
	}
}
}