#include "DTEngine/Renderer/Compositor.h"
#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/RenderWorker.h"
#include "DTEngine/Renderer/RenderTarget.h"
#include "DTEngine/Renderer/CommandPool.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/GPUImage.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"

#include "DTEngine/Engine/Log.h"
#include "DTEngine/Core/Scene.h"
#include "DTEngine/Core/Viewport.h"
#include "DTEngine/Core/RenderStatsManager.h"

namespace DTEngine
{
namespace Renderer
{

	Compositor::Compositor() :
		Singleton<Compositor>(),
		mRenderWorker(nullptr),
		mActiveScene(nullptr),
		mActiveViewport(nullptr),
		mVkRenderPass(VK_NULL_HANDLE),
		mVkPresentSemaphore(VK_NULL_HANDLE),
		mVkCopyEvent(VK_NULL_HANDLE)
	{
		Log::info("Compositor created");
	}

	Compositor::~Compositor()
	{
		Log::info("Compositor destroyed");
	}

	void Compositor::init()
	{
		mDevice = &RenderSystem::getInstance().gpuDevice();

		mTimestampPeriod = mDevice->vkPhysicalDevice().getProperties().limits().timestampPeriod();

		mRenderWorker = new RenderWorker();

		createRenderPass();

		createTimerQueryPool();

		vk::EventCreateInfo copyEvtCreateInfo;
		mVkCopyEvent = mDevice->vkDevice().createEvent(copyEvtCreateInfo);
	}

	void Compositor::shutdown()
	{
		mActiveScene = nullptr;
		mActiveViewport = nullptr;

		mDevice->vkDevice().waitIdle();

		destroySynchronization();

		mDevice->vkDevice().destroyEvent(mVkCopyEvent);
		mDevice->vkDevice().destroyQueryPool(mVkTimerQueryPool);

		if (mRenderWorker)
			delete mRenderWorker;

		if(mVkRenderPass)
			mDevice->vkDevice().destroyRenderPass(mVkRenderPass, nullptr);
	}

	void Compositor::setActiveScene(Core::Scene* scene)
	{
		MutexGuard locker(mGuard);

		mActiveScene = scene;
	}

	void Compositor::setActiveViewport(Core::Viewport* viewport)
	{
		MutexGuard locker(mGuard);

		mActiveViewport = viewport;
	}

	void Compositor::draw()
	{
		MutexGuard locker(mGuard);

		RenderPhaseParams renderPhase = {};
		renderPhase.scene = mActiveScene;

		GPUDevice &device = RenderSystem::getInstance().gpuDevice();
		GPUGraphicsQueue &queue = device.graphicsQueue();

		prepareSynchronization();

		SharedPtr<RenderTarget> renderTarget = mActiveViewport->renderTarget();

		renderTarget->nextTarget(mVkPresentSemaphore);
		vk::Framebuffer frameBuffer = renderTarget->targetFramebuffer();

		SharedPtr<CommandBuffer> drawCmdBuff = queue.globalCommandPool()
			.allocCommandBuffer(vk::CommandBufferLevel::ePrimary);

		SharedPtr<CommandBuffer> copyCmdBuff = queue.globalCommandPool()
			.allocCommandBuffer(vk::CommandBufferLevel::ePrimary);

		vk::CommandBufferBeginInfo commandBuffBeginInfo;
		commandBuffBeginInfo.flags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

		drawCmdBuff->vkCommandBuffer().begin(commandBuffBeginInfo);
		copyCmdBuff->vkCommandBuffer().begin(commandBuffBeginInfo);

		renderPhase.target = renderTarget;
		renderPhase.commandPool = &queue.globalCommandPool();
		renderPhase.drawCommandBuffer = drawCmdBuff;
		renderPhase.copyCommandBuffer = copyCmdBuff;
		renderPhase.viewport = mActiveViewport;
		renderPhase.frameBuffer = frameBuffer;
		renderPhase.renderPass = mVkRenderPass;
		renderPhase.subPassId = 0;

		glm::vec4 backgroundColor = mActiveScene->backgroundColor().normVec();

		std::array<float, 4> sceneBackground = {
			backgroundColor.r,
			backgroundColor.g,
			backgroundColor.b,
			1.0f
		};

		vk::ClearValue clearValues[2];
		clearValues[0].depthStencil(vk::ClearDepthStencilValue(1.0f, 0));
		clearValues[1].color(vk::ClearColorValue(sceneBackground));

		vk::Rect2D renderArea;
		renderArea.extent({renderTarget->width(), renderTarget->height()})
			.offset({ 0,0 });

		vk::RenderPassBeginInfo renderPassBeginInfo;
		renderPassBeginInfo.renderPass(mVkRenderPass)
			.renderArea(renderArea)
			.clearValueCount(2)
			.pClearValues(clearValues)
			.framebuffer(frameBuffer);

		drawCmdBuff->vkCommandBuffer().resetQueryPool(mVkTimerQueryPool, 0, 2);
		drawCmdBuff->vkCommandBuffer().writeTimestamp(vk::PipelineStageFlagBits::eTopOfPipe, mVkTimerQueryPool, 0);

	/*	prepareRenderTarget(drawCmdBuff, renderTarget,
			vk::AccessFlags(), vk::AccessFlagBits::eColorAttachmentWrite,
			vk::ImageLayout::ePresentSrcKHR, vk::ImageLayout::eColorAttachmentOptimal);
			*/
		drawCmdBuff->vkCommandBuffer().waitEvents(1, &mVkCopyEvent, vk::PipelineStageFlagBits::eBottomOfPipe, vk::PipelineStageFlagBits::eTopOfPipe, 0, nullptr, 0, nullptr, 0, nullptr);

		drawCmdBuff->vkCommandBuffer().beginRenderPass(&renderPassBeginInfo, vk::SubpassContents::eInline);

		mRenderWorker->prepare();
		mRenderWorker->renderScene(renderPhase);
		mRenderWorker->join();

		//Could not be eTransfare due to some bug?? AMD driver crash
		copyCmdBuff->vkCommandBuffer().setEvent(mVkCopyEvent, vk::PipelineStageFlagBits::eBottomOfPipe);

		drawCmdBuff->vkCommandBuffer().endRenderPass();
		/*
		prepareRenderTarget(drawCmdBuff, renderTarget,
			vk::AccessFlagBits::eColorAttachmentWrite, vk::AccessFlags(),
			vk::ImageLayout::eColorAttachmentOptimal, vk::ImageLayout::ePresentSrcKHR);
			*/

		drawCmdBuff->vkCommandBuffer().writeTimestamp(vk::PipelineStageFlagBits::eTopOfPipe, mVkTimerQueryPool, 1);

		copyCmdBuff->vkCommandBuffer().end();
		drawCmdBuff->vkCommandBuffer().end();

		vk::PipelineStageFlags dstStageMask[1];
		dstStageMask[0] = vk::PipelineStageFlagBits::eBottomOfPipe;

		std::array<vk::Semaphore, 1> waitSemaphores = { mVkPresentSemaphore };
		std::array<vk::SubmitInfo, 2> submitInfos;

		submitInfos[0].commandBufferCount(1)
			.pCommandBuffers(&copyCmdBuff->vkCommandBuffer());
		
		submitInfos[1].commandBufferCount(1)
			.pCommandBuffers(&drawCmdBuff->vkCommandBuffer())
			.waitSemaphoreCount((uint32_t) waitSemaphores.size())
			.pWaitSemaphores(waitSemaphores.data())
			.pWaitDstStageMask(dstStageMask);
		
		queue.submit((uint32_t) submitInfos.size(), submitInfos.data());
		renderTarget->present();
	}

	void Compositor::wait()
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();
		GPUGraphicsQueue &queue = device.graphicsQueue();

		queue.waitToIdle();

		calcGPUTime();

		queue.globalCommandPool().freeCommandBuffers();
	}

	void Compositor::createRenderPass()
	{
		GPUDevice &device = RenderSystem::getInstance().gpuDevice();

		vk::AttachmentDescription attachments[2];
		attachments[0].format(device.getSupportedDepthFormat())
			.samples(vk::SampleCountFlagBits::e1)
			.loadOp(vk::AttachmentLoadOp::eClear)
			.storeOp(vk::AttachmentStoreOp::eStore)
			.stencilLoadOp(vk::AttachmentLoadOp::eClear)
			.stencilStoreOp(vk::AttachmentStoreOp::eStore)
			.initialLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal)
			.finalLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);

		attachments[1].format(vk::Format::eR8G8B8A8Unorm)
			.samples(vk::SampleCountFlagBits::e1)
			.loadOp(vk::AttachmentLoadOp::eClear)
			.storeOp(vk::AttachmentStoreOp::eStore)
			.stencilLoadOp(vk::AttachmentLoadOp::eDontCare)
			.stencilStoreOp(vk::AttachmentStoreOp::eDontCare)
			.initialLayout(vk::ImageLayout::ePresentSrcKHR)
			.finalLayout(vk::ImageLayout::ePresentSrcKHR);

		vk::AttachmentReference depthStencilReference;
		depthStencilReference.attachment(0)
			.layout(vk::ImageLayout::eDepthStencilAttachmentOptimal);

		vk::AttachmentReference colorReference;
		colorReference.attachment(1)
			.layout(vk::ImageLayout::eColorAttachmentOptimal);

		vk::SubpassDescription subpass;
		subpass.pipelineBindPoint(vk::PipelineBindPoint::eGraphics)
			.colorAttachmentCount(1)
			.pColorAttachments(&colorReference)
			.pDepthStencilAttachment(&depthStencilReference);
		
		vk::RenderPassCreateInfo createInfo;
		createInfo.attachmentCount(2)
			.pAttachments(attachments)
			.subpassCount(1)
			.pSubpasses(&subpass);

		device.vkDevice().createRenderPass(&createInfo, nullptr, &mVkRenderPass);
	}

	void Compositor::createTimerQueryPool()
	{
		vk::QueryPoolCreateInfo createInfo;
		createInfo.queryCount(2)
			.queryType(vk::QueryType::eTimestamp);

		mVkTimerQueryPool = mDevice->vkDevice().createQueryPool(createInfo);
	}

	void Compositor::prepareSynchronization()
	{
		destroySynchronization();

		vk::SemaphoreCreateInfo semaphoreCreateInfo;
		mDevice->vkDevice().createSemaphore(&semaphoreCreateInfo, nullptr, &mVkPresentSemaphore);

		mDevice->vkDevice().resetEvent(mVkCopyEvent);
	}

	void Compositor::destroySynchronization()
	{
		if (mVkPresentSemaphore)
			mDevice->vkDevice().destroySemaphore(mVkPresentSemaphore, nullptr);

		mVkPresentSemaphore = VK_NULL_HANDLE;
	}

	void Compositor::calcGPUTime()
	{
		std::vector<uint32_t> timestampData(2);
		mDevice->vkDevice().getQueryPoolResults(mVkTimerQueryPool, 0, 2, timestampData, sizeof(uint32_t), vk::QueryResultFlags());

		uint32_t timestampDiff = timestampData[1] - timestampData[0];
		float miliseconds = timestampDiff * mTimestampPeriod / 1000000;

		Core::RenderStatsManager::getInstance().currentFrame()->gpuFrameTime = DTEngine::Millisecs((size_t)miliseconds);
	}

	void Compositor::prepareRenderTarget(SharedPtr<CommandBuffer> cmdBuff, const SharedPtr<RenderTarget> &target,
		vk::AccessFlags srcFlags, vk::AccessFlags dstFlags,
		vk::ImageLayout oldLayout, vk::ImageLayout newLayout)
	{
		vk::ImageSubresourceRange range;
		range.aspectMask(vk::ImageAspectFlagBits::eColor)
			.layerCount(1)
			.baseArrayLayer(0)
			.levelCount(1)
			.baseMipLevel(0);

		vk::ImageMemoryBarrier transitionBarrier;
		transitionBarrier.dstAccessMask(vk::AccessFlagBits::eColorAttachmentWrite)
			.oldLayout(oldLayout)
			.newLayout(newLayout)
			.srcAccessMask(srcFlags)
			.dstAccessMask(dstFlags)
			.srcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
			.dstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
			.subresourceRange(range)
			.image(*target->targetImage());

		cmdBuff->vkCommandBuffer().pipelineBarrier(
			vk::PipelineStageFlagBits::eAllCommands,
			vk::PipelineStageFlagBits::eTopOfPipe,
			vk::DependencyFlags(),
			0, nullptr,
			0, nullptr,
			1, &transitionBarrier
			);
	}
}
}

