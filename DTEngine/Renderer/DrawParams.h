#pragma once

#include <vector>

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class CommandBuffer;
		class Pipeline;
		class DescriptorPool;
		class StagingBufferPool;
	
		struct RenderNode;
		struct RenderNodeDiff;

		struct DrawParams
		{
			CommandBuffer *drawCmdBuff;
			CommandBuffer *copyCmdBuff;
			std::vector<vk::WriteDescriptorSet> *descriptorUpdate;

			DescriptorPool *descriptorPool;
			StagingBufferPool *stagingPool;

			vk::DescriptorSet viewport;
			vk::DescriptorSet scene;

			vk::Pipeline currentPipeline;
			vk::PipelineLayout currentPipelineLayout;
			vk::DescriptorSet currentMaterial;
			vk::Buffer currentVertexBuffer;
			vk::Buffer currentIndexBuffer;

			glm::mat4 modelMatrix;
			glm::mat4 viewMatrix;
			glm::mat4 projectionMatrix;
		};

	}
}
