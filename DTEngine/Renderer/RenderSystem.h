#pragma once

#include "DTEngine/Renderer/Prereq.h"

#include "DTEngine/Engine/Singleton.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class Window;

		class RenderSystem : public Singleton<RenderSystem>
		{
		public:
			explicit RenderSystem();
			~RenderSystem();

			void init();
			void shutdown();

			inline vk::Instance vkInstance() const { return mVkInstance; }
			operator vk::Instance() { return mVkInstance; }

			inline GPUDevice& gpuDevice() const { return *mDevice; }

			SharedPtr<Window> createWindow(const std::string &name, unsigned int width, unsigned int height);

			static uint32_t findQueueIndex(vk::PhysicalDevice phyDevice, vk::QueueFlags flags);

			static void activeVkLayers(std::vector<const char *> &layers);

		private:
			void initVKInstance();
			void initVkDebug();
			void initVKDevice();

			void destroyVkDebug();

		private:

			static VkBool32 vkMsgCallback(
				VkDebugReportFlagsEXT flags,
				VkDebugReportObjectTypeEXT objType,
				uint64_t srcObject,
				size_t location,
				int32_t msgCode,
				const char* pLayerPrefix,
				const char* pMsg,
				void* pUserData);

		private:
			vk::Instance mVkInstance;
			VkDebugReportCallbackEXT mVkMsgCallback;

			GPUDevice *mDevice;
		};

	}

}