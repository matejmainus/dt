#include "DTEngine/Renderer/GPUMeshRenderer.h"
#include "DTEngine/Renderer/Entities/GPUMesh.h"
#include "DTEngine/Renderer/Entities/GPUMaterial.h"
#include "DTEngine/Renderer/Entities/GPUSceneObject.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/Pipeline.h"
#include "DTEngine/Renderer/GPUBuffer.h"
#include "DTEngine/Renderer/DescriptorPool.h"
#include "DTEngine/Renderer/DescriptorSet.h"
#include "DTEngine/Renderer/DrawParams.h"

#include "DTEngine/Core/RenderSettings.h"
#include "DTEngine/Core/SceneObject.h"
#include "DTEngine/Core/SceneObjectComponents/MeshRenderer.h"
#include "DTEngine/Core/Mesh.h"
#include "DTEngine/Core/Material.h"

namespace DTEngine
{
	namespace Renderer
	{
		GPUMeshRenderer::GPUMeshRenderer(Core::SceneObjectComponents::MeshRenderer &renderer) :
			GPURenderer(renderer),
			mDescriptorSet(nullptr)
		{
			Core::SceneObjectComponents::MeshRenderer &meshRenderer = static_cast<Core::SceneObjectComponents::MeshRenderer&>(mRenderer);

			mGpuSceneObject = new Entities::GPUSceneObject(meshRenderer.sceneObject());
		}

		GPUMeshRenderer::~GPUMeshRenderer()
		{
			delete mGpuSceneObject;
		}

		bool GPUMeshRenderer::canRender() const
		{
			Core::SceneObjectComponents::MeshRenderer &meshRenderer = static_cast<Core::SceneObjectComponents::MeshRenderer&>(mRenderer);

			return meshRenderer.mesh() != nullptr && meshRenderer.material() != nullptr;
		}

		uint64_t GPUMeshRenderer::hash() const
		{
			Core::SceneObjectComponents::MeshRenderer &meshRenderer = static_cast<Core::SceneObjectComponents::MeshRenderer&>(mRenderer);

			uint64_t materialTypeHash = (uint16_t) meshRenderer.material()->id();
			uint64_t meshHash = (uint16_t) meshRenderer.mesh()->id();
			//get lower par of material address
			uint64_t materialHash = reinterpret_cast<uint32_t>(meshRenderer.material().get());

			uint64_t hash = (materialTypeHash << 48) | (meshHash << 32) | materialHash;

			return hash;
		}

		void GPUMeshRenderer::prepare(DrawParams &drawParams)
		{
			Core::SceneObjectComponents::MeshRenderer &meshRenderer = static_cast<Core::SceneObjectComponents::MeshRenderer&>(mRenderer);

			mGpuMesh = meshRenderer.mesh()->gpuMesh();
			mGpuMaterial = meshRenderer.material()->gpuMaterial();
		}

		void GPUMeshRenderer::bufferData(DrawParams &drawParams)
		{
			mGpuMesh->bufferData(drawParams);
			mGpuMaterial->bufferData(drawParams);

			mGpuSceneObject->bufferData(drawParams);
		}

		void GPUMeshRenderer::draw(DrawParams &drawParams)
		{
			vk::Buffer meshBuffer = mGpuMesh->buffer()->vkBuffer();

			const Pipeline &pipeline = mGpuMaterial->pipeline();

			std::vector<vk::DescriptorSet> vkDescSet;

			if (pipeline.vkPipeline() != drawParams.currentPipeline)
			{
				drawParams.drawCmdBuff->bindPipeline(vk::PipelineBindPoint::eGraphics, pipeline);
				drawParams.currentPipeline = pipeline.vkPipeline();
				drawParams.currentPipelineLayout = pipeline.vkPipelineLayout();

				vkDescSet.push_back(drawParams.viewport);
			}

			if (drawParams.currentMaterial != mGpuMaterial->vkDescriptorSet())
			{
				drawParams.currentMaterial = mGpuMaterial->vkDescriptorSet();

				vkDescSet.push_back(mGpuMaterial->vkDescriptorSet());
			}

			if (!Core::RenderSettings::getInstance().usePushConstants())
			{
				vkDescSet.push_back(mGpuSceneObject->vkDescriptorSet());
			}

			if (!vkDescSet.empty())
			{
				uint32_t descriptorOffset = 0;
				
				if(Core::RenderSettings::getInstance().usePushConstants())
					descriptorOffset = (uint32_t)(1 + GPURenderer::sMaterialDescriptorSet - vkDescSet.size());
				else
					descriptorOffset = (uint32_t)(1 + GPURenderer::sObjectDescriptorSet - vkDescSet.size());

				drawParams.drawCmdBuff->bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pipeline.vkPipelineLayout(),
					descriptorOffset, (uint32_t) vkDescSet.size(), vkDescSet.data(), 0, nullptr);
			}

			if(meshBuffer != drawParams.currentVertexBuffer)
			{
				std::array<vk::DeviceSize, 1> offsets = { mGpuMesh->vertexOffset() };
				drawParams.drawCmdBuff->vkCommandBuffer().bindVertexBuffers(0, offsets.size(), &meshBuffer, offsets.data());

				drawParams.currentVertexBuffer = meshBuffer;
			}

			if (meshBuffer != drawParams.currentIndexBuffer)
			{
				drawParams.drawCmdBuff->vkCommandBuffer().bindIndexBuffer(meshBuffer, mGpuMesh->indexOffset(), vk::IndexType::eUint16);

				drawParams.currentIndexBuffer = meshBuffer;
			}

			if (Core::RenderSettings::getInstance().usePushConstants())
				mGpuSceneObject->bufferConstants(drawParams);

			drawParams.drawCmdBuff->drawIndexed((uint32_t) mGpuMesh->indexiesCount(), 1, 0, 0, 1);
		}
	}
}