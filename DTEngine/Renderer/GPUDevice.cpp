#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUMemoryManager.h"
#include "DTEngine/Renderer/PipelineManager.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"
#include "DTEngine/Renderer/CommandPool.h"
#include "DTEngine/Renderer/Window.h"
#include "DTEngine/Renderer/RenderSystem.h"

#include "DTEngine/Core/RenderStatsManager.h"
#include "DTEngine/Engine/Log.h"

namespace DTEngine
{
namespace Renderer
{

	GPUDevice::GPUDevice(vk::PhysicalDevice physicalDevice) :
		mVkPhysDevice(physicalDevice),
		mMemoryManager(nullptr),
		mGraphicsQueue(nullptr)
	{
		vk::PhysicalDeviceProperties phyDeviceProps = physicalDevice.getProperties();

		Log::info("Created device: " + std::string(phyDeviceProps.deviceName()));

		std::vector<const char*> extensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
		std::vector<const char*> layers;

#ifdef VK_DEBUG
		RenderSystem::activeVkLayers(layers);
#endif

		std::array<float, 1> queuePriorities = { 0.0f };
		uint32_t graphicsQueueIndex = findQueueIndex(vk::QueueFlagBits::eGraphics);

		vk::DeviceQueueCreateInfo queueCreateInfo;
		queueCreateInfo.queueCount(1);
		queueCreateInfo.queueFamilyIndex(graphicsQueueIndex);
		queueCreateInfo.pQueuePriorities(queuePriorities.data());

		vk::DeviceCreateInfo deviceCreateInfo;
		deviceCreateInfo.queueCreateInfoCount(1);
		deviceCreateInfo.pQueueCreateInfos(&queueCreateInfo);
		deviceCreateInfo.enabledExtensionCount((uint32_t)extensions.size());
		deviceCreateInfo.ppEnabledExtensionNames(extensions.data());
		deviceCreateInfo.enabledLayerCount((uint32_t) layers.size());
		deviceCreateInfo.ppEnabledLayerNames(layers.data());

		mVkDevice = physicalDevice.createDevice(deviceCreateInfo);

		mPipelineManager = new PipelineManager(*this);
		mMemoryManager = new GPUMemoryManager(*this);
		mGraphicsQueue = new GPUGraphicsQueue(*this, graphicsQueueIndex);

		mMemoryManager->init();
	}

	GPUDevice::~GPUDevice()
	{
		if (mGraphicsQueue)
			delete mGraphicsQueue;

		if (mMemoryManager)
			delete mMemoryManager;

		if (mPipelineManager)
			delete mPipelineManager;

		mVkDevice.destroy();
	}

	vk::Format GPUDevice::getSupportedDepthFormat() const
	{
		// Valid depth formats ordered by pack size and precission
		std::vector<vk::Format> depthFormats = {
			vk::Format::eD32SfloatS8Uint,
			vk::Format::eD32Sfloat,
			vk::Format::eD24UnormS8Uint,
			vk::Format::eD16UnormS8Uint,
			vk::Format::eD16Unorm
		};

		VkFormatProperties formatProps;
		for (const vk::Format &format : depthFormats)
		{
			formatProps = mVkPhysDevice.getFormatProperties(format);
			if (formatProps.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
			{
				return format;
			}
		}

		return vk::Format::eUndefined;
	}

	void GPUDevice::updateDescriptorSets(std::vector<vk::WriteDescriptorSet> const & descriptorWrites, std::vector<vk::CopyDescriptorSet> const & descriptorCopies)
	{
		Core::RenderStatsManager::getInstance().currentFrame()->descriptorUpdateCount += descriptorWrites.size();
		Core::RenderStatsManager::getInstance().currentFrame()->descriptorCopyCount += descriptorCopies.size();

		mVkDevice.updateDescriptorSets(descriptorWrites, descriptorCopies);
	}

	void* GPUDevice::mapMemory(vk::DeviceMemory memory, vk::DeviceSize offset, vk::DeviceSize size, vk::MemoryMapFlags flags)
	{
		Core::RenderStatsManager::getInstance().currentFrame()->memoryMapCount++;

		return mVkDevice.mapMemory(memory, offset, size, flags);
	}

	vk::SwapchainKHR GPUDevice::createSwapchain(vk::SurfaceKHR surface, vk::SwapchainCreateInfoKHR &createInfo)
	{
		vk::Bool32 surfaceSup = mVkPhysDevice.getSurfaceSupportKHR(mGraphicsQueue->familyIndex(), surface);
		assert(surfaceSup);

		vk::SurfaceCapabilitiesKHR surfCaps;
		VKPP_VERIFY(mVkPhysDevice.getSurfaceCapabilitiesKHR(surface, surfCaps));

		std::vector<vk::PresentModeKHR> presentModes = mVkPhysDevice.getSurfacePresentModesKHR(surface);

		std::vector<vk::SurfaceFormatKHR> surfFormats = mVkPhysDevice.getSurfaceFormatsKHR(surface);

		vk::SwapchainCreateInfoKHR swapchainCI;
		swapchainCI.imageExtent(surfCaps.currentExtent())
			.surface(surface)
			.imageUsage(vk::ImageUsageFlagBits::eColorAttachment)
			.imageArrayLayers(1)
			.clipped(true)
			.compositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque)
			.presentMode(vk::PresentModeKHR::eFifoKHR);

		for (const vk::PresentModeKHR &presentMode : presentModes)
		{
			if (presentMode == vk::PresentModeKHR::eMailboxKHR )
			{
				swapchainCI.presentMode(presentMode);
				break;
			}

			if ((swapchainCI.presentMode() != vk::PresentModeKHR::eMailboxKHR) && (presentMode == vk::PresentModeKHR::eImmediateKHR))
			{
				swapchainCI.presentMode(presentMode);
			}
		}

		swapchainCI.minImageCount(surfCaps.minImageCount() + 1);
		if ((surfCaps.maxImageCount() > 0) && (swapchainCI.minImageCount() > surfCaps.maxImageCount()))
		{
			swapchainCI.minImageCount(surfCaps.maxImageCount());
		}

		if (surfCaps.supportedTransforms() & vk::SurfaceTransformFlagBitsKHR::eIdentity)
		{
			swapchainCI.preTransform(vk::SurfaceTransformFlagBitsKHR::eIdentity);
		}
		else {
			swapchainCI.preTransform(surfCaps.currentTransform());
		}

		swapchainCI.imageFormat(surfFormats[0].format());
		swapchainCI.imageColorSpace(surfFormats[0].colorSpace());

		if (swapchainCI.imageFormat() == vk::Format::eUndefined)
			swapchainCI.imageFormat(vk::Format::eR8G8B8Unorm);

		createInfo = reinterpret_cast<vk::SwapchainCreateInfoKHR&>(swapchainCI);

		return mVkDevice.createSwapchainKHR(swapchainCI);
	}

	uint32_t GPUDevice::findQueueIndex(vk::QueueFlags flags)
	{
		return RenderSystem::findQueueIndex(mVkPhysDevice, flags);
	}

}
}

