#pragma once

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUImage;

		class RenderTarget
		{
		public:
			explicit RenderTarget(uint32_t width, uint32_t height);
			~RenderTarget();

			virtual uint32_t nextTarget(vk::Semaphore semaphore) = 0;
			virtual vk::Framebuffer targetFramebuffer() = 0;
			virtual SharedPtr<GPUImage> targetImage() = 0;

			virtual void present() {};

			inline uint32_t width() const { return mWidth; }
			inline uint32_t height() const { return mHeight;  };

		protected:
			uint32_t mWidth;
			uint32_t mHeight;
		};

	}
}