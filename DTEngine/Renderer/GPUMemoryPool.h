#pragma once

#include <vector>

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/GPUMemory.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;

		class GPUMemoryPool
		{
		public:
			explicit GPUMemoryPool(GPUDevice &device, vk::MemoryPropertyFlags flags, vk::DeviceSize pageSize, vk::DeviceSize pageAlignment, size_t pageCount = 0);
			~GPUMemoryPool();
			
			SharedPtr<GPUMemory> pull(vk::MemoryRequirements requirements, GPUMemory::GPUSubMemory &allocInfo);
			SharedPtr<GPUMemory> pull(vk::DeviceSize size, vk::DeviceSize alignment, GPUMemory::GPUSubMemory &allocInfo);

		private:
			SharedPtr<GPUMemory> createNewPage(vk::DeviceSize size);

			vk::DeviceSize pageAllocSize(vk::DeviceSize requestedSize) const;

		private:
			std::mutex mGuard;

			GPUDevice &mDevice;

			vk::MemoryPropertyFlags mMemFlags;

			vk::DeviceSize mPageSize;
			vk::DeviceSize mPageAlignment;

			std::vector<SharedPtr<GPUMemory>> mPages;
		};

	}
}