#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/GPUDevice.h"

#include "DTEngine/Core/RenderStatsManager.h"

namespace DTEngine
{
namespace Renderer
{
	CommandBuffer::CommandBuffer(GPUDevice &device, vk::CommandBufferAllocateInfo &allocInfo) :
		mDevice(device),
		mVkAllocInfo(allocInfo)
	{
		mVkCmdBuff = device.vkDevice().allocateCommandBuffers(allocInfo)[0];
	}

	CommandBuffer::~CommandBuffer()
	{
		mDevice.vkDevice().freeCommandBuffers(mVkAllocInfo.commandPool(), 1, &mVkCmdBuff);
	}

	void CommandBuffer::drawIndexed(uint32_t indexCount, uint32_t instanceCount, uint32_t firstIndex, int32_t vertexOffset, uint32_t firstInstance)
	{
		Core::RenderStatsManager::getInstance().currentFrame()->drawCallCount++;

		mVkCmdBuff.drawIndexed(indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
	}

	void CommandBuffer::bindDescriptorSets(vk::PipelineBindPoint pipelineBindPoint, vk::PipelineLayout layout, uint32_t firstSet, uint32_t descriptorSetCount, const vk::DescriptorSet * pDescriptorSets, uint32_t dynamicOffsetCount, const uint32_t * pDynamicOffsets)
	{
		Core::RenderStatsManager::getInstance().currentFrame()->descriptorBindCount += descriptorSetCount;

		mVkCmdBuff.bindDescriptorSets(pipelineBindPoint, layout, firstSet, descriptorSetCount, pDescriptorSets, dynamicOffsetCount, pDynamicOffsets);
	}

	void CommandBuffer::bindPipeline(vk::PipelineBindPoint pipelineBindPoint, vk::Pipeline pipeline)
	{
		Core::RenderStatsManager::getInstance().currentFrame()->pipelineBindCount++;

		mVkCmdBuff.bindPipeline(pipelineBindPoint, pipeline);
	}

	void CommandBuffer::copyBufferToImage(vk::Buffer srcBuffer, vk::Image dstImage, vk::ImageLayout dstImageLayout, uint32_t regionCount, const vk::BufferImageCopy * pRegions)
	{
		Core::RenderStatsManager::getInstance().currentFrame()->copyCallCount += regionCount;

		mVkCmdBuff.copyBufferToImage(srcBuffer, dstImage, dstImageLayout, regionCount, pRegions);
	}

	void CommandBuffer::copyBuffer(vk::Buffer srcBuffer, vk::Buffer dstBuffer, uint32_t regionCount, const vk::BufferCopy * pRegions)
	{
		Core::RenderStatsManager::getInstance().currentFrame()->copyCallCount += regionCount;

		mVkCmdBuff.copyBuffer(srcBuffer, dstBuffer, regionCount, pRegions);
	}

}
}