#include "DTEngine/Renderer/GPUGraphicsQueue.h"

#include "DTEngine/Renderer/GPUDevice.h"

namespace DTEngine
{
namespace Renderer
{

	GPUGraphicsQueue::GPUGraphicsQueue(GPUDevice &device, uint32_t queueFamilyIndex) :
		GPUQueue(device, queueFamilyIndex)
	{
	}

	GPUGraphicsQueue::~GPUGraphicsQueue()
	{
	}

	void GPUGraphicsQueue::present(const vk::PresentInfoKHR &presentInfo) const
	{
		mQueueMutex.lock();

		mVkQueue.presentKHR(presentInfo);
		
		mQueueMutex.unlock();
	}
}
}