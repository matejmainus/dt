#pragma once

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Core
	{
		class Renderer;
	}

	namespace Renderer
	{
		struct DrawParams;

		class GPURenderer
		{
		public:
			explicit GPURenderer(DTEngine::Core::Renderer &renderer);
			virtual ~GPURenderer();

			virtual bool canRender() const = 0;

			virtual uint64_t hash() const = 0;

			virtual void prepare(DrawParams &drawParams) = 0;

			virtual void bufferData(DrawParams &drawParams) = 0;

			virtual void draw(DrawParams &drawParams) = 0;

		public:
			static const uint32_t sViewportDescriptorSet = 0;
			static const uint32_t sMaterialDescriptorSet = 1;
			static const uint32_t sObjectDescriptorSet = 2;

		protected:
			DTEngine::Core::Renderer &mRenderer;

		};
	}

}
