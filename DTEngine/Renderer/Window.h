#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/RenderTarget.h"

#include "DTEngine/Core/EventHandler.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class GPUImage;

		class Window : public RenderTarget, public DTEngine::Core::EventHandler
		{
		public:
			explicit Window(GPUDevice &device, const std::string &title, uint32_t width, uint32_t height);
			~Window();

			virtual bool handleEvent(const SDL_Event *event) override;
			
			void setTitle(const std::string &title);

			virtual uint32_t nextTarget(vk::Semaphore semaphore) override;
			virtual vk::Framebuffer targetFramebuffer() override;
			virtual SharedPtr<GPUImage> targetImage() override;

			virtual SharedPtr<GPUImage> depthStencilImage();

			virtual void present() override;

		private:
			void createWindow(const std::string &name, uint32_t width, uint32_t height);
			void initSwapchain();
			void initFramebuffers();

			bool handleWindowEvent(const SDL_WindowEvent *event);

		private:
			GPUDevice &mDevice;

			SDL_Window *mWindow;

			vk::SurfaceKHR mVkSurface;
			vk::SwapchainKHR mVkSwapchain;

			vk::SwapchainCreateInfoKHR mVkSwapchainCreateInfo;

			SharedPtr<GPUImage> mDepthStencilImage;
			std::vector<SharedPtr<GPUImage>> mSwapchainImages;
			std::vector<vk::Framebuffer> mVkFrameBuffers;
			
			uint32_t mCurrentPresImage;
		};

	}
}

