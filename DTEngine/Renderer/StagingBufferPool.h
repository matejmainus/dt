#pragma once

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class GPUMemoryManager;
		class GPUBuffer;

		class StagingBufferPool
		{
		public:
			enum class PageType
			{
				None,
				Buffer,
				Image
			};

		public:
			explicit StagingBufferPool(GPUDevice &device, vk::DeviceSize pageSize, vk::DeviceSize pageAlignment, size_t pageCount = 0);
			~StagingBufferPool();
			
			void* map(vk::MemoryRequirements &requirements, SharedPtr<GPUBuffer> &buffer, vk::DeviceSize &offset, PageType type);
			void* map(vk::DeviceSize size, vk::DeviceSize alignment, SharedPtr<GPUBuffer> &buffer, vk::DeviceSize &offset, PageType type);

			void flushPages();
			void freePages();

		private:
			struct Page;

			typedef std::vector<Page*> PageVec;

		private:
			StagingBufferPool::Page* createNewPage(vk::DeviceSize pageSize) const;

			vk::DeviceSize pageAllocSize(vk::DeviceSize requestedSize) const;
		private:
			std::mutex mPagesGuard;

			GPUDevice &mDevice;
			GPUMemoryManager &mMemoryMgr;

			vk::DeviceSize mPageSize;
			vk::DeviceSize mPageAlignment;

			PageVec mPages;
		};


	}
}