#include "DTEngine/Renderer/GPUMemoryManager.h"
#include "DTEngine/Renderer/GPUMemory.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUMemoryPool.h"

#define DEFAULT_PAGE_SIZE 4 * 1024
#define DEFAULT_PAGE_ALIGNMENT 4 *1024
#define INITIAL_POOL_SIZE 2

namespace DTEngine
{
namespace Renderer
{

	GPUMemoryManager::GPUMemoryManager(GPUDevice &device) :
		mDevice(device)
	{
	}

	GPUMemoryManager::~GPUMemoryManager()
	{
		for (GPUMemoryPool* const &pool : mPools)
			delete pool;

		mPools.clear();
	}

	void GPUMemoryManager::init()
	{
		mMemoryProps = mDevice.vkPhysicalDevice().getMemoryProperties();

		for (uint32_t i = 0; i < mMemoryProps.memoryTypeCount(); ++i)
		{
			mPools.push_back(new GPUMemoryPool(mDevice, mMemoryProps.memoryTypes()[i].propertyFlags(), DEFAULT_PAGE_SIZE, DEFAULT_PAGE_ALIGNMENT, INITIAL_POOL_SIZE));
		}
	}

	SharedPtr<GPUMemory> GPUMemoryManager::allocateMemory(std::initializer_list<vk::MemoryRequirements> requirements, vk::MemoryPropertyFlags flags)
	{
		vk::DeviceSize size = 0;
		uint32_t memTypeMask = memoryTypeMask(flags);

		for (const vk::MemoryRequirements &requirement : requirements)
		{
			size += requirement.size();
			memTypeMask &= requirement.memoryTypeBits();
		}

		return allocateMemory(size, bestMemoryType(memTypeMask));
	}

	SharedPtr<GPUMemory> GPUMemoryManager::allocateMemory(vk::DeviceSize size, vk::MemoryPropertyFlags flags)
	{
		uint32_t memTypeMask = memoryTypeMask(flags);

		return allocateMemory(size, bestMemoryType(memTypeMask));
	}

	SharedPtr<GPUMemory> GPUMemoryManager::allocateMemory(vk::MemoryRequirements &requirements, vk::MemoryPropertyFlags flags)
	{
		uint32_t memTypeMask = memoryTypeMask(flags) & requirements.memoryTypeBits();

		return allocateMemory(requirements.size(), bestMemoryType(memTypeMask));
	}

	SharedPtr<GPUMemory> GPUMemoryManager::allocateMemory(vk::DeviceSize size, uint32_t memoryTypeIndex)
	{
		vk::MemoryAllocateInfo allocInfo(size, memoryTypeIndex);

		return std::make_shared<GPUMemory>(mDevice, allocInfo);
	}

	SharedPtr<GPUMemory> GPUMemoryManager::pullMemory(vk::DeviceSize size, vk::DeviceSize alignment, vk::MemoryPropertyFlags flags, GPUMemory::GPUSubMemory &allocInfo)
	{
		uint32_t memTypeMask = memoryTypeMask(flags);

		return pullMemory(size, alignment, bestMemoryType(memTypeMask), allocInfo);
	}

	SharedPtr<GPUMemory> GPUMemoryManager::pullMemory(vk::MemoryRequirements & requirements, GPUMemory::GPUSubMemory &allocInfo, vk::MemoryPropertyFlags flags)
	{
		uint32_t memTypeMask = memoryTypeMask(flags) & requirements.memoryTypeBits();

		return pullMemory(requirements.size(), requirements.alignment(), bestMemoryType(memTypeMask), allocInfo);
	}

	SharedPtr<GPUMemory> GPUMemoryManager::pullMemory(vk::DeviceSize size, vk::DeviceSize alignment, uint32_t memoryTypeIndex, GPUMemory::GPUSubMemory &allocInfo)
	{
		return mPools[memoryTypeIndex]->pull(size, alignment, allocInfo);
	}

	GPUMemoryPool& GPUMemoryManager::pool(vk::MemoryRequirements requirements)
	{
		uint32_t poolIndex = bestMemoryType(requirements.memoryTypeBits());

		return *mPools[poolIndex];
	}

	uint32_t GPUMemoryManager::memoryTypeMask(vk::MemoryPropertyFlags flags)
	{
		std::bitset<VK_MAX_MEMORY_TYPES> mask;

		for (uint32_t i = 0; i < mMemoryProps.memoryTypeCount(); ++i)
		{
			mask[i] = (mMemoryProps.memoryTypes()[i].propertyFlags() & flags) == flags;
		}

		return static_cast<uint32_t>(mask.to_ulong());
	}

	uint32_t GPUMemoryManager::bestMemoryType(uint32_t bitfield)
	{
		std::bitset<VK_MAX_MEMORY_TYPES> mask(bitfield);

		assert(bitfield != 0);

		for (uint32_t i = 0; i < mMemoryProps.memoryTypeCount(); ++i)
		{
			if (mask[i])
				return i;
		}

		throw std::runtime_error("No valid GPU memory type");
	}

}
}