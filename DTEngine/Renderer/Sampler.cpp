#include "DTEngine/Renderer/Sampler.h"

#include "DTEngine/Renderer/GPUDevice.h"


namespace DTEngine
{
namespace Renderer
{
	Sampler::Sampler(GPUDevice & device, vk::SamplerCreateInfo & createInfo) :
		mDevice(device)
	{
		mVkSampler = mDevice.vkDevice().createSampler(createInfo);
	}

	Sampler::~Sampler()
	{
		mDevice.vkDevice().destroySampler(mVkSampler);
	}
}
}

