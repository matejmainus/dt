#pragma once

#include <vector>
#include <list>

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Engine/Lockable.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPURenderer;

		struct RenderContext
		{
			Renderer::GPURenderer *renderer;

			uint64_t hash;

			glm::mat4 modelMatrix;
		};

		struct RenderNode
		{
			std::vector<RenderContext> childs;
		};

		typedef SharedPtr<RenderNode> RenderNodeSPtr;

		typedef std::vector<RenderContext>::const_iterator RenderContextIterator_const;
		typedef std::vector<RenderContext>::iterator RenderContextIterator;
		typedef std::vector<RenderNodeSPtr>::iterator RenderNodeIterator;
		typedef std::vector<RenderNodeSPtr>::const_iterator RenderNodeIterator_const;

		class RenderTree : public Lockable
		{
		public:
			explicit RenderTree();
			~RenderTree();

			void add(const RenderContext &context);

			bool empty() const;

			void toNodes();

			void clear();

			inline RenderNodeIterator begin() { return mNodes.begin(); }
			inline RenderNodeIterator end() { return mNodes.end(); }
			inline RenderNodeIterator_const cbegin() const { return mNodes.cbegin(); }
			inline RenderNodeIterator_const cend() const { return mNodes.cend(); };

		private:
			RenderNodeSPtr createNode();

		private:

			std::vector<RenderContext> mContexts;

			std::vector<RenderNodeSPtr> mNodes;
		};
	}
}