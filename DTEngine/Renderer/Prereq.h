#pragma once

//#define VK_DEBUG

#ifdef WIN32
#include <windows.h>
#endif

#define VKCPP_ENHANCED_MODE
#include <vk_cpp.h>

#include <cassert>
#include <string>
#include <iostream>

#include "DTEngine/Engine/glm.h"

#include "DTEngine/Engine/Engine.h"

#define VK_VERIFY(expr)\
{\
	VkResult result = expr; \
	if (result != VK_SUCCESS) \
	{ \
		throw std::system_error(result, ""#expr); \
	} \
}

#define VKPP_VERIFY(expr)\
{ \
	vk::Result result = expr; \
	if (result != vk::Result::eSuccess) \
	{ \
		throw std::system_error(result, ""#expr); \
	} \
}

#define VK_GET_INSTANCE_PROC_ADDR(inst, entrypoint) \
	fp##entrypoint = (PFN_vk##entrypoint) vkGetInstanceProcAddr(inst, "vk"#entrypoint); \
    assert(fp##entrypoint != NULL)      

#define VK_GET_DEVICE_PROC_ADDR(dev, entrypoint) \
    fp##entrypoint = (PFN_vk##entrypoint) vkGetDeviceProcAddr(dev, "vk"#entrypoint); \
    assert(fp##entrypoint != NULL)

#define VK_MEMORY_ALL_FLAGS (vk::MemoryPropertyFlagBits) -1