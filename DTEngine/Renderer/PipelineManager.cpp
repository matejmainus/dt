#include <iostream>
#include <fstream>

#include "DTEngine/Renderer/PipelineManager.h"
#include "DTEngine/Renderer/Pipeline.h"
#include "DTEngine/Renderer/GPUDevice.h"

namespace DTEngine
{
namespace Renderer
{

	PipelineManager::PipelineManager(GPUDevice &device) :
		mDevice(device),
		mPipelineId(0)
	{
		vk::PipelineCacheCreateInfo cacheCreateInfo;

		mVkPipelineCache = mDevice.vkDevice().createPipelineCache(cacheCreateInfo);
	}

	PipelineManager::~PipelineManager()
	{
		for (Pipeline* const &pipeline : mPipelines)
			delete pipeline;

		mDevice.vkDevice().destroyPipelineCache(mVkPipelineCache, nullptr);

		for (auto pair : mShaders)
			mDevice.vkDevice().destroyShaderModule(pair.second, nullptr);
	}

	vk::ShaderModule PipelineManager::shader(const std::string & filePath)
	{
		MutexGuard locker(mShadersMutex);

		ShadersMap::iterator it = mShaders.find(filePath);

		if (it != mShaders.end())
			return it->second;

		vk::ShaderModule shaderModule;
		vk::ShaderModuleCreateInfo moduleCreateInfo;

		std::vector<char> shaderCode = loadShaderFile(filePath);

		moduleCreateInfo.codeSize(shaderCode.size());
		moduleCreateInfo.pCode((uint32_t *)shaderCode.data());

		shaderModule = mDevice.vkDevice().createShaderModule(moduleCreateInfo);

		mShaders[filePath] = shaderModule;

		return shaderModule;
	}

	Pipeline* PipelineManager::graphicsPipeline(vk::GraphicsPipelineCreateInfo &createInfo, vk::PipelineLayoutCreateInfo &layoutCreateInfo)
	{
		MutexGuard locker(mPipelineMutex);

		Pipeline *pipeline = new Pipeline(mPipelineId, mDevice, mVkPipelineCache, createInfo, layoutCreateInfo);

		mPipelineId++;

		return pipeline;
	}

	std::vector<char> PipelineManager::loadShaderFile(const std::string &filePath)
	{
		std::streampos size;
		std::vector<char> data;

		std::ifstream file(filePath, std::ios::in | std::ios::binary | std::ios::ate);
		if (file.is_open())
		{
			size = file.tellg();
			data.resize(size);

			file.seekg(0, std::ios::beg);
			file.read(&data[0], size);
			file.close();

			return data;
		}
		else
			throw std::runtime_error("Could not open shader file: " + filePath);
	}

}
}