#include "DTEngine/Renderer/DescriptorPool.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/DescriptorSet.h"

namespace DTEngine
{
namespace Renderer
{

	DescriptorPool::DescriptorPool(GPUDevice &device, vk::DescriptorPoolCreateInfo &createInfo) :
		mDevice(device),
		mVkCreateInfo(createInfo)
	{
		mVkPool = mDevice.vkDevice().createDescriptorPool(createInfo);
	}

	DescriptorPool::~DescriptorPool()
	{
		freeDescriptorSets();

		mDevice.vkDevice().destroyDescriptorPool(mVkPool);
	}

	SharedPtr<DescriptorSet> DescriptorPool::allocateDescriptorSet(vk::DescriptorSetLayout layout)
	{
		vk::DescriptorSetAllocateInfo allocInfo(mVkPool, 1, &layout);

		SharedPtr<DescriptorSet> sptr = std::make_shared<DescriptorSet>(mDevice, allocInfo,
			(mVkCreateInfo.flags() & vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet) == vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet);

 		return sptr;
	}

	void DescriptorPool::freeDescriptorSets()
	{
		mDevice.vkDevice().resetDescriptorPool(mVkPool, vk::DescriptorPoolResetFlags());
	}

}
}