#include <algorithm>

#include "DTEngine/Renderer/GPUMemoryPool.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUMemoryManager.h"

namespace DTEngine
{
namespace Renderer
{
	GPUMemoryPool::GPUMemoryPool(GPUDevice &device, vk::MemoryPropertyFlags flags, vk::DeviceSize pageSize, vk::DeviceSize pageAlignment, size_t pageCount):
		mDevice(device),
		mMemFlags(flags),
		mPageSize(pageSize),
		mPageAlignment(pageAlignment)
	{
		for (size_t i = 0; i < pageCount; ++i)
			mPages.push_back(createNewPage(pageSize));
	}

	GPUMemoryPool::~GPUMemoryPool()
	{
		mPages.clear();
	}

	SharedPtr<GPUMemory> GPUMemoryPool::pull(vk::DeviceSize size, vk::DeviceSize alignment, GPUMemory::GPUSubMemory &allocInfo)
	{
		MutexGuard locker(mGuard);

		for (const SharedPtr<GPUMemory> &page : mPages)
		{
			page->lock();

			if (page->subAllocate(size, alignment, allocInfo))
			{
				page->unlock();

				return page;
			}

			page->unlock();
		}

		SharedPtr<GPUMemory> newPage = createNewPage(pageAllocSize(size));
		if (!newPage->subAllocate(size, alignment, allocInfo))
			throw std::runtime_error("Could not suballocate memory");

		return newPage;
	}

	vk::DeviceSize GPUMemoryPool::pageAllocSize(vk::DeviceSize requestedSize) const
	{
		return GPUMemory::size(std::max(mPageSize, requestedSize), mPageAlignment);
	}

	SharedPtr<GPUMemory> GPUMemoryPool::pull(vk::MemoryRequirements requirements, GPUMemory::GPUSubMemory &allocInfo)
	{
		return pull(requirements.size(), requirements.alignment(), allocInfo);
	}

	SharedPtr<GPUMemory> GPUMemoryPool::createNewPage(vk::DeviceSize size)
	{
		SharedPtr<GPUMemory> page = mDevice.memoryManager().allocateMemory(size, mMemFlags);

		mPages.push_back(page);

		return page;
	}

}
}