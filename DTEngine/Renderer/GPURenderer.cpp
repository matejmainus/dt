#include "DTEngine/Renderer/GPURenderer.h"

#include "DTEngine/Core/Renderer.h"

namespace DTEngine
{
namespace Renderer
{
	GPURenderer::GPURenderer(DTEngine::Core::Renderer &renderer) :
		mRenderer(renderer)
	{
	}

	GPURenderer::~GPURenderer()
	{}
}
}

