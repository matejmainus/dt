#pragma once

#include <atomic>

#include "DTEngine/Renderer/Prereq.h"

#include "DTEngine/Engine/Lockable.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class GPUResource;

		class GPUMemory : public Lockable
		{
		public:
			struct GPUSubMemory
			{
				GPUSubMemory(vk::DeviceSize off = 0, vk::DeviceSize sizz = 0)
				{
					size = sizz;
					offset = off;
				}

				inline void clear()
				{
					offset = 0;
					size = 0;
				}

				inline vk::DeviceSize start() const { return offset; }
				inline vk::DeviceSize end() const { return offset + size; }

				vk::DeviceSize offset;
				vk::DeviceSize size;
			};

		public:
			explicit GPUMemory(GPUDevice &device, vk::MemoryAllocateInfo &allocInfo);
			~GPUMemory();

			inline vk::DeviceMemory vkMemory() const { return mVkMemory; };
			operator vk::DeviceMemory() { return mVkMemory; }

			bool subAllocate(vk::DeviceSize size, vk::DeviceSize alignment, GPUSubMemory &subMem);
			bool subAllocate(const vk::MemoryRequirements &requirements, GPUSubMemory &subMem);
			void subFree(const GPUSubMemory &subMem);

			void subFreeAll();

			void* map(vk::DeviceSize offset = 0, vk::DeviceSize size = 0);
			void unmap();

			inline vk::DeviceSize size() const { return mVkAllocInfo.allocationSize(); }
	
			static vk::DeviceSize size(vk::DeviceSize size, vk::DeviceSize alignment);

		private:
			static vk::DeviceSize offset(int64_t offset, vk::DeviceSize alignment);

		private:
			struct MapState
			{
				inline void clear()
				{
					dest = nullptr;
					count = 0;
					offset = 0;
					size = 0;
				}

				void* dest;
				size_t count;

				vk::DeviceSize offset;
				vk::DeviceSize size;
			};

		private:
			GPUDevice &mDevice;

			vk::DeviceMemory mVkMemory;
			vk::MemoryAllocateInfo mVkAllocInfo;

			std::vector<GPUSubMemory> mFreeRegions;
			//std::vector<GPUSubMemory> mSubAllocs;

			MapState mMapState;
		};

	}
}