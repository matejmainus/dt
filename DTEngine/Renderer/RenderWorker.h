#pragma once

#include <queue>
#include <atomic>

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/RenderTree.h"

namespace DTEngine
{
	namespace Core
	{
		class Scene;
		class SceneObject;
		class Viewport;
	}

	namespace Renderer
	{
		class RenderTarget;
		class RenderThread;
		class CommandPool;
		class CommandBuffer;

		struct RenderPhaseParams
		{
			Core::Scene *scene;
			Core::Viewport* viewport;

			SharedPtr<Renderer::RenderTarget> target;

			Renderer::CommandPool* commandPool;
			SharedPtr<Renderer::CommandBuffer> drawCommandBuffer;
			SharedPtr<Renderer::CommandBuffer> copyCommandBuffer;

			vk::Framebuffer frameBuffer;
			vk::RenderPass renderPass;
			uint32_t subPassId;
		};

		class RenderWorker
		{
		public:

			explicit RenderWorker();
			~RenderWorker();

			void prepare();

			

			void renderScene(const RenderPhaseParams &params);

			void join();

			bool isRenderRunning() const;

		private:
			void initRenderThreads(size_t count);
			void stopRenderThreads();

			void beginTraverse();
			void traverseScene(const RenderContext &context, const Core::SceneObject &sceneObject);
			void endTraverse();
			void beginRender();
			void endRender();

			void fetchNode(SharedPtr<RenderNode> &node);
			void endNode(SharedPtr<RenderNode> &node);
			bool isCurrentAtEnd() const;

		private:
			friend class RenderThread;

			std::vector<RenderThread *> mThreads;

			RenderPhaseParams mPhaseParams;

			RenderTree mTree;
			RenderNodeIterator mCurrentNode;

			mutable std::mutex mTreeCondMutex;
			std::condition_variable mTreeCondition;

			mutable std::mutex mJoinConditionMutex;
			std::condition_variable mJoinCondition;

			std::atomic<bool> mActive;
		};
	}
}

