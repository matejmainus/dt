#pragma once

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;

		class DescriptorSet
		{
		public:
			explicit DescriptorSet(GPUDevice &device, vk::DescriptorSetAllocateInfo &allocInfo, bool freeOnDestroy = true);
			~DescriptorSet();

			inline vk::DescriptorSet vkDescriptorSet() const { return mVkDescriptorSet; }
			operator vk::DescriptorSet() const { return mVkDescriptorSet; }

		private:
			GPUDevice &mDevice;

			bool mFreeOnDestroy;

			vk::DescriptorSetAllocateInfo mVkAllocInfo;
			vk::DescriptorSet mVkDescriptorSet;
		};

	}
}