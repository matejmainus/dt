#include "DTEngine/Renderer/GPUImage.h"

#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUMemory.h"
#include "DTEngine/Renderer/CommandBuffer.h"

#include "DTEngine/Core/RenderStatsManager.h"

namespace DTEngine
{
namespace Renderer
{
	GPUImage::GPUImage(GPUDevice &device, vk::ImageCreateInfo &createInfo) :
		GPUResource(device),
		mVkCreateInfo(createInfo)
	{
		mVkImage = mDevice.vkDevice().createImage(createInfo);

		mVkMemoryRequirements = mDevice.vkDevice().getImageMemoryRequirements(mVkImage);

		Core::RenderStatsManager::getInstance().usageStats()->gpuImageCount++;
	}

	GPUImage::GPUImage(GPUDevice &device, vk::Image externalImage) :
		GPUResource(device),
		mVkImage(externalImage),
		mVkCreateInfo()
	{
		Core::RenderStatsManager::getInstance().usageStats()->gpuImageCount++;
	}

	GPUImage::~GPUImage()
	{
		if(mVkCreateInfo.format() != vk::Format::eUndefined)
		{
			mDevice.vkDevice().destroyImage(mVkImage);
		}

		Core::RenderStatsManager::getInstance().usageStats()->gpuImageCount--;
	}

	void GPUImage::bind(vk::DeviceMemory deviceMemory, vk::DeviceSize offset)
	{
		mDevice.vkDevice().bindImageMemory(mVkImage, deviceMemory, offset);
	}

	void GPUImage::barrier(vk::CommandBuffer cmdBuff,
		vk::ImageLayout oldLayout, vk::ImageLayout newLayout,
		vk::AccessFlags srcAccessMask, vk::AccessFlags dstAccessMask, 
		vk::ImageSubresourceRange range,
		vk::PipelineStageFlags srcStage, vk::PipelineStageFlags dstStage)
	{
		vk::ImageMemoryBarrier imgBarrier;
		imgBarrier
			.image(mVkImage)
			.newLayout(newLayout)
			.oldLayout(oldLayout)
			.srcAccessMask(srcAccessMask)
			.dstAccessMask(dstAccessMask)
			.subresourceRange(range)
			.srcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
			.dstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED);

		cmdBuff.pipelineBarrier(srcStage, dstStage,
			vk::DependencyFlagBits(),
			0, nullptr,
			0, nullptr,
			1, &imgBarrier);
	}

	SharedPtr<GPUImageView> GPUImage::view(vk::ImageViewType type, vk::Format format, vk::ComponentMapping & mapping, vk::ImageSubresourceRange & range)
	{
		vk::ImageViewCreateFlags flags;

		vk::ImageViewCreateInfo createInfo(flags, mVkImage, type, format, mapping, range);

		SharedPtr<GPUImageView> viewSptr = std::make_shared<GPUImageView>(*this, createInfo);

		addView(viewSptr);

		return viewSptr;
	}

	GPUImageView::GPUImageView(GPUImage &image, vk::ImageViewCreateInfo & createInfo) :
		mImage(image)
	{
		mVkView = mImage.device().vkDevice().createImageView(createInfo);
	}

	GPUImageView::~GPUImageView()
	{
		mImage.device().vkDevice().destroyImageView(mVkView);
	}
}
}
