#pragma once


#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class VkExtLoader
		{
		public:
			static void initInstanPtrs(vk::Instance instance);
			static void initDevicePtrs(vk::Device device);
		};

	}
}