#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Engine/Lockable.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;

		class CommandBuffer : public Lockable
		{
		public:
			explicit CommandBuffer(GPUDevice &device, vk::CommandBufferAllocateInfo &allocInfo);
			~CommandBuffer();

			inline vk::CommandBuffer vkCommandBuffer() const { return mVkCmdBuff; }
			operator vk::CommandBuffer() const { return mVkCmdBuff; }

			void drawIndexed(uint32_t indexCount, uint32_t instanceCount, uint32_t firstIndex, int32_t vertexOffset, uint32_t firstInstance);
			void bindDescriptorSets(vk::PipelineBindPoint pipelineBindPoint, vk::PipelineLayout layout, uint32_t firstSet, uint32_t descriptorSetCount, const vk::DescriptorSet* pDescriptorSets, uint32_t dynamicOffsetCount, const uint32_t* pDynamicOffsets);
			void bindPipeline(vk::PipelineBindPoint pipelineBindPoint, vk::Pipeline pipeline);
			void copyBufferToImage(vk::Buffer srcBuffer, vk::Image dstImage, vk::ImageLayout dstImageLayout, uint32_t regionCount, const vk::BufferImageCopy* pRegions);
			void copyBuffer(vk::Buffer srcBuffer, vk::Buffer dstBuffer, uint32_t regionCount, const vk::BufferCopy* pRegions);

		private:
			GPUDevice &mDevice;

			vk::CommandBufferAllocateInfo mVkAllocInfo;

			vk::CommandBuffer mVkCmdBuff;
		};
	}
}