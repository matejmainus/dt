#pragma once

#include "DTEngine/Renderer/Prereq.h"

#include "DTEngine/Renderer/GPUResource.h"

namespace DTEngine
{
namespace Renderer
{
	class GPUBufferView;

	class GPUBuffer : public GPUResource
	{
	public:
		explicit GPUBuffer(GPUDevice &device, vk::BufferCreateInfo &createInfo);
		virtual ~GPUBuffer();

		inline vk::Buffer vkBuffer() const { return mVkBuffer; }
		operator vk::Buffer() const { return mVkBuffer; }

		inline const vk::BufferCreateInfo& vkCreateInfo() { return mVkCreateInfo; }

		SharedPtr<GPUBufferView> view(vk::Format format, vk::DeviceSize offset, vk::DeviceSize range);

	protected:
		virtual void bind(vk::DeviceMemory deviceMemory, vk::DeviceSize offset);

	private:
		friend class GPUBufferView;

		vk::Buffer mVkBuffer;

		vk::BufferCreateInfo mVkCreateInfo;
	};

	class GPUBufferView
	{
	public:
		explicit GPUBufferView(GPUBuffer &buffer, vk::BufferViewCreateInfo &createInfo);
		~GPUBufferView();

		inline vk::BufferView vkView() const { return mVkView; }
		operator vk::BufferView() const { return mVkView; }

	private:
		GPUBuffer &mBuffer;

		vk::BufferView mVkView;
	};

}
}