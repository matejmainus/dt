#pragma once

#include <bitset>

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/GPUMemory.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class GPUMemoryPool;

		class GPUMemoryManager
		{
		public:
			explicit GPUMemoryManager(GPUDevice &device);
			~GPUMemoryManager();

			void init();

			GPUMemoryPool& pool(vk::MemoryRequirements requirements);

			SharedPtr<GPUMemory> allocateMemory(std::initializer_list<vk::MemoryRequirements> requirements, vk::MemoryPropertyFlags flags = VK_MEMORY_ALL_FLAGS);
			SharedPtr<GPUMemory> allocateMemory(vk::DeviceSize size, vk::MemoryPropertyFlags flags = vk::MemoryPropertyFlagBits::eDeviceLocal);
			SharedPtr<GPUMemory> allocateMemory(vk::MemoryRequirements &requirements, vk::MemoryPropertyFlags flags = VK_MEMORY_ALL_FLAGS);
			inline SharedPtr<GPUMemory> allocateMemory(vk::DeviceSize size, uint32_t memoryTypeIndex);

			SharedPtr<GPUMemory> pullMemory(vk::DeviceSize size, vk::DeviceSize alignment, vk::MemoryPropertyFlags flags, GPUMemory::GPUSubMemory &allocInfo);
			SharedPtr<GPUMemory> pullMemory(vk::MemoryRequirements &requirements, GPUMemory::GPUSubMemory &allocInfo, vk::MemoryPropertyFlags flags = VK_MEMORY_ALL_FLAGS);
			inline SharedPtr<GPUMemory> pullMemory(vk::DeviceSize size, vk::DeviceSize alignment, uint32_t memoryTypeIndex, GPUMemory::GPUSubMemory &allocInfo);

		private:
			uint32_t memoryTypeMask(vk::MemoryPropertyFlags flags);
			uint32_t bestMemoryType(uint32_t memTypeMask);

		private:
			GPUDevice &mDevice;

			vk::PhysicalDeviceMemoryProperties mMemoryProps;

			std::vector<GPUMemoryPool *> mPools;
		};
	}
}