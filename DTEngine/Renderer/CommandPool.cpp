#include "DTEngine/Renderer/CommandPool.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/CommandBuffer.h"

namespace DTEngine
{
namespace Renderer
{

	CommandPool::CommandPool(GPUDevice &device, vk::CommandPoolCreateInfo &createInfo) :
		mDevice(device),
		mVkCreateInfo(createInfo)
	{
		mVkCmdPool = device.vkDevice().createCommandPool(createInfo);
	}

	CommandPool::~CommandPool()
	{
		freeCommandBuffers();

		mDevice.vkDevice().destroyCommandPool(mVkCmdPool);
	}

	SharedPtr<CommandBuffer> CommandPool::allocCommandBuffer(vk::CommandBufferLevel level)
	{
		vk::CommandBufferAllocateInfo allocInfo(mVkCmdPool, level, 1);
		
		SharedPtr<CommandBuffer> sptr = std::make_shared<CommandBuffer>(mDevice, allocInfo);

		mCommandBuffers.push_back(sptr);

		return sptr;
	}

	void CommandPool::freeCommandBuffers()
	{
		mCommandBuffers.clear();

		//VKPP_VERIFY(vk::resetCommandPool(mDevice, mVkCmdPool, vk::CommandPoolResetFlags()));
	}
}
}