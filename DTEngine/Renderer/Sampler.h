#pragma once

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;

		class Sampler
		{
		public:
			explicit Sampler(GPUDevice &device, vk::SamplerCreateInfo &createInfo);
			~Sampler();

			inline vk::Sampler vkSampler() const { return mVkSampler; }
			operator vk::Sampler() const { return mVkSampler; }

		private:
			GPUDevice &mDevice;

			vk::Sampler mVkSampler;
		};
	}
}