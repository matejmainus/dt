#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Renderer/GPURenderer.h"

namespace DTEngine
{
	namespace Core
	{
		namespace SceneObjectComponents
		{
			class MeshRenderer;
		}
	}

	namespace Renderer
	{
		namespace Entities
		{
			class GPUSceneObject;
			class GPUMaterial;
			class GPUMesh;
		}

		class DescriptorSet;

		class GPUMeshRenderer : public GPURenderer
		{
		public:
			explicit GPUMeshRenderer(Core::SceneObjectComponents::MeshRenderer &renderer);
			virtual ~GPUMeshRenderer();

			virtual bool canRender() const override;

			virtual uint64_t hash() const override;

			virtual void prepare(DrawParams &drawParams) override;
			virtual void bufferData(DrawParams &drawParams) override;
			virtual void draw(DrawParams &drawParams) override;
	
		private:
			Entities::GPUMesh *mGpuMesh;
			Entities::GPUMaterial *mGpuMaterial;
			Entities::GPUSceneObject *mGpuSceneObject;

			SharedPtr<DescriptorSet> mDescriptorSet;
		};

	}
}