#include "DTEngine/Renderer/RenderTree.h"
#include "DTEngine/Core/Renderer.h"

#include "DTEngine/Core/RenderSettings.h"

#define CONTEXT_BUCKET 100

namespace DTEngine
{
	namespace Renderer
	{

		RenderTree::RenderTree()
		{
			clear();
		}

		RenderTree::~RenderTree()
		{
		}

		void RenderTree::add(const RenderContext &context)
		{
			mContexts.push_back(context);
		}

		bool RenderTree::empty() const
		{
			return mNodes.empty();
		}

		void RenderTree::toNodes()
		{
			if(Core::RenderSettings::getInstance().useTreeSort())
			{
				std::sort(mContexts.begin(), mContexts.end(), [](const RenderContext &c1, const RenderContext &c2)
				{
					return (c1.hash < c2.hash);
				});
			}

			RenderNodeSPtr lastNode = createNode();
			RenderContextIterator testSameIt;

			for (RenderContextIterator contextIt = mContexts.begin(); contextIt != mContexts.end() ; ++contextIt)
			{
				//testSameIt = contextIt + std::min(20, (int) std::distance(contextIt, mContexts.end()));
				//|| ((testSameIt != mContexts.end()) && lastNode->childs.size() > (CONTEXT_BUCKET / 4) && testSameIt->hash == contextIt->hash)

				if (lastNode->childs.size() > CONTEXT_BUCKET)
				{
					lastNode = createNode();
				}
				
				lastNode->childs.push_back(*contextIt);
			}

			mContexts.clear();
		}

		void RenderTree::clear()
		{
			mNodes.clear();
			mContexts.clear();
		}

		RenderNodeSPtr RenderTree::createNode()
		{
			RenderNodeSPtr node = std::make_shared<RenderNode>();
			mNodes.push_back(node);

			return node;
		}

	}
}