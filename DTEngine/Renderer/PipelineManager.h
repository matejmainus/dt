#pragma once

#include <map>
#include <vector>

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class Pipeline;

		class PipelineManager
		{
		public:
			typedef std::map<std::string, vk::ShaderModule> ShadersMap;
			typedef std::vector<Pipeline*> PipelineVec;

		public:
			explicit PipelineManager(GPUDevice &device);
			~PipelineManager();

			inline vk::PipelineCache vkPipelineCache() const { return mVkPipelineCache; }

			vk::ShaderModule shader(const std::string &filePath);

			Pipeline* graphicsPipeline(vk::GraphicsPipelineCreateInfo &createInfo, vk::PipelineLayoutCreateInfo &layoutCreateInfo);

		private:
			static std::vector<char> loadShaderFile(const std::string &filePath);

		private:
			mutable std::mutex mPipelineMutex;
			mutable std::mutex mShadersMutex;

			GPUDevice &mDevice;

			vk::PipelineCache mVkPipelineCache;

			ShadersMap mShaders;
			PipelineVec mPipelines;

			size_t mPipelineId;
		};

	}
}