#include "DTEngine/Renderer/Pipeline.h"
#include "DTEngine/Renderer/GPUDevice.h"

namespace DTEngine
{
namespace Renderer
{

	Pipeline::Pipeline(size_t pipelineId, GPUDevice &device, vk::PipelineCache cache, 
		vk::GraphicsPipelineCreateInfo &pipelineCreateInfo, vk::PipelineLayoutCreateInfo &pipelineLayoutCreateInfo) :
		mDevice(device),
		mPipelineId(pipelineId)
	{
		mVkPipelineLayout = mDevice.vkDevice().createPipelineLayout(pipelineLayoutCreateInfo);

		pipelineCreateInfo.layout(mVkPipelineLayout);

		VKPP_VERIFY(mDevice.vkDevice().createGraphicsPipelines(cache, 1, &pipelineCreateInfo, nullptr, &mVkPipeline));
	}

	Pipeline::~Pipeline()
	{
		mDevice.vkDevice().destroyPipeline(mVkPipeline);

		mDevice.vkDevice().destroyPipelineLayout(mVkPipelineLayout);
	}

}
}