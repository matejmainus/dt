#include "DTEngine/Renderer/RenderTarget.h"

namespace DTEngine
{
namespace Renderer
{

	RenderTarget::RenderTarget(uint32_t width, uint32_t height) :
		mWidth(width),
		mHeight(height)
	{
	}

	RenderTarget::~RenderTarget()
	{
	}

}
}