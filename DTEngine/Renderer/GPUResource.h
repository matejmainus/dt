#pragma once

#include "DTEngine/Renderer/Prereq.h"

#include "DTEngine/Engine/Lockable.h"

#include "DTEngine/Renderer/GPUMemory.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;

		class GPUResource : public Lockable
		{
		public:
			explicit GPUResource(GPUDevice &device);
			virtual ~GPUResource();

			inline GPUDevice& device() const { return mDevice; }

			void destroyView(const SharedPtr<void> &view);

			virtual vk::MemoryRequirements memoryRequirements() const;

			virtual void bindToGpuMemory(const SharedPtr<GPUMemory> &gpuMemory);
			virtual void bindToInherentGpuMemory(vk::MemoryPropertyFlags flags = VK_MEMORY_ALL_FLAGS);
			virtual void bindToPoolGpuMemory(vk::MemoryPropertyFlags flags = VK_MEMORY_ALL_FLAGS);

			inline SharedPtr<GPUMemory> boundMemory() { return mMemory; }

			void* map(vk::DeviceSize offset = 0, vk::DeviceSize size = 0);
			void unmap();

		protected:
			virtual void bind(vk::DeviceMemory memory, vk::DeviceSize offset) = 0;

			void allocateGpuMemory(vk::MemoryRequirements &requirements, vk::MemoryPropertyFlags flags = VK_MEMORY_ALL_FLAGS);
			void suballocateGpuMemory(vk::MemoryRequirements &requirements, const SharedPtr<GPUMemory> &gpuMemory);
			void poolGpuMemory(vk::MemoryRequirements &requirements, vk::MemoryPropertyFlags flags = VK_MEMORY_ALL_FLAGS);

			void freeGpuMemory();

			void addView(const SharedPtr<void> &view);

		protected:
			GPUDevice &mDevice;
			SharedPtr<GPUMemory> mMemory;

			GPUMemory::GPUSubMemory mSubMemory;

			vk::MemoryRequirements mVkMemoryRequirements;

			std::vector<SharedPtr<void>> mViews;
		};
	}
}