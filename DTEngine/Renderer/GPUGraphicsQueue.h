#pragma once

#include "DTEngine/Renderer/Prereq.h"

#include "DTEngine/Renderer/GPUQueue.h"

namespace DTEngine
{
	namespace Renderer
	{

		class GPUGraphicsQueue : public GPUQueue
		{
		public:
			explicit GPUGraphicsQueue(GPUDevice &device, uint32_t queueFamilyIndex);
			virtual ~GPUGraphicsQueue();

			void present(const vk::PresentInfoKHR &presentInfo) const;

		private:
		};
	}
}

