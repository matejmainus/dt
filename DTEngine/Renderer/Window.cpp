#include <SDL_syswm.h>
#include <windows.h>

#include "DTEngine/Renderer/Window.h"
#include "DTEngine/Renderer/RenderSystem.h"
#include "DTEngine/Renderer/Compositor.h"
#include "DTEngine/Renderer/GPUDevice.h"
#include "DTEngine/Renderer/GPUGraphicsQueue.h"
#include "DTEngine/Renderer/CommandPool.h"
#include "DTEngine/Renderer/CommandBuffer.h"
#include "DTEngine/Renderer/GPUImage.h"

namespace DTEngine
{
namespace Renderer
{

	Window::Window(GPUDevice &device, const std::string &title, uint32_t width, uint32_t height) :
		RenderTarget(width, height),
		mDevice(device)
	{
		createWindow(title, width, height);

		initSwapchain();
	}

	Window::~Window()
	{
		for (const vk::Framebuffer &framebuffer : mVkFrameBuffers)
			mDevice.vkDevice().destroyFramebuffer(framebuffer);

		mDevice.vkDevice().destroySwapchainKHR(mVkSwapchain);

		RenderSystem::getInstance().vkInstance().destroySurfaceKHR(mVkSurface);
	}

	bool Window::handleEvent(const SDL_Event * event)
	{
		switch (event->type)
		{
		case SDL_WINDOWEVENT:
			return handleWindowEvent(&event->window);
		default:
			return false;
		}
	}

	uint32_t Window::nextTarget(vk::Semaphore semaphore)
	{
		mDevice.vkDevice().acquireNextImageKHR(mVkSwapchain, UINT64_MAX, semaphore, nullptr, mCurrentPresImage);

		return mCurrentPresImage;
	}

	void Window::present()
	{
		vk::PresentInfoKHR presentInfo = {};
		presentInfo.swapchainCount(1);
		presentInfo.pSwapchains(&mVkSwapchain);
		presentInfo.pImageIndices(&mCurrentPresImage);

		mDevice.graphicsQueue().present(presentInfo);
	}

	void Window::setTitle(const std::string &title)
	{
		SDL_SetWindowTitle(mWindow, title.c_str());
	}

	vk::Framebuffer Window::targetFramebuffer()
	{
		return mVkFrameBuffers[mCurrentPresImage];
	}

	SharedPtr<GPUImage> Window::targetImage()
	{
		return mSwapchainImages[mCurrentPresImage];
	}

	SharedPtr<GPUImage> Window::depthStencilImage()
	{
		return mDepthStencilImage;
	}

	void Window::createWindow(const std::string &name, unsigned int width, unsigned int height)
	{
		mWindow = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0);

#ifdef _WIN32
		SDL_SysWMinfo windowInfo;
		SDL_VERSION(&windowInfo.version);

		if (!SDL_GetWindowWMInfo(mWindow, &windowInfo))
			throw std::runtime_error("Could not get window info");

		HINSTANCE instance = GetModuleHandle(NULL);
		HWND windowHandle = windowInfo.info.win.window;

		vk::Win32SurfaceCreateInfoKHR surfaceCreateInfo;
		surfaceCreateInfo.hinstance(instance);
		surfaceCreateInfo.hwnd(windowHandle);

		mVkSurface = RenderSystem::getInstance().vkInstance().createWin32SurfaceKHR(surfaceCreateInfo);
#endif
	}

	void Window::initSwapchain()
	{
		mVkSwapchain = mDevice.createSwapchain(mVkSurface, mVkSwapchainCreateInfo);

		std::vector<vk::Image> images = mDevice.vkDevice().getSwapchainImagesKHR(mVkSwapchain);

		vk::ImageSubresourceRange colorRange;
		colorRange.aspectMask(vk::ImageAspectFlagBits::eColor)
			.layerCount(1)
			.baseArrayLayer(0)
			.levelCount(1)
			.baseMipLevel(0);

		vk::ImageMemoryBarrier colorBarrier;
		colorBarrier.dstAccessMask(vk::AccessFlagBits::eColorAttachmentWrite)
			.oldLayout(vk::ImageLayout::eUndefined)
			.newLayout(vk::ImageLayout::ePresentSrcKHR)
			.srcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
			.dstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
			.subresourceRange(colorRange);
		
		std::vector<vk::ImageMemoryBarrier> transfareBariers;

		for (const vk::Image &image : images)
		{ 			
			colorBarrier.image(image);

			transfareBariers.push_back(colorBarrier);
			mSwapchainImages.push_back(std::make_shared<GPUImage>(mDevice, image));
		}

		vk::Extent2D colorExtend = mVkSwapchainCreateInfo.imageExtent();

		vk::ImageCreateInfo depthImageCreateInfo;
		depthImageCreateInfo.imageType(vk::ImageType::e2D)
			.format(mDevice.getSupportedDepthFormat())
			.extent({ colorExtend.width(), colorExtend.height(), 1 })
			.mipLevels(1)
			.arrayLayers(1)
			.samples(vk::SampleCountFlagBits::e1)
			.tiling(vk::ImageTiling::eOptimal)
			.usage(vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eTransferSrc);
		
		mDepthStencilImage = std::make_shared<GPUImage>(mDevice, depthImageCreateInfo);
		mDepthStencilImage->bindToInherentGpuMemory(vk::MemoryPropertyFlagBits::eDeviceLocal);

		vk::ImageSubresourceRange depthRange;
		depthRange.aspectMask(vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil)
			.layerCount(1)
			.baseArrayLayer(0)
			.levelCount(1)
			.baseMipLevel(0);

		vk::ImageMemoryBarrier depthBarrier;
		depthBarrier.dstAccessMask(vk::AccessFlagBits::eDepthStencilAttachmentWrite)
			.oldLayout(vk::ImageLayout::eUndefined)
			.newLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal)
			.srcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
			.dstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
			.subresourceRange(depthRange)
			.image(*mDepthStencilImage);

		transfareBariers.push_back(depthBarrier);

		SharedPtr<CommandBuffer> commandBuff = mDevice.graphicsQueue().globalCommandPool().allocCommandBuffer(vk::CommandBufferLevel::ePrimary);
		commandBuff->vkCommandBuffer().begin(vk::CommandBufferBeginInfo());

		commandBuff->vkCommandBuffer().pipelineBarrier(
			vk::PipelineStageFlagBits::eAllCommands,
			vk::PipelineStageFlagBits::eTopOfPipe,
			vk::DependencyFlags(),
			0, nullptr,
			0, nullptr,
			(uint32_t) transfareBariers.size(), transfareBariers.data()
			);

		commandBuff->vkCommandBuffer().end();

		std::vector<vk::CommandBuffer> buffs;
		buffs.push_back(*commandBuff);

		vk::SubmitInfo submitInfo;
		submitInfo.commandBufferCount((uint32_t) buffs.size())
			.pCommandBuffers(buffs.data());

		mDevice.graphicsQueue().submit(submitInfo);

		mVkFrameBuffers.resize(images.size());
		initFramebuffers();
	}

	void Window::initFramebuffers()
	{
		vk::ComponentMapping viewMapping;

		vk::ImageSubresourceRange colorRange;
		colorRange.aspectMask(vk::ImageAspectFlagBits::eColor)
			.layerCount(1)
			.baseArrayLayer(0)
			.levelCount(1)
			.baseMipLevel(0);

		vk::ImageSubresourceRange depthStencilRange;
		depthStencilRange.aspectMask(vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil)
			.layerCount(1)
			.baseArrayLayer(0)
			.levelCount(1)
			.baseMipLevel(0);

		vk::ImageView attachments[2];

		vk::FramebufferCreateInfo frameBufferCreateInfo;

		frameBufferCreateInfo.renderPass(Compositor::getInstance().vkRenderPass())
			.attachmentCount(2)
			.pAttachments(attachments)
			.width(mWidth)
			.height(mHeight)
			.layers(1);

		SharedPtr<GPUImageView> depthView = mDepthStencilImage->view(vk::ImageViewType::e2D, mDepthStencilImage->vkCreateInfo().format(), viewMapping, depthStencilRange);

		attachments[0] = *depthView;

		SharedPtr<GPUImageView> colorView;
		for (uint32_t i = 0; i < mSwapchainImages.size(); i++)
		{
			colorView = mSwapchainImages[i]->view(vk::ImageViewType::e2D, mVkSwapchainCreateInfo.imageFormat(), viewMapping, colorRange);

			attachments[1] = *colorView;

			mVkFrameBuffers[i] = mDevice.vkDevice().createFramebuffer(frameBufferCreateInfo);
		}
	}

	bool Window::handleWindowEvent(const SDL_WindowEvent * event)
	{
		return event->windowID == SDL_GetWindowID(mWindow);
	}


}
}