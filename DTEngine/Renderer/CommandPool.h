#pragma once

#include "DTEngine/Renderer/Prereq.h"
#include "DTEngine/Engine/Lockable.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUDevice;
		class GPUQueue;
		class CommandBuffer;

		class CommandPool : public Lockable
		{
		public:
			explicit CommandPool(GPUDevice &device, vk::CommandPoolCreateInfo &createInfo);
			~CommandPool();

			inline vk::CommandPool vkCmdPool() const { return mVkCmdPool; }

			SharedPtr<CommandBuffer> allocCommandBuffer(vk::CommandBufferLevel level);

			void freeCommandBuffers();

		private:
			GPUDevice &mDevice;

			vk::CommandPoolCreateInfo mVkCreateInfo;
			vk::CommandPool mVkCmdPool;

			std::vector<SharedPtr<CommandBuffer>> mCommandBuffers;
		};
	}
}