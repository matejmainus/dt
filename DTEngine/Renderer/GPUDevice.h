#pragma once

#include <map>

#include "DTEngine/Renderer/Prereq.h"

namespace DTEngine
{
	namespace Renderer
	{
		class GPUGraphicsQueue;
		class GPUMemoryManager;
		class PipelineManager;

		class Window;
		class RenderTarget;

		class GPUImage;

		class GPUDevice 
		{
		public:
			explicit GPUDevice(vk::PhysicalDevice physicalDevice);
			~GPUDevice();

			inline vk::PhysicalDevice vkPhysicalDevice() const { return mVkPhysDevice; }

			operator vk::Device() const { return mVkDevice; }
			inline vk::Device vkDevice() const { return mVkDevice; }

			inline GPUMemoryManager& memoryManager() const { return *mMemoryManager; }
			inline GPUGraphicsQueue& graphicsQueue() const { return *mGraphicsQueue; }
			inline PipelineManager& pipelineManager() const { return *mPipelineManager; }

			vk::SwapchainKHR GPUDevice::createSwapchain(vk::SurfaceKHR surface, vk::SwapchainCreateInfoKHR &createInfo);
			
			uint32_t findQueueIndex(vk::QueueFlags flags);

			vk::Format getSupportedDepthFormat() const;

			void updateDescriptorSets(std::vector<vk::WriteDescriptorSet> const & descriptorWrites, std::vector<vk::CopyDescriptorSet> const & descriptorCopies);
			void* mapMemory(vk::DeviceMemory memory, vk::DeviceSize offset, vk::DeviceSize size, vk::MemoryMapFlags flags);

		private:
			mutable std::mutex mDeviceMutex;

			vk::PhysicalDevice mVkPhysDevice;
			vk::Device mVkDevice;

			GPUMemoryManager *mMemoryManager;
			GPUGraphicsQueue *mGraphicsQueue;
			PipelineManager *mPipelineManager;
		};

	}
}