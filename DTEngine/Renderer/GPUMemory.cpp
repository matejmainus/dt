#include <algorithm>

#include "DTEngine/Renderer/GPUMemory.h"
#include "DTEngine/Renderer/GPUDevice.h"

#include "DTEngine/Core/RenderStatsManager.h"

namespace DTEngine
{
namespace Renderer
{
	GPUMemory::GPUMemory(GPUDevice &device, vk::MemoryAllocateInfo &allocInfo) :
		mDevice(device),
		mVkAllocInfo(allocInfo)
	{
		mMapState.clear();
		subFreeAll();

		assert(allocInfo.allocationSize() > 0);

		mVkMemory = mDevice.vkDevice().allocateMemory(mVkAllocInfo);

		Core::RenderStatsManager::getInstance().usageStats()->gpuMemoryAllocSize += mVkAllocInfo.allocationSize();
	}

	GPUMemory::~GPUMemory()
	{
		mDevice.vkDevice().freeMemory(mVkMemory);

		Core::RenderStatsManager::getInstance().usageStats()->gpuMemoryAllocSize -= mVkAllocInfo.allocationSize();
	}

	bool GPUMemory::subAllocate(vk::DeviceSize size, vk::DeviceSize alignment, GPUSubMemory & subMem)
	{
		subMem = {0,0};

		std::vector<GPUSubMemory>::iterator it = mFreeRegions.begin();
		vk::DeviceSize alignedOffset;
		vk::DeviceSize offsetDiff;

		for (; it != mFreeRegions.end(); ++it)
		{
			alignedOffset = offset(it->offset, alignment);
			offsetDiff = (alignedOffset - it->offset);

			if (it->size - offsetDiff >= size)
			{
				subMem.offset = alignedOffset;
				subMem.size = size;

				if (offsetDiff > 0)
				{
					GPUSubMemory mem(it->offset, offsetDiff);
					it = ++mFreeRegions.insert(it, mem);
				}

				it->offset += offsetDiff + size;
				it->size -= size;

				return true;
			}
		}

		return false;
		/*
		std::vector<GPUSubMemory>::iterator it = mSubAllocs.begin();
		std::vector<GPUSubMemory>::iterator next = mSubAllocs.begin();

		if (next != mSubAllocs.end())
			++next;

		//Find free space between two neighbour buffers
		for (; next != mSubAllocs.end(); ++it, ++next)
		{
			if (next->offset - offset(it->size + it->offset, alignment) >= size)
				break;
		}

		if (it == mSubAllocs.end() || next == mSubAllocs.end())
		{
			if (it == mSubAllocs.end())
			{
				if (size > mVkAllocInfo.allocationSize())
					return false;
			}
			else
			{
				if (offset(it->offset + it->size, alignment) + size > mVkAllocInfo.allocationSize())
					return false;
			}

			subMem.offset = it == mSubAllocs.end() ? 0 : offset(it->offset + it->size, alignment);
			subMem.size = size;

			mSubAllocs.push_back(subMem);

			return true;
		}
		else if ((offset(it->offset + it->size, alignment) + size) <= mVkAllocInfo.allocationSize())
		{
			subMem.offset = offset(it->offset + it->size, alignment);
			subMem.size = size;

			mSubAllocs.insert(++it, subMem);

			return true;
		}*/

		return false;
	}

	bool GPUMemory::subAllocate(const vk::MemoryRequirements &requirements, GPUSubMemory &subMem)
	{
		return subAllocate(requirements.size(), requirements.alignment(), subMem);
	}

	void GPUMemory::subFree(const GPUSubMemory &info)
	{
		std::vector<GPUSubMemory>::iterator next = mFreeRegions.begin();
		std::vector<GPUSubMemory>::iterator prev = mFreeRegions.begin();

		if (mFreeRegions.size() == 0)
		{
			mFreeRegions.push_back(info);
			return;
		}

		if (next != mFreeRegions.end())
			++next;

		//Insert phase
		for (; next != mFreeRegions.end(); ++prev, ++next)
		{
			
			//Merge two regions together
			if ((info.start() >= prev->start() && info.start() <= prev->end())
				&& (info.end() >= next->start() && info.end() <= next->end()))
			{
				prev->size = next->end() - prev->start();
				mFreeRegions.erase(next);

				return;
			}
			//prepend
			else if (info.start() <= next->start() && info.end() >= next->start())
			{
				next->size = next->end() - info.start();
				next->offset = info.start();

				return;
			}
			//append
			else if (info.start() >= prev->start() && info.start() <= prev->end())
			{
				prev->size = info.end() - prev->start();

				return;
			}
			//add before next
			else if (info.start() > prev->end() && info.end() < next->start())
			{
				mFreeRegions.insert(next, info);
				return;
			}

		}

		if (info.start() <= prev->start() && info.end() >= prev->start() && info.end() <= prev->end())
		{
			prev->size = prev->end() - info.start();
			prev->offset = info.start();
		}
		else
			mFreeRegions.insert(prev, info);
	}

	void GPUMemory::subFreeAll()
	{
		GPUSubMemory fullMemory(0, mVkAllocInfo.allocationSize());

		mFreeRegions.clear();
		mFreeRegions.push_back(fullMemory);

		//mSubAllocs.clear();
	}

	void* GPUMemory::map(vk::DeviceSize offset, vk::DeviceSize size)
	{
		if (size == 0)
			size = mVkAllocInfo.allocationSize() - offset;

		vk::MemoryMapFlags flags;
		
		MutexGuard locker(mGuard);

		if (mMapState.count == 0)
		{
			mMapState.count = 1;
			mMapState.offset = offset;
			mMapState.size = size;

			mMapState.dest = mDevice.mapMemory(mVkMemory, offset, size, flags);

			return mMapState.dest;
		}
		else
		{
			assert(offset >= mMapState.offset && (offset + size) <= (mMapState.offset + mMapState.size));

			++mMapState.count;

			return (char *) mMapState.dest + offset;
		}
	}

	void GPUMemory::unmap()
	{
		MutexGuard locker(mGuard);

		--mMapState.count;

		if (mMapState.count == 0)
		{
			mDevice.vkDevice().unmapMemory(mVkMemory);

			mMapState.clear();
		}
	}

	vk::DeviceSize GPUMemory::size(vk::DeviceSize size, vk::DeviceSize alignment)
	{
		return ((size / alignment) + 1) * alignment;
	}

	vk::DeviceSize GPUMemory::offset(int64_t offset, vk::DeviceSize alignment)
	{
		return offset + (-offset % alignment);
	}

}
}