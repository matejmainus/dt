#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

const vec4 lightPos = vec4(0,-5,-5,1);

layout (location = 0) in vec4 inPos;
layout (location = 1) in vec4 inNormal;
layout (location = 2) in vec2 inUV;

layout (set = 0, binding = 0) uniform viewport_t
{
	mat4 viewMatrix;
	mat4 projMatrix;
} viewportBuffer;

layout (push_constant) uniform objBuffer_t
{
	mat4 modelMatrix;
	mat4 normalMatrix;
} objBuffer;

layout (location = 0) out vec4 outPos;
layout (location = 1) out vec4 outNormal;
layout (location = 2) out vec2 outUV;

layout (location = 3) out vec4 outLightPos;

void main() 
{
	mat4 mvp = viewportBuffer.projMatrix * viewportBuffer.viewMatrix * objBuffer.modelMatrix;
	
	gl_Position = mvp * inPos;

	outPos = viewportBuffer.viewMatrix * objBuffer.modelMatrix * inPos;
	outNormal = objBuffer.normalMatrix * inNormal;
	outUV = inUV;

	outLightPos = viewportBuffer.projMatrix * viewportBuffer.viewMatrix * lightPos;
}
