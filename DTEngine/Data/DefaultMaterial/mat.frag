#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

const vec3 lightColor = vec3(1.0f, 1.0f, 0.8f);
const float lightIntensity = 0.1f;

layout (location = 0) in vec4 inPos;
layout (location = 1) in vec4 inNormal;
layout (location = 2) in vec2 inUV;

layout (location = 3) in vec4 inLightPos;

layout (location = 0) out vec4 outFragColor;

layout (set = 1, binding = 0) uniform matBuffer_t
{
	vec4 diffuseColor;
} matBuffer;

layout (set = 1, binding = 1) uniform sampler2D diffuseSampler;

void main() 
{
	vec4 N = normalize(inNormal);
	vec4 L = normalize(inLightPos - inPos);

	float lambertian = max(dot(L,N), 0.0);

	vec4 color = texture(diffuseSampler, inUV) + matBuffer.diffuseColor;
	color += vec4(lightColor * lightIntensity, 0.0f);

	outFragColor = vec4(color.rgb * lambertian, color.a);
}