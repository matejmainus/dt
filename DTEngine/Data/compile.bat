
call:compileShader %~dp0/DefaultMaterial/mat.vert
call:compileShader %~dp0/DefaultMaterial/mat_push.vert
call:compileShader %~dp0/DefaultMaterial/mat.frag

call:compileShader %~dp0/SimpleMaterial/mat.vert
call:compileShader %~dp0/SimpleMaterial/mat_push.vert
call:compileShader %~dp0/SimpleMaterial/mat.frag

EXIT /B 0

:compileShader
glslangValidator.exe -V -o %~1.spv  %~1
EXIT /B 0